﻿Imports DataAccess
Imports Entidades

Public Class BLArticulo
    Public Function getArticuloxCodigoImp(ByVal _codigo As String)
        Return New DAArticulo().getArticuloxCodigoImp(_codigo)
    End Function

    Public Function getComboxCodigo(ByVal _codigo As String)
        Return New DAArticulo().getComboxCodigo(_codigo)
    End Function

    Public Function getArticuloxNombre(ByVal _nombre As String)
        Return New DAArticulo().getArticuloxNombre(_nombre)
    End Function

    Public Function getComboxNombre(ByVal _nombre As String)
        Return New DAArticulo().getComboxNombre(_nombre)
    End Function

    Public Function getFullArticulosxCodigoImp(ByVal _codigo As String)
        Return New DAArticulo().getFullArticulosxCodigoImp(_codigo)
    End Function

    Public Function getFullArticulosxNombre(ByVal _nombre As String)
        Return New DAArticulo().getFullArticulosxNombre(_nombre)
    End Function

    Public Function getConsolidadoArticulosxEmpresaAlmacenUbicacion(ByVal _hcConsolidado As HCConsolidado)
        Return New DAArticulo().getConsolidadoArticulosxEmpresaAlmacenUbicacion(_hcConsolidado)
    End Function

    Public Function obtenerStockArticulosPorEmpresaAlmacenUbicacionDelKardex(ByVal idEmpresa As Integer, ByVal idMaestroArticulo As Integer, ByVal idAlmacen As Integer, ByVal idUbicacion As Integer) As Integer
        Return New DAArticulo().obtenerStockArticulosPorEmpresaAlmacenUbicacionDelKardex(idEmpresa, idMaestroArticulo, idAlmacen, idUbicacion)
    End Function

    Public Function obtenerPreciosArticulo(ByVal intCodMaestroArt As Integer) As EArticulo
        Return New DAArticulo().obtenerPreciosArticulo(intCodMaestroArt)
    End Function

    Public Function obtenerComboActivo(ByVal _codigo As String)
        Return New DAArticulo().obtenerComboActivo(_codigo)
    End Function

End Class
