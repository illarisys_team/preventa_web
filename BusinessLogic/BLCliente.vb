﻿Imports Entidades

Public Class BLCliente

    Public Function insertarCliente(ByVal objEntCliente As EClientes) As String
        Dim objDaCliente As New DataAccess.DaCliente
        Return objDaCliente.InsertarCliente(objEntCliente)
    End Function

    Public Function obtenerCliente_PFV_DNI(ByVal dni As String) As EClientes
        Dim objDaCliente As New DataAccess.DACliente
        Return objDaCliente.obtenerCliente_PFV_DNI(dni)
    End Function

    Public Function obtenerCliente_PFV_RUC(ByVal ruc As String) As EClientes
        Dim objDaCliente As New DataAccess.DACliente
        Return objDaCliente.obtenerCliente_PFV_RUC(ruc)
    End Function

    Public Function existeClientePorDoc(ByVal tipoDocumento As String, ByVal numeroDocumento As String) As Boolean
        Dim objDaCliente As New DataAccess.DACliente
        Return objDaCliente.existeClientePorDoc(tipoDocumento, numeroDocumento)
    End Function

    Public Function getClienteXDocumento(ByVal documento As String) As List(Of String)
        Return New DataAccess.DACliente().getClienteXDocumento(documento)
    End Function

End Class
