﻿Public Class BLDistrito
    Public Function obtenerDistritoxProvincia(ByVal pkProvincia As Integer, ByVal fkdpto As Integer) As DataTable
        Dim table As New DataTable
        Dim objDaDistrito As New DataAccess.DADistrito()
        table = objDaDistrito.obtenerDistritoxProvincia(pkProvincia, fkdpto)
        Dim row As DataRow = table.NewRow()
        row("CODIGO") = 0
        row("DESCRIPCION") = "[SELECCIONE]"
        table.Rows.InsertAt(row, 0)
        Return table
    End Function
End Class
