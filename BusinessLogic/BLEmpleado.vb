﻿Imports DataAccess
Imports Entidades

Public Class BLEmpleado

    Public Function obtenerEmpleadoPorUserName(ByVal strUsuario As String, strClave As String) As EEmpleado

        Dim objDaEmpleado As New DAEmpleado()
        Return objDaEmpleado.obtenerEmpleadoPorUserName(strUsuario, strClave)

    End Function

End Class
