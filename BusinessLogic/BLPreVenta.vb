﻿Imports Entidades
Imports DataAccess

Public Class BLPreVenta
    Function GrabarHelperDetPreVenta(ByVal oHelperPreVenta As EHelperPreVenta) As Integer
        Try
            Return New DAPreVenta().GrabarHelperDetPreVenta(oHelperPreVenta)
        Catch ex As Exception
            Throw ex
        End Try
    End Function
    Public Function ObtenerHelperDetPreVenta(ByVal _empleado_Id As String) As DataTable
        Try
            Return New DAPreVenta().ObtenerHelperDetPreVenta(_empleado_Id)
        Catch ex As Exception
            Throw ex
        End Try
    End Function
    Public Function EliminarHelperDetPreVenta(ByVal _empleado_Id As Integer, ByVal _maestro_id As Integer) As Integer
        Try
            Return New DAPreVenta().EliminarHelperDetPreVenta(_empleado_Id, _maestro_id)
        Catch ex As Exception
            Throw ex
        End Try
    End Function
    Public Function ObtenerCorrelativoNumPreVenta(ByVal clasePedido As String) As String
        Try
            Return New DAPreVenta().ObtenerCorrelativoNumPreVenta(clasePedido)
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function InsertarPreVenta(ByVal oEPreVenta As EPreVenta) As String
        Try
            Return New DAPreVenta().InsertarPreVenta(oEPreVenta)
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function ObtenerPreVentaPorNumPV(ByVal numPreVenta As String) As EPreVenta
        Try
            Return New DAPreVenta().obtenerPreVentaPorNumPV(numPreVenta)
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function ObtenerDetallePV(ByVal pkCabecera As Integer) As DataTable
        Try
            Return New DAPreVenta().obtenerDetallePV(pkCabecera)
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function obtenerDetallePVImprimir(ByVal pkCabecera As Integer) As DataTable
        Try
            Return New DAPreVenta().obtenerDetallePVImprimir(pkCabecera)
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Function ActualizarPreventa(objEntPreVenta As EPreVenta) As String
        Try
            Return New DAPreVenta().ActualizarPreVenta(objEntPreVenta)
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Function InsertarDetallePreventa(oEPreVenta As EHelperPreVenta) As Integer
        Try
            Return New DAPreVenta().InsertarDetallePreventa(oEPreVenta)
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Function EliminarDetallePreVenta(codigoItemDetalle As Integer) As Integer
        Try
            Return New DAPreVenta().EliminarDetallePreVenta(codigoItemDetalle)
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Function ObtenerPreVentaImprimir(_num_preVenta As String) As EImprimirPreVenta
        Try
            Return New DAPreVenta().ObtenerPreVentaImprimir(_num_preVenta)
        Catch ex As Exception
            Throw ex
        End Try
    End Function
End Class
