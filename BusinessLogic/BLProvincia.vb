﻿Public Class BLProvincia
    Public Function obtenerProvinciaPorDepartamento(ByVal idDepartamento As Integer)
        Dim table As New DataTable
        Dim objDaDistrito As New DataAccess.DAProvincia()
        table = objDaDistrito.obtenerProvinciaPorDepartamento(idDepartamento)
        Dim row As DataRow = table.NewRow()
        row("CODIGO") = 0
        row("DESCRIPCION") = "[SELECCIONE]"
        table.Rows.InsertAt(row, 0)
        Return table
    End Function
End Class
