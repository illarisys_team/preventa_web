﻿Public Class BLTipoCambio

    Public Function obtenerTipoCambioPorEstado() As DataTable
        Return New DataAccess.DATipoCambio().obtenerTipoCambioPorEstado()
    End Function

    Public Function obtenerTipoCambioPorPrimaria(ByVal _llave_primaria As Integer) As DataTable
        Return New DataAccess.DATipoCambio().obtenerTipoCambioPorPrimaria(_llave_primaria)
    End Function

End Class
