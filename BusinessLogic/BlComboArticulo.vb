﻿Imports System.Data
Imports System.Data.SqlClient
Imports Entidades
Imports DataAccess


Public Class BlComboArticulos

    Dim objAccesos As New DataAccess.DaComboArticulos

    Public Function obtenerCombos() As DataTable
        Return objAccesos.obtenerCombos()
    End Function

    Public Function obtenerCombosPorCodigo(ByVal IdUsuario As Integer) As DataTable
        Return objAccesos.obtenerCombosPorCodigo(IdUsuario)
    End Function

    Public Function verificarExistencia(ByVal codCombo As String) As Boolean
        Return objAccesos.verificarExistencia(codCombo)
    End Function

    Public Function getFullArticulosxCodigoCombo(ByVal _codigo As String)
        Return New DaComboArticulos().getFullArticulosxCodigoCombo(_codigo)
    End Function

    Public Function getFullArticulosxNombreCombo(ByVal _nombre As String)
        Return New DaComboArticulos().getFullArticulosxNombreCombo(_nombre)
    End Function
End Class
