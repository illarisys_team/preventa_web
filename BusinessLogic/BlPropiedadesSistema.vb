﻿Imports DataAccess

Public Class BlPropiedadesSistema

    Dim objDaFuncionesGlobales As New DaPropiedadesSistema

    Public Function obtenerPropiedadesSistema() As Dictionary(Of String, String)
        Return objDaFuncionesGlobales.obtenerPropiedadesSistema
    End Function

End Class
