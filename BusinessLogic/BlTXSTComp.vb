﻿Imports DataAccess
Imports Entidades

Public Class BlTXSTComp
    Dim objDaTXSTComp As New DaTXSTComp

    Public Function insertarStockComprometidoTemporal(ByVal objEntTXSTComp As ETXSTComp) As String
        Return objDaTXSTComp.insertarStockComprometidoTemporal(objEntTXSTComp)
    End Function

    Public Function eliminarStockComprometidoTemporal(ByVal objEntTXSTComp As ETXSTComp) As String
        Return objDaTXSTComp.eliminarStockComprometidoTemporal(objEntTXSTComp)
    End Function

    Public Function eliminarListaStockComprometidoTemporal(ByVal objEntTXSTComp As ETXSTComp) As String
        Return objDaTXSTComp.eliminarListaStockComprometidoTemporal(objEntTXSTComp)
    End Function
End Class
