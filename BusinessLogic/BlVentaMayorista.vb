﻿Imports DataAccess

Public Class BlVentaMayorista

    Public Function obtenerStockDisponibleArticulo(ByVal intCodMaestroArt As Integer, ByVal intCodAlmacen As Integer,
                                                       ByVal intCodUbicacion As Integer, ByVal intCodEmpresa As Integer) As Integer
        Return New DaVentaMayorista().obtenerStockDisponibleArticulo(intCodMaestroArt, intCodAlmacen, intCodUbicacion, intCodEmpresa)
    End Function

End Class
