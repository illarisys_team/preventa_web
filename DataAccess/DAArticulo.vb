﻿Imports System.Data.SqlClient
Imports Entidades

Public Class DAArticulo

    Public Function getArticuloxCodigoImp(ByVal _codigo As String) As List(Of String)
        Dim oConexion As SqlConnection
        Dim oComandos As SqlCommand
        Dim reader As SqlDataReader
        Dim lista As New List(Of String)
        Dim strSql As String

        oConexion = New SqlConnection(CadenaConexion.CadenaConexionServer)
        oComandos = New SqlCommand()

        Try
            oConexion.Open()
            strSql = "SELECT TOP 20 [PK_MAESTRO_ARTICULOS] AS 'COD_MAESTRO',[COD_ART_IMPORT] AS 'CODIGO',[NOMBRE_VENTA] AS 'NOMBRE'" &
                "FROM [dbo].[TB_MAESTRO_ARTICULOS] WHERE [COD_ART_IMPORT] LIKE '%" & _codigo & "%'"
            oComandos.Connection = oConexion
            oComandos.CommandType = CommandType.Text
            oComandos.CommandText = strSql

            reader = oComandos.ExecuteReader()
            If reader.HasRows() Then
                While reader.Read()
                    lista.Add(reader("CODIGO"))
                End While
            End If
            reader.Close()
            Return lista

        Catch ex As Exception
            Throw ex
        Finally
            oConexion.Close()
        End Try
    End Function

    Public Function getComboxCodigo(ByVal _codigo As String) As List(Of String)
        Dim oConexion As SqlConnection
        Dim oComandos As SqlCommand
        Dim reader As SqlDataReader
        Dim lista As New List(Of String)
        Dim strSql As String

        oConexion = New SqlConnection(CadenaConexion.CadenaConexionServer)
        oComandos = New SqlCommand()

        Try
            oConexion.Open()
            strSql = "SELECT TOP 20 [PK_GRP_VENTA] AS 'COD_MAESTRO',[COD_CBO_VTA] AS 'CODIGO',[NOMBRE_AGRUPACION] AS 'NOMBRE' " &
                "FROM [TB_GRP_ARTICULOS] WHERE [COD_CBO_VTA] LIKE '%" & _codigo & "%'"
            oComandos.Connection = oConexion
            oComandos.CommandType = CommandType.Text
            oComandos.CommandText = strSql

            reader = oComandos.ExecuteReader()
            If reader.HasRows() Then
                While reader.Read()
                    lista.Add(reader("CODIGO"))
                End While
            End If
            reader.Close()
            Return lista

        Catch ex As Exception
            Throw ex
        Finally
            oConexion.Close()
        End Try
    End Function

    Public Function getArticuloxNombre(ByVal _nombre As String) As List(Of String)
        Dim oConexion As SqlConnection
        Dim oComandos As SqlCommand
        Dim reader As SqlDataReader
        Dim lista As New List(Of String)

        Dim strSql As String

        oConexion = New SqlConnection(CadenaConexion.CadenaConexionServer)
        oComandos = New SqlCommand()

        Try
            oConexion.Open()
            strSql = "SELECT TOP 20 [PK_MAESTRO_ARTICULOS] AS 'COD_MAESTRO',[COD_ART_IMPORT] AS 'CODIGO',[NOMBRE_VENTA] AS 'NOMBRE'" &
                "FROM [dbo].[TB_MAESTRO_ARTICULOS] WHERE [NOMBRE_VENTA] LIKE '%" & _nombre & "%'"
            oComandos.Connection = oConexion
            oComandos.CommandType = CommandType.Text
            oComandos.CommandText = strSql

            reader = oComandos.ExecuteReader()
            If reader.HasRows() Then
                While reader.Read()
                    lista.Add(reader("NOMBRE"))
                End While
            End If
            reader.Close()
            Return lista

        Catch ex As Exception
            Throw ex
        Finally
            oConexion.Close()
        End Try
    End Function


    Public Function getComboxNombre(ByVal _nombre As String) As List(Of String)
        Dim oConexion As SqlConnection
        Dim oComandos As SqlCommand
        Dim reader As SqlDataReader
        Dim lista As New List(Of String)

        Dim strSql As String

        oConexion = New SqlConnection(CadenaConexion.CadenaConexionServer)
        oComandos = New SqlCommand()

        Try
            oConexion.Open()
            strSql = "SELECT TOP 20 [PK_GRP_VENTA] AS 'COD_MAESTRO',[COD_CBO_VTA] AS 'CODIGO',[NOMBRE_AGRUPACION] AS 'NOMBRE' " &
                "FROM [TB_GRP_ARTICULOS] WHERE [NOMBRE_AGRUPACION] LIKE '%" & _nombre & "%'"
            oComandos.Connection = oConexion
            oComandos.CommandType = CommandType.Text
            oComandos.CommandText = strSql

            reader = oComandos.ExecuteReader()
            If reader.HasRows() Then
                While reader.Read()
                    lista.Add(reader("NOMBRE"))
                End While
            End If
            reader.Close()
            Return lista

        Catch ex As Exception
            Throw ex
        Finally
            oConexion.Close()
        End Try
    End Function

    Public Function getFullArticulosxCodigoImp(ByVal _codigo As String) As DataTable
        Dim oConexion As SqlConnection
        Dim oComandos As SqlCommand
        oConexion = New SqlConnection(CadenaConexion.CadenaConexionServer)
        oComandos = New SqlCommand()
        Try
            oConexion.Open()
            oComandos.Connection = oConexion
            oComandos.CommandType = CommandType.Text
            oComandos.CommandText = VariableArticulo.sqlGetConsolidado & " AND ma.COD_ART_IMPORT = '" & _codigo & "'"
            Dim oDataAdapter As New SqlDataAdapter(oComandos)
            Dim dt As New DataTable
            oDataAdapter.Fill(dt)
            Return getConsolidado(dt)
        Catch ex As Exception
            Throw ex
        Finally
            oConexion.Close()
        End Try
    End Function

    Public Function getFullArticulosxNombre(ByVal _nombre As String) As DataTable
        Dim oConexion As SqlConnection
        Dim oComandos As SqlCommand
        oConexion = New SqlConnection(CadenaConexion.CadenaConexionServer)
        oComandos = New SqlCommand()
        Try
            oConexion.Open()
            oComandos.Connection = oConexion
            oComandos.CommandType = CommandType.Text
            oComandos.CommandText = VariableArticulo.sqlGetConsolidado & " AND ma.NOMBRE_VENTA LIKE '" & _nombre & "'"
            Dim oDataAdapter As New SqlDataAdapter(oComandos)
            Dim dt As New DataTable
            oDataAdapter.Fill(dt)
            Return getConsolidado(dt)
        Catch ex As Exception
            Throw ex
        Finally
            oConexion.Close()
        End Try
    End Function

#Region "********************** CONSOLIDADO ARTICULO POR ALMACEN ***********************"
    Public Function getConsolidadoArticulosxEmpresaAlmacenUbicacion(ByVal hcConsolidado As HCConsolidado) As DataTable
        Dim oConexion As SqlConnection
        Dim oDataAdapter As SqlDataAdapter

        oConexion = New SqlConnection(CadenaConexion.CadenaConexionServer)
        Try
            oDataAdapter = New SqlDataAdapter("SP_CALC_DISPONIBLE_V2", oConexion)
            oDataAdapter.SelectCommand.CommandType = CommandType.StoredProcedure
            With oDataAdapter.SelectCommand.Parameters
                .Add("@FK_MAES_ART", SqlDbType.Int).Value = hcConsolidado.IDMaestroArticulo
                .Add("@FK_EMP", SqlDbType.Int).Value = hcConsolidado.IDEmpresa
            End With
            Dim dt As New DataTable
            oDataAdapter.Fill(dt)
            Return getConsolidadoxEmpresaAlmacen(dt, hcConsolidado)
        Catch ex As Exception
            Throw ex
        Finally
            oConexion.Close()
        End Try
    End Function

    Function getConsolidadoxEmpresaAlmacen(ByVal dtConsolidado As DataTable, ByVal hcConsolidado As HCConsolidado) As DataTable
        Dim cantRegistrosencontrados As Integer = 0
        Dim resultConsolidado As New DataTable
        Dim dStockDistribuido As Dictionary(Of String, Integer)
        Try
            resultConsolidado.Columns.Add(VariableArticulo.codEmpresa, GetType(String))
            resultConsolidado.Columns.Add(VariableArticulo.nombreEmpresa, GetType(String))
            resultConsolidado.Columns.Add(VariableArticulo.codEmpresaArticulo, GetType(String))
            resultConsolidado.Columns.Add(VariableArticulo.codMaestro, GetType(String))
            resultConsolidado.Columns.Add(VariableArticulo.codArticulo, GetType(String))
            resultConsolidado.Columns.Add(VariableArticulo.nombreArticulo, GetType(String))
            resultConsolidado.Columns.Add(VariableArticulo.txtPacking, GetType(String))
            resultConsolidado.Columns.Add(VariableArticulo.codAlmacen, GetType(String))
            resultConsolidado.Columns.Add(VariableArticulo.nombreAlmacen, GetType(String))
            resultConsolidado.Columns.Add(VariableArticulo.codUbicacion, GetType(String))
            resultConsolidado.Columns.Add(VariableArticulo.nombreUbicacion, GetType(String))
            resultConsolidado.Columns.Add(VariableArticulo.reposicion, GetType(String))
            resultConsolidado.Columns.Add("CAJA", GetType(String))
            resultConsolidado.Columns.Add("DOCENA", GetType(String))
            resultConsolidado.Columns.Add("UNIDAD", GetType(String))
            resultConsolidado.Columns.Add(VariableArticulo.codUMedidaPack, GetType(String))
            resultConsolidado.Columns.Add(VariableArticulo.nombreUMedida, GetType(String))
            resultConsolidado.Columns.Add(VariableArticulo.precioMinimo, GetType(String))
            resultConsolidado.Columns.Add(VariableArticulo.precioProm, GetType(String))
            resultConsolidado.Columns.Add(VariableArticulo.precioMax, GetType(String))
            resultConsolidado.Columns.Add(VariableArticulo.precioTienda, GetType(String))
            resultConsolidado.Columns.Add(VariableArticulo.precioCosto, GetType(String)) 'traer el costo del producto
            resultConsolidado.Columns.Add(VariableArticulo.precioCostoIgvUtil, GetType(String))
            resultConsolidado.Columns.Add(VariableArticulo.cantMayor, GetType(String))
            resultConsolidado.Columns.Add(VariableArticulo.cantPacking, GetType(String))
            resultConsolidado.Columns.Add(VariableArticulo.ipImpresora, GetType(String))

            For Each row As DataRow In dtConsolidado.Rows
                cantRegistrosencontrados = cantRegistrosencontrados + 1
                dStockDistribuido = distribuirStockPresentaciones(hcConsolidado.NombreUMedida, hcConsolidado.CantUPacking, row(VariableArticulo.stockDispxAlmacen))
                resultConsolidado.Rows.Add(hcConsolidado.IDEmpresa, hcConsolidado.NombreEmpresa, hcConsolidado.IDEmpresaArticulo, _
                                       hcConsolidado.IDMaestroArticulo, hcConsolidado.CodigoArticulo, hcConsolidado.NombreArticulo, hcConsolidado.TxtPacking, _
                                       row(VariableArticulo.codAlmacen), row(VariableArticulo.nombreAlmacen), row(VariableArticulo.codUbicacion), _
                                       row(VariableArticulo.nombreUbicacion), row(VariableArticulo.reposicion), _
                                       dStockDistribuido.Item("CAJA"), dStockDistribuido.Item("DOCENA"), dStockDistribuido.Item("UNIDAD"), _
                                       hcConsolidado.CodigoUMedidaPack, hcConsolidado.NombreUMedida, hcConsolidado.PrecioMin, hcConsolidado.PrecioProm, _
                                       hcConsolidado.PrecioMax, hcConsolidado.PrecioTienda, hcConsolidado.PrecioCosto, hcConsolidado.PrecioCostoIgvUtil, hcConsolidado.CantMayor, _
                                       hcConsolidado.CantUPacking, row(VariableArticulo.ipImpresora))
            Next
            Return resultConsolidado
        Catch ex As Exception
            Throw ex
        End Try
    End Function

#End Region

#Region "*********** RETORNAR EL CONSOLIDADO *********"
    Function getConsolidado(ByVal dtConsolidado As DataTable) As DataTable
        Dim cantRegistrosencontrados As Integer = 0
        Dim resultConsolidado As New DataTable
        Dim dStockDistribuido As Dictionary(Of String, Integer)
        Try
            resultConsolidado.Columns.Add(VariableArticulo.codEmpresaArticulo, GetType(String))
            resultConsolidado.Columns.Add(VariableArticulo.codEmpresa, GetType(String))
            resultConsolidado.Columns.Add(VariableArticulo.nombreEmpresa, GetType(String))
            resultConsolidado.Columns.Add(VariableArticulo.codMaestro, GetType(String))
            resultConsolidado.Columns.Add(VariableArticulo.codArticulo, GetType(String))
            resultConsolidado.Columns.Add(VariableArticulo.nombreArticulo, GetType(String))
            resultConsolidado.Columns.Add(VariableArticulo.txtPacking, GetType(String))
            resultConsolidado.Columns.Add("CAJA", GetType(String))
            resultConsolidado.Columns.Add("DOCENA", GetType(String))
            resultConsolidado.Columns.Add("UNIDAD", GetType(String))
            resultConsolidado.Columns.Add(VariableArticulo.nombreUMedida, GetType(String))
            resultConsolidado.Columns.Add(VariableArticulo.cantPacking, GetType(String))
            resultConsolidado.Columns.Add(VariableArticulo.codUMedidaPack, GetType(String))
            resultConsolidado.Columns.Add(VariableArticulo.precioMinimo, GetType(String))
            resultConsolidado.Columns.Add(VariableArticulo.precioProm, GetType(String))
            resultConsolidado.Columns.Add(VariableArticulo.precioMax, GetType(String))
            resultConsolidado.Columns.Add(VariableArticulo.precioCosto, GetType(String))
            resultConsolidado.Columns.Add(VariableArticulo.precioCostoIgvUtil, GetType(String))
            resultConsolidado.Columns.Add(VariableArticulo.precioTienda, GetType(String))
            resultConsolidado.Columns.Add(VariableArticulo.cantMayor, GetType(String))

            For Each row As DataRow In dtConsolidado.Rows
                cantRegistrosencontrados = cantRegistrosencontrados + 1
                dStockDistribuido = distribuirStockPresentaciones(row(VariableArticulo.nombreUMedida), row(VariableArticulo.cantPacking), row(VariableArticulo.stockDisponible))
                resultConsolidado.Rows.Add(row(VariableArticulo.codEmpresaArticulo), row(VariableArticulo.codEmpresa), row(VariableArticulo.nombreEmpresa), _
                                       row(VariableArticulo.codMaestro), row(VariableArticulo.codArticulo), row(VariableArticulo.nombreArticulo), row(VariableArticulo.txtPacking), _
                                       dStockDistribuido.Item("CAJA"), dStockDistribuido.Item("DOCENA"), dStockDistribuido.Item("UNIDAD"), _
                                       row(VariableArticulo.nombreUMedida), row(VariableArticulo.cantPacking), row(VariableArticulo.codUMedidaPack), row(VariableArticulo.precioMinimo), _
                                       row(VariableArticulo.precioProm), row(VariableArticulo.precioMax), row(VariableArticulo.precioCosto), row(VariableArticulo.precioCostoIgvUtil), _
                                       row(VariableArticulo.precioTienda),
                                       row(VariableArticulo.cantMayor))
            Next
            Return resultConsolidado
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function distribuirStockPresentaciones(ByVal pTextoUnidadPacking As String, ByVal pCantidadUnidadPacking As Integer, ByVal pCantidadUnidades As Integer) As Dictionary(Of String, Integer)
        'Para evitar los negativos en el informe
        If pCantidadUnidades < 0 Then
            pCantidadUnidades = pCantidadUnidades * -1
        End If
        Try
            Dim unidadPacking As Double = textoUnidadesPacking.Item(pTextoUnidadPacking) * pCantidadUnidadPacking
            Dim residuo As Double = pCantidadUnidades Mod unidadPacking
            Dim cajas As Integer = Int(pCantidadUnidades / unidadPacking)
            Dim docenas As Integer = IIf(residuo >= 12, Int(residuo / 12), 0)
            Dim unidad As Integer = IIf(docenas > 0, residuo - (docenas * 12), residuo)
            Dim resultado As New Dictionary(Of String, Integer)
            resultado.Add("CAJA", cajas)
            resultado.Add("DOCENA", docenas)
            resultado.Add("UNIDAD", unidad)

            Return resultado
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Protected textoUnidadesPacking As New Dictionary(Of String, Integer) From {{"SET", 1}, {"DOC", 12}, {"UND", 1}, {"DEC", 10}}

#End Region

    Public Function obtenerStockArticulosPorEmpresaAlmacenUbicacionDelKardex(ByVal idEmpresa As Integer, ByVal idMaestroArticulo As Integer, ByVal idAlmacen As Integer, ByVal idUbicacion As Integer) As Integer
        Dim oConexion As SqlConnection
        Dim oComandos As SqlCommand
        Dim reader As SqlDataReader
        Dim strSql As String
        oConexion = New SqlConnection(CadenaConexion.CadenaConexionServer)
        Try
            oConexion.Open()
            strSql = "SELECT SUM(KG.CANTIDADES) as 'STOCK' FROM TB_KARDEX_GENERAL as kg where kg.FK_MAESTRO_ARTICULOS = " _
                & idMaestroArticulo & " AND kg.FK_EMPRESA = " & idEmpresa & " and kg.FK_ALMACEN = " & idAlmacen & _
                " AND kg.FK_UBICACION = " & idUbicacion

            oComandos = New SqlCommand(strSql, oConexion)
            oComandos.CommandType = CommandType.Text
            reader = oComandos.ExecuteReader()

            Dim stock As Integer

            If reader.Read() Then
                stock = reader("stock").ToString
                Return stock
            Else
                Return 0
            End If
            reader.Close()

        Catch ex As Exception
            Throw ex
        Finally
            oConexion.Close()
        End Try
    End Function

    Public Function obtenerPreciosArticulo(ByVal intCodMaestroArt As Integer) As EArticulo
        Dim oConexion As SqlConnection
        Dim oComandos As SqlCommand
        Dim reader As SqlDataReader
        Dim objEntArticulo As New EArticulo
        Dim strSql As String

        oConexion = New SqlConnection(CadenaConexion.CadenaConexionServer)
        oComandos = New SqlCommand()

        Try
            oConexion.Open()

            strSql = "SELECT PK_MAESTRO_ARTICULOS, IIF(PRECIO_COSTO IS NULL,0,PRECIO_COSTO) AS 'PRECIO_COSTO', " + _
                     "IIF(TIPO_CAMBIO IS NULL,0,TIPO_CAMBIO) AS 'TIPO_CAMBIO', " + _
                     "IIF(PRECIO_COSTO_COTIZACION IS NULL,0,PRECIO_COSTO_COTIZACION) AS 'PRECIO_COSTO_COTIZACION', " + _
                     "PRECIO_MAX, PRECIO_PROM, PRECIO_MINIMO, PRECIO_TIENDA, DSCTO_CATEG, DSCTO_COND_PAGO, DSCTO_VOL, DSCTO_CANT " + _
                     "FROM TB_MAESTRO_ARTICULOS " + _
                     "WHERE PK_MAESTRO_ARTICULOS = " & intCodMaestroArt & ""
            oComandos.Connection = oConexion
            oComandos.CommandType = CommandType.Text
            oComandos.CommandText = strSql

            reader = oComandos.ExecuteReader()

            If reader.Read() Then
                objEntArticulo._pk_articulo = reader("PK_MAESTRO_ARTICULOS").ToString()
                objEntArticulo._precio_costo = reader("PRECIO_COSTO").ToString() 'Costo de Factura en Comp. Nacional e Importación (Con IGV y DOLARES) 
                objEntArticulo._tipo_cambio = reader("TIPO_CAMBIO").ToString()
                objEntArticulo._precio_costo_cotizacion = reader("PRECIO_COSTO_COTIZACION").ToString 'Costo de Cotización - Comp. Nacional
                objEntArticulo._precio_maximo = reader("PRECIO_MAX").ToString()
                objEntArticulo._precio_promedio = reader("PRECIO_PROM").ToString()
                objEntArticulo._precio_minimo = reader("PRECIO_MINIMO").ToString()
                objEntArticulo._precio_tienda = reader("PRECIO_TIENDA").ToString()
                objEntArticulo._desc_categoria = reader("DSCTO_CATEG").ToString()
                objEntArticulo._desc_cond_pago = reader("DSCTO_COND_PAGO").ToString()
                objEntArticulo._desc_volumen = reader("DSCTO_VOL").ToString()
                objEntArticulo._desc_cantidad = reader("DSCTO_CANT").ToString()
            End If

            reader.Close()
        Catch ex As Exception
            Throw ex
        Finally
            oComandos.Dispose()
            oConexion.Close()
        End Try
        Return objEntArticulo
    End Function

#Region "OBTENER LOS ARTICULOS POR COMBO"
    Public Function obtenerComboActivo(ByVal codCombo As String) As DataTable
        Dim dt As New DataTable
        Dim oConexion As SqlConnection
        Dim oComandos As SqlCommand
        Dim strSqlCombo As String = ""

        oConexion = New SqlConnection(CadenaConexion.CadenaConexionServer)
        oComandos = New SqlCommand

        Try
            oConexion.Open()
            strSqlCombo = "SELECT DGA.FK_MAESTRO_ARTICULOS, DGA.FK_EMPR_ARTC, DGA.FK_EMPRESA, EM.NOMBRE_EMPRESA, EM.DIRECCION, " + _
                          "EM.NIF, EM.TELEF1, MA.COD_ART_IMPORT, MA.NOMBRE_VENTA, MA.TXT_PACKING, DGA.P_VTA_CBO, DGA.P_VTA_CBO," + _
                          "DGA.CANT_VTA, DGA.P_COSTO, GA.PK_GRP_VENTA, MA.CANT_PACKING, MA.CANT_MAYOR, UM.NOMBRE_UND " + _
                          "FROM TB_GRP_ARTICULOS GA " + _
                          "INNER JOIN TB_DET_GRP_ARTICULOS DGA ON GA.PK_GRP_VENTA=DGA.FK_GRP_VTA " + _
                          "INNER JOIN TB_MAESTRO_ARTICULOS MA ON DGA.FK_MAESTRO_ARTICULOS=MA.PK_MAESTRO_ARTICULOS " + _
                          "INNER JOIN TB_UND_MEDIDA UM ON MA.UMD_PACKING=UM.PK_UND " + _
                          "INNER JOIN TB_EMPRESAS EM ON DGA.FK_EMPRESA=EM.PK_EMPRESAS " + _
                          "WHERE GA.FK_ESTADO=211 AND GA.COD_CBO_VTA='" & codCombo & "'"
            oComandos.Connection = oConexion
            oComandos.CommandType = CommandType.Text
            oComandos.CommandText = strSqlCombo

            Dim oDataAdapter As New SqlDataAdapter(oComandos)
            oDataAdapter.Fill(dt)
        Catch ex As Exception
            Throw ex
        Finally
            oConexion.Dispose()
            oConexion.Close()
        End Try
        Return dt
    End Function

#End Region
End Class