﻿Imports System.Data.SqlClient
Imports Entidades

Public Class DACliente

    Public Function obtenerCliente() As DataTable

        Dim oConexion As SqlConnection
        Dim oComandos As SqlCommand
        Dim strSql As String
        oConexion = New SqlConnection(CadenaConexion.CadenaConexionServer)
        oComandos = New SqlCommand()

        Try
            oConexion.Open()
            'strSql = "SELECT CLI.PK_CLIENTE as 'CODIGO' ,CLI.NOMBRE_CORP AS 'CORPORACION' ,CLI.NOMBRE_COMERCIAL,CLI.DNI , CLI.RAZON_SOCIAL, CLI.NIF AS 'RUC'  ,CLI.NOMBRE_CONTACTO, CLI.DPTO, CLI.DISTRITO ,CLI.EMAIL_1 ,CLI.TELEF1 ,CLI.TELEF2 ,CLI.FAX ,CLI.WEB_SITE ,CLI.LINEA_CREDITO ,CLI.FK_LINEA ,CLI.NOMBRE_LINEA  ,CLI.DESCRIPCION_ESTADO, CLI.TRATAMIENTO,CTA_AGRP FROM TB_MAESTRO_CLIENTES AS CLI "
            strSql = "SELECT DISTINCT CLI.PK_CLIENTE as 'CODIGO' ,CLI.NOMBRE_CORP AS 'CORPORACION' ,CLI.NOMBRE_COMERCIAL,CLI.DNI , CLI.RAZON_SOCIAL, CLI.NIF AS 'RUC'  ,CLI.NOMBRE_CONTACTO, CLI.DPTO, CLI.DISTRITO ,CLI.EMAIL_1 ,CLI.TELEF1 ,CLI.TELEF2 ,CLI.FAX ,CLI.WEB_SITE ,CLI.LINEA_CREDITO ,CLI.FK_LINEA ,CLI.NOMBRE_LINEA  ,CLI.FK_ESTADO ,CLI.DESCRIPCION_ESTADO, CLI.TRATAMIENTO,CTA_AGRP,DE.TXT_DIRECC ,DE.TIPO_DIRECC FROM TB_MAESTRO_CLIENTES AS CLI INNER JOIN TB_DIREC_EMPRESAS DE ON CLI.PK_CLIENTE=DE.FK_EMPRESA WHERE DE.TIPO_DIRECC='DIRECCION PRINCIPAL'"
            oComandos.Connection = oConexion
            oComandos.CommandType = CommandType.Text
            oComandos.CommandText = strSql

            Dim oDataAdapter As New SqlDataAdapter(oComandos)
            Dim dt As New DataTable
            oDataAdapter.Fill(dt)
            Return dt

        Catch ex As Exception
            Throw ex
        Finally
            oConexion.Close()
        End Try
    End Function

    Public Function getClienteXDocumento(ByVal _documento As String) As List(Of String)
        Dim oConexion As SqlConnection
        Dim oComandos As SqlCommand
        Dim reader As SqlDataReader
        Dim lista As New List(Of String)

        Dim strSql As String

        oConexion = New SqlConnection(CadenaConexion.CadenaConexionServer)
        oComandos = New SqlCommand()

        Try
            oConexion.Open()
            strSql = "SELECT TOP 10 NOMBRE_COMERCIAL AS 'NOMBRE', IIF(DNI = '',NIF,DNI) AS 'DOCUMENT' " &
                "FROM TB_MAESTRO_CLIENTES WHERE  DNI LIKE '" & _documento & "%' OR NIF LIKE '" & _documento & "%'"
            oComandos.Connection = oConexion
            oComandos.CommandType = CommandType.Text
            oComandos.CommandText = strSql

            reader = oComandos.ExecuteReader()
            If reader.HasRows() Then
                While reader.Read()
                    lista.Add(reader("DOCUMENT") + "-" + reader("NOMBRE"))
                End While
            End If
            reader.Close()
            Return lista

        Catch ex As Exception
            Throw ex
        Finally
            oConexion.Close()
        End Try
    End Function

    Public Function obtenerCliente_PFV_DNI(ByVal dni As String) As EClientes
        Dim oConexion As SqlConnection
        Dim oDataAdapter As SqlDataAdapter
        Dim readerClienteDNI As SqlDataReader
        Dim oClientes As EClientes = Nothing

        oConexion = New SqlConnection(CadenaConexion.CadenaConexionServer)

        Try
            oConexion.Open()
            oDataAdapter = New SqlDataAdapter("SP_LISTA_CLIENTE_X_DNI", oConexion)
            oDataAdapter.SelectCommand.CommandType = CommandType.StoredProcedure
            With oDataAdapter.SelectCommand.Parameters
                .Add("@DNI", SqlDbType.VarChar, 50).Value = dni
            End With

            readerClienteDNI = oDataAdapter.SelectCommand.ExecuteReader

            If readerClienteDNI.Read() Then
                oClientes = New EClientes()
                oClientes._pk_cliente = readerClienteDNI("CODIGO").ToString
                oClientes._nombre_comercial = readerClienteDNI("NOMBRE_COMERCIAL").ToString
                oClientes._dni = readerClienteDNI("DNI").ToString
                oClientes._razon_social = readerClienteDNI("RAZON_SOCIAL").ToString
                oClientes._nif = readerClienteDNI("RUC").ToString
                oClientes._linea_credito = readerClienteDNI("LINEA_CREDITO").ToString
                oClientes._nombre_linea = readerClienteDNI("NOMBRE_LINEA").ToString
                oClientes._fk_estado = readerClienteDNI("FK_ESTADO").ToString
                oClientes._descripcion_estado = readerClienteDNI("DESCRIPCION_ESTADO").ToString
                oClientes._CTA_AGRP = readerClienteDNI("CTA_AGRP").ToString
                oClientes._txt_direcc = readerClienteDNI("TXT_DIRECC").ToString
                oClientes._tipo_direcc = readerClienteDNI("TIPO_DIRECC").ToString
            End If

            Return oClientes
        Catch ex As Exception
            Throw ex
        Finally
            oConexion.Close()
        End Try
    End Function

    Public Function obtenerCliente_PFV_RUC(ByVal ruc As String) As EClientes
        Dim oConexion As SqlConnection
        Dim oDataAdapter As SqlDataAdapter
        Dim readerClienteRUC As SqlDataReader
        Dim oClientes As EClientes = Nothing

        oConexion = New SqlConnection(CadenaConexion.CadenaConexionServer)

        Try
            oConexion.Open()
            oDataAdapter = New SqlDataAdapter("SP_LISTA_CLIENTE_X_RUC", oConexion)
            oDataAdapter.SelectCommand.CommandType = CommandType.StoredProcedure

            With oDataAdapter.SelectCommand.Parameters
                .Add("@RUC", SqlDbType.VarChar, 12).Value = ruc
            End With

            readerClienteRUC = oDataAdapter.SelectCommand.ExecuteReader

            If readerClienteRUC.Read() Then
                oClientes = New EClientes()
                oClientes._pk_cliente = readerClienteRUC("CODIGO").ToString
                oClientes._nombre_comercial = readerClienteRUC("NOMBRE_COMERCIAL").ToString
                oClientes._dni = readerClienteRUC("DNI").ToString
                oClientes._razon_social = readerClienteRUC("RAZON_SOCIAL").ToString
                oClientes._nif = readerClienteRUC("RUC").ToString
                oClientes._linea_credito = readerClienteRUC("LINEA_CREDITO").ToString
                oClientes._nombre_linea = readerClienteRUC("NOMBRE_LINEA").ToString
                oClientes._fk_estado = readerClienteRUC("FK_ESTADO").ToString
                oClientes._descripcion_estado = readerClienteRUC("DESCRIPCION_ESTADO").ToString
                oClientes._CTA_AGRP = readerClienteRUC("CTA_AGRP").ToString
                oClientes._txt_direcc = readerClienteRUC("TXT_DIRECC").ToString
                oClientes._tipo_direcc = readerClienteRUC("TIPO_DIRECC").ToString
            End If

            Return oClientes
        Catch ex As Exception
            Throw ex
        Finally
            oConexion.Close()
        End Try
    End Function

    Public Function existeClientePorDoc(ByVal tipoDocumento As String, ByVal numeroDocumento As String) As Boolean
        If numeroDocumento = "" Then
            Return False
        End If
        Dim oConexion As SqlConnection
        Dim oComandos As SqlCommand
        Dim reader As SqlDataReader
        Dim strSql As String
        oConexion = New SqlConnection(CadenaConexion.CadenaConexionServer)
        oComandos = New SqlCommand()

        Try
            oConexion.Open() '1 RUc, 2 DNI
            If tipoDocumento = "J" Then
                strSql = "SELECT NIF FROM TB_MAESTRO_CLIENTES WHERE NIF = '" & numeroDocumento & "'"
            Else
                strSql = "SELECT DNI FROM TB_MAESTRO_CLIENTES WHERE DNI = '" & numeroDocumento & "'"
            End If

            oComandos.Connection = oConexion
            oComandos.CommandType = CommandType.Text
            oComandos.CommandText = strSql
            reader = oComandos.ExecuteReader()

            If reader.Read() Then
                Return True
            Else
                Return False
            End If
            reader.Close()
        Catch ex As Exception
            Throw ex
        Finally
            oConexion.Close()
        End Try
    End Function

    Public Function InsertarCliente(ByVal objEntCliente As EClientes) As String

        Dim oConexion As SqlConnection
        Dim oComandos As SqlCommand
        Dim strSql As String
        Dim strSqlDireciones As String
        Dim VD As String
        Dim sqlTransac As SqlTransaction
        oConexion = New SqlConnection(CadenaConexion.CadenaConexionServer)
        oComandos = New SqlCommand()
        oConexion.Open()
        sqlTransac = oConexion.BeginTransaction

        Try
            strSql = "INSERT INTO TB_MAESTRO_CLIENTES (FK_CORP,NOMBRE_CORP, NOMBRE_COMERCIAL, NIF, DNI, RAZON_SOCIAL, " &
                "TRATAMIENTO, NOMBRE_CONTACTO, DPTO, FK_DPTO, DISTRITO, FK_DISTRITO, EMAIL_1, TELEF1, TELEF2, FAX, " &
                "WEB_SITE, LINEA_CREDITO, FK_LINEA, NOMBRE_LINEA, FK_ESTADO, DESCRIPCION_ESTADO, TIPO_CLIENTE, " &
                "FLAG_INTERNO,FK_PROV,TXT_PROV,CTA_AGRP) VALUES (" & objEntCliente._fk_corp & ",'" & objEntCliente._nombre_corp & "','" &
                objEntCliente._nombre_comercial & "','" & objEntCliente._nif & "','" & objEntCliente._dni & "','" & objEntCliente._razon_social & "','" &
                objEntCliente._tratamiento & "','" & objEntCliente._nombre_contacto & "','" & objEntCliente._dpto & "'," &
                objEntCliente._fk_departamento & ",'" & objEntCliente._distrito & "'," & objEntCliente._fk_distrito & ",'" &
                objEntCliente._email & "','" & objEntCliente._telef1 & "','" & objEntCliente._telef2 & "','" & objEntCliente._fax & "','" &
                objEntCliente._website & "'," & objEntCliente._linea_credito & ", " & objEntCliente._fk_linea & ",' " & objEntCliente._nombre_linea & "'," &
                objEntCliente._fk_estado & ",'" & objEntCliente._descripcion_estado & "','" & objEntCliente._tipo_cliente & "','" &
                objEntCliente._flag_interno & "','" & objEntCliente._fk_prov & "','" & objEntCliente._txt_prov & "','" & objEntCliente._CTA_AGRP & "'); SELECT @@IDENTITY"

            oComandos.Connection = oConexion
            oComandos.CommandType = CommandType.Text
            oComandos.CommandText = strSql
            oComandos.Transaction = sqlTransac
            Dim id As Integer = oComandos.ExecuteScalar()

            For Each item As Entidades.EDirecEmpresas In objEntCliente._direcciones
                strSqlDireciones = "INSERT INTO TB_DIREC_EMPRESAS VALUES ('" & id & "','" & item._TXT_DIRECC & "','" &
                    item._ESTADO & "','" & item._TIPOD_DIRECC & "')"
                oComandos.CommandText = strSqlDireciones
                oComandos.Transaction = sqlTransac
                oComandos.ExecuteNonQuery()
            Next item
            sqlTransac.Commit()
            VD = "OK"
        Catch ex As Exception
            sqlTransac.Rollback()
            VD = ex.Message.ToString
        Finally
            oConexion.Close()
        End Try
        Return VD
    End Function
End Class
