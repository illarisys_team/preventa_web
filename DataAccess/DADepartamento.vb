﻿Imports System.Data.SqlClient

Public Class DADepartamento
    Public Function cargarComboDepartamento() As DataTable

        Dim oConexion As SqlConnection
        Dim oComandos As SqlCommand

        Dim strSql As String

        oConexion = New SqlConnection(CadenaConexion.CadenaConexionServer)
        oComandos = New SqlCommand()

        Try
            oConexion.Open()

            strSql = "SELECT PK_dpto AS 'CODIGO', nombre_dpto AS 'DESCRIPCION' FROM TB_Departamento"

            oComandos.Connection = oConexion
            oComandos.CommandType = CommandType.Text
            oComandos.CommandText = strSql

            Dim oDataAdapter As New SqlDataAdapter(oComandos)
            Dim dt As New DataTable

            oDataAdapter.Fill(dt)

            Return dt

        Catch ex As Exception
            Throw ex
        Finally
            oConexion.Close()
        End Try
    End Function
End Class
