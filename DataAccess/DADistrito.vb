﻿Imports System.Data.SqlClient

Public Class DADistrito
    Public Function obtenerDistritoxProvincia(ByVal pkProvincia As Integer, ByVal fkdpto As Integer) As DataTable
        Dim oConexion As SqlConnection
        Dim oComandos As SqlCommand
        Dim strSql As String

        oConexion = New SqlConnection(CadenaConexion.CadenaConexionServer)
        oComandos = New SqlCommand

        Try
            oConexion.Open()
            strSql = "SELECT pk_distr AS 'CODIGO', txt_descr AS 'DESCRIPCION' FROM TB_distr WHERE fk_prov='" & pkProvincia & "' AND fk_dpto='" & fkdpto & "'"
            oComandos.Connection = oConexion
            oComandos.CommandType = CommandType.Text
            oComandos.CommandText = strSql

            Dim oDataAdapter As New SqlDataAdapter(oComandos)
            Dim dt As New DataTable
            oDataAdapter.Fill(dt)

            Return dt

        Catch ex As Exception
            Throw ex
        Finally
            oConexion.Close()

        End Try
    End Function
End Class
