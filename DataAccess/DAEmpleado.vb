﻿Imports System.Data.SqlClient
Imports Entidades

Public Class DAEmpleado

    Public Function obtenerEmpleadoPorUserName(ByVal strUsuario As String, strClave As String) As EEmpleado
        'MLUYO mas na....!
        'Función para obtener el empleado por usuario.
        Dim oConexion As SqlConnection
        Dim oComandos As SqlCommand
        Dim reader As SqlDataReader
        Dim objEntEmpleado As New EEmpleado
        Dim strSql As String

        oConexion = New SqlConnection(CadenaConexion.CadenaConexionServer)
        oComandos = New SqlCommand()

        Try
            oConexion.Open()
            'strSql = "SELECT PK_EMPLEADO, USUARIO, CLAVE,FK_ESTADO,CAMB_PASS FROM TB_EMPLEADOS AS TE WHERE USUARIO='" & strUsuario & "'"
            strSql = "SELECT PK_EMPLEADO,NOMBRES,APELLIDOS,USUARIO, CLAVE,FK_ESTADO,CAMB_PASS FROM TB_EMPLEADOS AS TE WHERE USUARIO='" & strUsuario & "' AND CLAVE=(SELECT HASHBYTES('SHA1','" & strClave & "'))"
            oComandos.Connection = oConexion
            oComandos.CommandType = CommandType.Text
            oComandos.CommandText = strSql

            reader = oComandos.ExecuteReader()
            If reader.Read() Then
                objEntEmpleado._PK_EMPLEADO = reader("PK_EMPLEADO").ToString
                objEntEmpleado._USUARIO = reader("USUARIO").ToString
                objEntEmpleado._NOMBRES = reader("NOMBRES").ToString
                objEntEmpleado._APELLIDOS = reader("APELLIDOS").ToString
                objEntEmpleado._USUARIO = reader("USUARIO").ToString
                objEntEmpleado._CLAVE = reader("CLAVE").ToString
                objEntEmpleado._FK_ESTADO = reader("FK_ESTADO")
                objEntEmpleado._CAMB_PASS = reader("CAMB_PASS")
            End If

            reader.Close()
            Return objEntEmpleado
        Catch ex As Exception
            Throw ex
        Finally
            oConexion.Close()
        End Try
    End Function

End Class
