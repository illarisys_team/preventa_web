﻿Imports System.Data.SqlClient
Imports Entidades

Public Class DAPreVenta

#Region "**************** HELPER PREVENTA ***************"
    Function GrabarHelperDetPreVenta(ByVal oHelperPreVenta As EHelperPreVenta) As Integer
        Dim oConexion As SqlConnection
        Dim command As SqlCommand
        Dim result As Integer = 0
        oConexion = New SqlConnection(CadenaConexion.CadenaConexionServer)
        Try
            command = New SqlCommand("SP_HELPER_PRE_VENTA_INS", oConexion)
            command.CommandType = CommandType.StoredProcedure
            With command.Parameters
                .Add("@PK_EMPLEADO", SqlDbType.Int).Value = oHelperPreVenta._FK_EMPLEADO
                .Add("@ORDEN_ITEM", SqlDbType.Int).Value = oHelperPreVenta._ORDEN_ITEM
                .Add("@FK_MAESTRO", SqlDbType.Int).Value = oHelperPreVenta._FK_MAESTRO
                .Add("@FK_EMP_ART", SqlDbType.Int).Value = oHelperPreVenta._FK_EMP_ART
                .Add("@COD_ARTICULO", SqlDbType.NVarChar).Value = oHelperPreVenta._COD_ART_IMPORT
                .Add("@DESC_ARTICULO", SqlDbType.NVarChar).Value = oHelperPreVenta._NOMBRE_VENTA
                .Add("@FK_EMPRESA", SqlDbType.Int).Value = oHelperPreVenta._FK_EMPRESA
                .Add("@DESC_EMPRESA", SqlDbType.NVarChar).Value = oHelperPreVenta._DESC_EMPRESA
                .Add("@FK_ALMACEN", SqlDbType.Int).Value = oHelperPreVenta._FK_ALMACEN
                .Add("@DESC_ALMACEN", SqlDbType.NVarChar).Value = oHelperPreVenta._DESC_ALMACEN
                .Add("@FK_UBICACION", SqlDbType.Int).Value = oHelperPreVenta._FK_UBICACION
                .Add("@DESC_UBICACION", SqlDbType.NVarChar).Value = oHelperPreVenta._DESC_UBICACION
                .Add("@UND_TOTAL_PEDIDA", SqlDbType.Int).Value = oHelperPreVenta._UNIDADES_TOTAL_PEDIDAS
                .Add("@CAJAS", SqlDbType.Int).Value = oHelperPreVenta._CAJAS_PEDIDAS
                .Add("@DOCENA", SqlDbType.Int).Value = oHelperPreVenta._DOCENAS_PEDIDAS
                .Add("@UNIDAD_SET", SqlDbType.Int).Value = oHelperPreVenta._UNIDADES_PEDIDAS
                .Add("@P_COSTO", SqlDbType.Decimal).Value = oHelperPreVenta._PRECIO_COSTO
                .Add("@P_COSTO_IGV_K", SqlDbType.Decimal).Value = oHelperPreVenta._PRECIO_COSTO_IGV_K
                .Add("@P_PRECIO", SqlDbType.Decimal).Value = oHelperPreVenta._PRECIO
                .Add("@P_TOTAL", SqlDbType.Decimal).Value = oHelperPreVenta._PRECIO_TOTAL
                .Add("@DESCUENTO", SqlDbType.Bit).Value = oHelperPreVenta._ST_DSCTO
                .Add("@DESC_PACKING", SqlDbType.NVarChar).Value = oHelperPreVenta._DESC_PACKING
                .Add("@CANT_PACKING", SqlDbType.Int).Value = oHelperPreVenta._CAN_PACKING
                .Add("@CANT_MAYOR", SqlDbType.Int).Value = oHelperPreVenta._CAN_MAYOR
                .Add("@NUEVO_REGISTRO", SqlDbType.NVarChar).Value = oHelperPreVenta._NUEVO_REGISTRO
                .Add("@COMENTARIO", SqlDbType.NVarChar).Value = oHelperPreVenta._COMENTARIO
                .Add("@IP_IMPR", SqlDbType.NVarChar).Value = oHelperPreVenta._IP_IMPRESORA 'check this
                .Add("@PK_GRP_VENTA", SqlDbType.BigInt).Value = oHelperPreVenta._PK_GRP_VENTA
            End With
            oConexion.Open()
            result = command.ExecuteScalar()
        Catch ex As Exception
            Throw ex
        Finally
            oConexion.Close()
        End Try
        Return result
    End Function

    Public Function ObtenerHelperDetPreVenta(ByVal _empleado_Id As String) As DataTable
        Dim oConexion As SqlConnection
        Dim command As SqlCommand
        oConexion = New SqlConnection(CadenaConexion.CadenaConexionServer)
        Try
            oConexion.Open()
            command = New SqlCommand("SP_HELPER_PRE_VENTA_SEL", oConexion)
            command.CommandType = CommandType.StoredProcedure
            command.Parameters.Add("@PK_EMPLEADO", SqlDbType.Int).Value = _empleado_Id
            Dim oDataAdapter As New SqlDataAdapter(command)
            Dim dt As New DataTable
            oDataAdapter.Fill(dt)
            Return dt
        Catch ex As Exception
            Throw ex
        Finally
            oConexion.Close()
        End Try
    End Function

    Public Function EliminarHelperDetPreVenta(ByVal _empleado_Id As Integer, ByVal _maestro_id As Integer) As Integer
        Dim oConexion As SqlConnection
        Dim command As SqlCommand
        Dim resultado As Integer = 0
        oConexion = New SqlConnection(CadenaConexion.CadenaConexionServer)
        Try
            oConexion.Open()
            command = New SqlCommand("SP_HELPER_PRE_VENTA_DEL", oConexion)
            command.CommandType = CommandType.StoredProcedure
            command.Parameters.Add("@PK_EMPLEADO", SqlDbType.Int).Value = _empleado_Id
            command.Parameters.Add("@FK_MAESTRO", SqlDbType.Int).Value = _maestro_id
            resultado = command.ExecuteScalar()

        Catch ex As Exception
            Throw ex
        Finally
            oConexion.Close()
        End Try
        Return resultado
    End Function
#End Region

#Region "**************** PREVENTA ***************"
    Public Function ObtenerCorrelativoNumPreVenta(ByVal clasePedido As String) As String
        Dim oConexion As SqlConnection = New SqlConnection(CadenaConexion.CadenaConexionServer)
        Dim command As SqlCommand
        Dim numero As String
        Try
            command = New SqlCommand("SP_GENERAR_NUM_PRE_VTA", oConexion)
            command.CommandType = CommandType.StoredProcedure
            command.Parameters.Add("@CL_PEDIDO", SqlDbType.VarChar, 1).Value = clasePedido.ToString
            oConexion.Open()
            numero = command.ExecuteScalar()
            Return numero
        Catch ex As Exception
            Throw ex
        Finally
            oConexion.Close()
        End Try
    End Function

    Public Function InsertarPreVenta(ByVal oEPreVenta As EPreVenta) As String

        'Inicializamos las variables de tipo SQL Client'
        Dim oConexion As SqlConnection = New SqlConnection(CadenaConexion.CadenaConexionServer)
        Dim command As SqlCommand = New SqlCommand
        Dim strSql As String
        Dim strDetallePreVenta As String
        Dim resultado As String
        Dim strSqlTxStComp As String
        Dim strUpdateStockComprometido As String
        Dim strUpdateStockDisponible As String
        Dim strSqlXDeleteTxStComp As String = ""
        Dim transaction As SqlTransaction
        oConexion.Open()

        transaction = oConexion.BeginTransaction()

        Dim dblImpTotal As Decimal = 0.0

        dblImpTotal = Format(oEPreVenta._imp_tot_igv, "Standard")

        Try
            strSql = "INSERT INTO TB_PR_VTA(num_pre_vta,fk_cliente,fk_cond_pago,fk_emp_vend,fk_alm_desp,fk_moned,fk_tipo_camb,fk_tip_doc_vta,tip_doc_cli," + _
                                    "ruc_dni,direc_entreg,fech_crea,fech_desp,usu_crea,obs,est_doc,imp_tot_igv,fk_linea_cred,st_dscto)" + _
                                    " VALUES('" & oEPreVenta._num_pre_vta & "','" & oEPreVenta._fk_cliente & "','" & oEPreVenta._fk_cond_pago & "'," + _
                                    "'" & oEPreVenta._fk_emp_vend & "','" & oEPreVenta._fk_alm_desp & "','" & oEPreVenta._fk_moned & "'," + _
                                    "'" & oEPreVenta._fk_tipo_camb & "','" & oEPreVenta._fk_tip_doc_vta & "','" & oEPreVenta._tip_doc_cli & "'," + _
                                    "'" & oEPreVenta._ruc_dni & "','" & oEPreVenta._direc_entreg & "','" & oEPreVenta._fech_crea & "'," + _
                                    "'" & oEPreVenta._fech_desp & "','" & oEPreVenta._usu_crea & "','" & oEPreVenta._obs & "'," + _
                                    "'" & oEPreVenta._est_doc & "'," & dblImpTotal & ",'" & oEPreVenta._fk_linea_cred & "','" & oEPreVenta._st_dscto & "'); SELECT @@IDENTITY "

            command.Connection = oConexion
            command.CommandType = CommandType.Text
            command.CommandText = strSql
            command.Transaction = transaction

            Dim id As Integer = command.ExecuteScalar()
            Dim cantidadUnidadesSolicitadas As Integer = 0

            Dim dblPCosto As Decimal = 0.0
            Dim dblPCostoIgvK As Decimal = 0.0
            Dim dblPVta As Decimal = 0.0
            Dim dblPVtaMod As Decimal = 0.0
            Dim dblPTotal As Decimal = 0.0

            For Each item As Entidades.EDetallePreVenta In oEPreVenta._detallePreVenta

                'Se llenan los valores decimales'
                dblPCosto = Format(item._P_COSTO, "Standard")
                dblPCostoIgvK = Format(item._P_COSTO_IGV_K, "Standard")
                dblPVta = Format(item._P_VTA, "Standard")
                dblPVtaMod = Format(item._P_VTA_MOD, "Standard")
                dblPTotal = Format(item._P_TOTAL, "Standard")

                strDetallePreVenta = "INSERT INTO TB_DET_PRE_VTA(ITEM, FK_PRE_VTA,EST_POS,FK_MA, FK_EMP_ART, FK_EMPR, FK_ALM, FK_UBIC, UND_TOT, CAJAS, DOC, UND_SET, P_COSTO, P_COSTO_IGV_K, P_VTA,P_VTA_MOD,ST_DSCTO,P_TOTAL,COMENTARIO,UND_FALT,NEW_CAJA,NEW_DOC,NEW_UND,NEW_UND_TOT,NEW_UND_TOT_2,CANT_IMPR,PK_GRP_VENTA)" + _
                                    "VALUES('" & item._ITEM & "','" & id & "'," & item._EST_POS & ",'" & item._FK_MAESTRO & "','" & item._FK_EMP_ART & "'," + _
                                    "'" & item._FK_EMPR & "','" & item._FK_ALM & "','" & item._FK_UBIC & "','" & item._UND_TOT & "','" & item._CAJAS & "','" & item._DOC & "'," + _
                                    "'" & item._UND_SET & "'," & dblPCosto & "," & dblPCostoIgvK & "," & dblPVta & "," & dblPVtaMod & ",'" & item._ST_DSCTO & "', " & dblPTotal & " ,'" & item._COMENTARIO & " ',0,0,0,0,0," & item._UND_TOT & "," & item._PK_GRP_VENTA & ",0 ); SELECT @@IDENTITY"
                cantidadUnidadesSolicitadas = cantidadUnidadesSolicitadas + (item._UND_TOT)
                command.CommandText = strDetallePreVenta
                command.Transaction = transaction

                Dim pkDet As Integer = command.ExecuteScalar

                'GSILVA'
                '18/12/2014'
                '-------------ELIMINAMOS EL STOCK COMPROMETIDO TEMPORAL DE LA TABLA TB_TX_ST_COMP'
                strSqlXDeleteTxStComp = "DELETE FROM TB_TX_ST_COMP WHERE NUM_PED='" & oEPreVenta._idTx & "' AND FK_EMPR=" & item._FK_EMPR & " AND FK_MA_ART=" & item._FK_MAESTRO & " AND FK_ALM=" & item._FK_ALM & " AND FK_UBIC=" & item._FK_UBIC & ""
                command.CommandText = strSqlXDeleteTxStComp
                command.Transaction = transaction
                command.ExecuteNonQuery()

                '-------------INSERT PARA LA TABLA TB_TX_ST_COMP'
                strSqlTxStComp = "INSERT INTO TB_TX_ST_COMP(NUM_PED,FK_MA_ART,FK_EMPR,FK_ALM,FK_UBIC,UNID,EST,FK_DET_PEDIDO,FECHA) VALUES('" & oEPreVenta._num_pre_vta & "','" & item._FK_MAESTRO & "','" & item._FK_EMPR & "','" & item._FK_ALM & "','" & item._FK_UBIC & "','" & item._UND_TOT & "',1,'" & pkDet & "', '" & oEPreVenta._FECHA_TX_ST_COMP & "')"
                command.CommandText = strSqlTxStComp
                command.Transaction = transaction
                command.ExecuteNonQuery()

                '--------------ACTUALIZA EL STOCK COMPROMETIDO
                strUpdateStockComprometido = "UPDATE TB_EMPR_ARTC SET STOCK_COMPROMETIDO = STOCK_COMPROMETIDO + " & cantidadUnidadesSolicitadas & ", FECHA_ACTUALIZACION = getDate() WHERE PK_EMPR_ART = " & item._FK_EMP_ART
                command.CommandText = strUpdateStockComprometido
                command.Transaction = transaction
                command.ExecuteNonQuery()

                '-------------ACTUALIZA EL STOCK DISPONIBLE'
                strUpdateStockDisponible = "UPDATE TB_EMPR_ARTC SET STOCK_DISPONIBLE = STOCK_FISICO - STOCK_COMPROMETIDO, FECHA_ACTUALIZACION = getDate()  WHERE PK_EMPR_ART = " & item._FK_EMP_ART
                command.CommandText = strUpdateStockDisponible
                command.Transaction = transaction
                command.ExecuteNonQuery()

                'Prueba de calculo de stock 16/07/2014'
                cantidadUnidadesSolicitadas = 0
            Next

            transaction.Commit()
            resultado = "OK"
        Catch ex As Exception
            transaction.Rollback()
            resultado = ex.Message
        Finally
            oConexion.Close()
        End Try
        Return resultado
    End Function

    Public Function obtenerPreVentaPorNumPV(ByVal numPreVenta As String) As EPreVenta
        Dim strSqlCabecera, strSqlDetalle As String
        Dim sqlReaderCabecera, sqlReaderDetalle As SqlDataReader

        Dim oConexion As SqlConnection = New SqlConnection(CadenaConexion.CadenaConexionServer)
        Dim oComandos As New SqlCommand

        Try
            oConexion.Open()
            strSqlCabecera = "SELECT PV.pk_cab_pre_vta,PV.num_pre_vta,PV.fk_tipo_camb,PV.fk_moned,pv.fk_cond_pago,PV.fk_linea_cred,PV.fk_tip_doc_vta,PV.obs,PV.est_doc,pv.st_dscto," + _
                             "PV.ruc_dni,TMC.CTA_AGRP,TMC.PK_CLIENTE,TMC.NOMBRE_COMERCIAL,PV.direc_entreg,PV.imp_tot_igv,PV.fech_desp,PV.fk_alm_desp,PV.fk_emp_vend,TE.NOMBRES,TE.APELLIDOS " + _
                             "FROM TB_PR_VTA PV " + _
                             "INNER JOIN TB_MAESTRO_CLIENTES TMC ON PV.fk_cliente= TMC.PK_CLIENTE " + _
                             "INNER JOIN TB_EMPLEADOS TE ON PV.fk_emp_vend=TE.PK_EMPLEADO " + _
                             "WHERE PV.num_pre_vta='" & numPreVenta & "' AND est_doc IN (1,2)"
            oComandos.Connection = oConexion
            oComandos.CommandType = CommandType.Text
            oComandos.CommandText = strSqlCabecera

            sqlReaderCabecera = oComandos.ExecuteReader

            Dim oEPreVenta As New EPreVenta

            If sqlReaderCabecera.Read() Then
                oEPreVenta._pk_cab_pre_vta = sqlReaderCabecera("pk_cab_pre_vta").ToString
                oEPreVenta._num_pre_vta = sqlReaderCabecera("num_pre_vta").ToString
                oEPreVenta._fk_tipo_camb = sqlReaderCabecera("fk_tipo_camb").ToString
                oEPreVenta._fk_moned = sqlReaderCabecera("fk_moned").ToString
                oEPreVenta._fk_cond_pago = sqlReaderCabecera("fk_cond_pago").ToString
                oEPreVenta._fk_linea_cred = sqlReaderCabecera("fk_linea_cred").ToString
                oEPreVenta._fk_tip_doc_vta = sqlReaderCabecera("fk_tip_doc_vta").ToString
                oEPreVenta._obs = sqlReaderCabecera("obs").ToString
                oEPreVenta._st_dscto = sqlReaderCabecera("st_dscto").ToString
                oEPreVenta._est_doc = sqlReaderCabecera("est_doc").ToString
                oEPreVenta._ruc_dni = sqlReaderCabecera("ruc_dni").ToString
                oEPreVenta._cta_agrp = sqlReaderCabecera("CTA_AGRP").ToString
                oEPreVenta._pk_cliente = sqlReaderCabecera("PK_CLIENTE").ToString
                oEPreVenta._nom_cliente = sqlReaderCabecera("NOMBRE_COMERCIAL").ToString
                oEPreVenta._direc_entreg = sqlReaderCabecera("direc_entreg").ToString
                oEPreVenta._imp_tot_igv = sqlReaderCabecera("imp_tot_igv").ToString
                oEPreVenta._fech_desp = sqlReaderCabecera("fech_desp").ToString
                oEPreVenta._fk_alm_desp = sqlReaderCabecera("fk_alm_desp").ToString
                oEPreVenta._fk_emp_vend = sqlReaderCabecera("fk_emp_vend").ToString
                oEPreVenta._nombres = sqlReaderCabecera("NOMBRES").ToString
                oEPreVenta._apellidos = sqlReaderCabecera("APELLIDOS").ToString


                'Datos del detalle'
                strSqlDetalle = "SELECT DPV.PK_DET_PRE_VTA,DPV.ITEM,DPV.FK_PRE_VTA,DPV.FK_MA,MA.COD_ART_IMPORT,MA.NOMBRE_VENTA,MA.TXT_PACKING,MA.CANT_PACKING,MA.CANT_MAYOR,DPV.FK_EMP_ART," + _
                                "DPV.FK_EMPR, TE.NOMBRE_EMPRESA, DPV.FK_ALM, TA.NOMBRE_ALMACEN, DPV.FK_UBIC, UA.NOMBRE_UBICACION, DPV.UND_TOT, DPV.CAJAS, DPV.DOC, DPV.UND_SET,DPV.P_COSTO, DPV.P_VTA,DPV.P_VTA_MOD, DPV.P_TOTAL,DPV.ST_DSCTO,DPV.COMENTARIO " + _
                                "FROM TB_DET_PRE_VTA DPV INNER JOIN TB_EMPRESAS TE ON DPV.FK_EMPR=TE.PK_EMPRESAS " + _
                                "INNER JOIN TB_ALMACENES TA ON DPV.FK_ALM=TA.PK_ALMACEN " + _
                                "INNER JOIN TB_UBICAC_ALM UA ON DPV.FK_UBIC=UA.PK_UBICACIONES " + _
                                "INNER JOIN TB_MAESTRO_ARTICULOS MA ON DPV.FK_MA=MA.PK_MAESTRO_ARTICULOS " + _
                                "WHERE DPV.FK_PRE_VTA='" & sqlReaderCabecera("pk_cab_pre_vta").ToString & "' "

                sqlReaderCabecera.Close()
                oComandos.CommandText = strSqlDetalle

                sqlReaderDetalle = oComandos.ExecuteReader

                Dim objLstEntDetallePreVenta As New List(Of EDetallePreVenta)

                While sqlReaderDetalle.Read
                    objLstEntDetallePreVenta.Add(New EDetallePreVenta() With
                                                 {._PK_DET_PRE_VTA = sqlReaderDetalle("PK_DET_PRE_VTA").ToString,
                                                  ._ITEM = sqlReaderDetalle("ITEM").ToString,
                                                  ._FK_PRE_VTA = sqlReaderDetalle("FK_PRE_VTA").ToString,
                                                  ._FK_MAESTRO = sqlReaderDetalle("FK_MA").ToString,
                                                  ._COD_ART_IMPORT = sqlReaderDetalle("COD_ART_IMPORT").ToString,
                                                  ._NOMBRE_VENTA = sqlReaderDetalle("NOMBRE_VENTA").ToString,
                                                  ._TXT_PACKING = sqlReaderDetalle("TXT_PACKING").ToString,
                                                  ._CANT_PACKING = sqlReaderDetalle("CANT_PACKING").ToString,
                                                  ._CANT_MAYOR = sqlReaderDetalle("CANT_MAYOR").ToString,
                                                  ._FK_EMP_ART = sqlReaderDetalle("FK_EMP_ART").ToString,
                                                  ._FK_EMPR = sqlReaderDetalle("FK_EMPR").ToString,
                                                  ._NOMBRE_EMPRESA = sqlReaderDetalle("NOMBRE_EMPRESA").ToString,
                                                  ._FK_ALM = sqlReaderDetalle("FK_ALM").ToString,
                                                  ._NOMBRE_ALMACEN = sqlReaderDetalle("NOMBRE_ALMACEN").ToString,
                                                  ._FK_UBIC = sqlReaderDetalle("FK_UBIC").ToString,
                                                  ._NOMBRE_UBICACION = sqlReaderDetalle("NOMBRE_UBICACION").ToString,
                                                  ._UND_TOT = sqlReaderDetalle("UND_TOT").ToString,
                                                  ._CAJAS = sqlReaderDetalle("CAJAS").ToString,
                                                  ._DOC = sqlReaderDetalle("DOC").ToString,
                                                  ._UND_SET = sqlReaderDetalle("UND_SET").ToString,
                                                  ._P_COSTO = sqlReaderDetalle("P_COSTO").ToString,
                                                  ._P_VTA = sqlReaderDetalle("P_VTA").ToString,
                                                  ._P_VTA_MOD = sqlReaderDetalle("P_VTA_MOD").ToString,
                                                  ._P_TOTAL = sqlReaderDetalle("P_TOTAL").ToString,
                                                  ._ST_DSCTO = sqlReaderDetalle("ST_DSCTO").ToString,
                                                  ._COMENTARIO = sqlReaderDetalle("COMENTARIO").ToString
                                                     })
                End While
                oEPreVenta._detallePreVenta = objLstEntDetallePreVenta
            End If

            Return oEPreVenta

        Catch ex As Exception
            Throw ex
        Finally
            oConexion.Close()
        End Try
    End Function

    Public Function obtenerDetallePV(ByVal pkCabecera As Integer) As DataTable
        Dim oConexion As SqlConnection
        Dim command As SqlCommand
        oConexion = New SqlConnection(CadenaConexion.CadenaConexionServer)
        Try
            oConexion.Open()
            command = New SqlCommand("SP_OBTENER_DETALLE_ART_WEB_APP", oConexion)
            command.CommandType = CommandType.StoredProcedure
            command.Parameters.Add("@PK_CABECERA", SqlDbType.Int).Value = pkCabecera
            Dim oDataAdapter As New SqlDataAdapter(command)
            Dim dt As New DataTable
            oDataAdapter.Fill(dt)
            Return dt
        Catch ex As Exception
            Throw ex
        Finally
            oConexion.Close()
        End Try
    End Function

    Public Function obtenerDetallePVImprimir(ByVal pkCabecera As Integer) As DataTable
        Dim oConexion As SqlConnection
        Dim command As SqlCommand
        oConexion = New SqlConnection(CadenaConexion.CadenaConexionServer)
        Try
            oConexion.Open()
            command = New SqlCommand("SP_OBTENER_DET_PREVENTA_IMPR", oConexion)
            command.CommandType = CommandType.StoredProcedure
            command.Parameters.Add("@PK_CABECERA", SqlDbType.Int).Value = pkCabecera
            Dim oDataAdapter As New SqlDataAdapter(command)
            Dim dt As New DataTable
            oDataAdapter.Fill(dt)
            Return dt
        Catch ex As Exception
            Throw ex
        Finally
            oConexion.Close()
        End Try
    End Function
#End Region

    Function actualizarPreVenta(oEPreVenta As EPreVenta) As String
        Dim cantidadUnidadesSolicitadas As Integer = 0
        Dim oConexion As SqlConnection = New SqlConnection(CadenaConexion.CadenaConexionServer)
        Dim oComandos = New SqlCommand
        Dim SqlTransac As SqlTransaction
        Dim strSql, strDetallePreVenta, resultado As String

        oConexion.Open()
        SqlTransac = oConexion.BeginTransaction()
        Try

            Dim importeTotalIGV As Decimal = 0.0
            importeTotalIGV = Format(oEPreVenta._imp_tot_igv, "Standard")

            strSql = "UPDATE TB_PR_VTA SET " + _
                                      "fk_cliente='" & oEPreVenta._fk_cliente & "'," + _
                                      "fk_cond_pago='" & oEPreVenta._fk_cond_pago & "'," + _
                                      "fk_emp_vend='" & oEPreVenta._fk_emp_vend & "'," + _
                                      "fk_alm_desp='" & oEPreVenta._fk_alm_desp & "'," + _
                                      "fk_moned='" & oEPreVenta._fk_moned & "'," + _
                                      "fk_tipo_camb='" & oEPreVenta._fk_tipo_camb & "'," + _
                                      "fk_tip_doc_vta='" & oEPreVenta._fk_tip_doc_vta & "'," + _
                                      "tip_doc_cli='" & oEPreVenta._tip_doc_cli & "'," + _
                                      "ruc_dni='" & oEPreVenta._ruc_dni & "'," + _
                                      "direc_entreg='" & oEPreVenta._direc_entreg & "'," + _
                                      "fech_mod='" & oEPreVenta._fech_mod & "'," + _
                                      "usu_mod='" & oEPreVenta._usu_mod & "'," + _
                                      "fech_desp='" & oEPreVenta._fech_desp & "'," + _
                                      "obs='" & oEPreVenta._obs & "'," + _
                                      "est_doc='" & oEPreVenta._est_doc & "'," + _
                                      "imp_tot_igv=" & importeTotalIGV & "," + _
                                      "fk_linea_cred='" & oEPreVenta._fk_linea_cred & "'," + _
                                      "st_dscto='" & oEPreVenta._st_dscto & "' " + _
                                      "WHERE pk_cab_pre_vta='" & oEPreVenta._pk_cab_pre_vta & "'"
            oComandos.Connection = oConexion
            oComandos.CommandType = CommandType.Text
            oComandos.CommandText = strSql
            oComandos.Transaction = SqlTransac
            oComandos.ExecuteScalar()

            For Each item As EDetallePreVenta In oEPreVenta._detallePreVenta
                Dim chkDescuento As Integer = IIf(item._ST_DSCTO = True, 1, 0)
                strDetallePreVenta = "UPDATE TB_DET_PRE_VTA SET ST_DSCTO = " & chkDescuento &
                                    " WHERE PK_DET_PRE_VTA = " & item._PK_DET_PRE_VTA
                oComandos.CommandText = strDetallePreVenta
                oComandos.Transaction = SqlTransac
                oComandos.ExecuteScalar()
            Next
            SqlTransac.Commit()
            resultado = "OK"
            Return resultado
        Catch ex As Exception
            SqlTransac.Rollback()
            Throw ex
        Finally
            oConexion.Close()
        End Try
    End Function

    Function InsertarDetallePreventa(oEHelperPreVenta As EHelperPreVenta) As Integer
        Dim oEPreVenta As New EDetallePreVenta
        oEPreVenta = Convertir(oEHelperPreVenta)
        Dim cantidadUnidadesSolicitadas As Integer = 0
        Dim oConexion As SqlConnection = New SqlConnection(CadenaConexion.CadenaConexionServer)
        Dim oComandos = New SqlCommand
        Dim SqlTransac As SqlTransaction
        Dim strDetallePreVenta, strUpdateStockComprometido, strUpdateStockDisponible, strSqlTxStComp As String
        Dim resultado As Integer

        oConexion.Open()
        SqlTransac = oConexion.BeginTransaction()
        Try
            Dim pCosto As Decimal = 0.0
            Dim pCostoIgvK As Decimal = 0.0
            Dim pVta As Decimal = 0.0
            Dim pVtaMod As Decimal = 0.0
            Dim PTotal As Decimal = 0.0

            pCosto = Format(oEPreVenta._P_COSTO, "Standard")
            pCostoIgvK = Format(oEPreVenta._P_COSTO_IGV_K, "Standard")
            pVta = Format(oEPreVenta._P_VTA, "Standard")
            pVtaMod = Format(oEPreVenta._P_VTA_MOD, "Standard")
            PTotal = Format(oEPreVenta._P_TOTAL, "Standard")

            strDetallePreVenta = "INSERT INTO TB_DET_PRE_VTA(ITEM, FK_PRE_VTA,EST_POS,FK_MA, FK_EMP_ART, FK_EMPR, FK_ALM, FK_UBIC, UND_TOT, " &
                            "CAJAS, DOC, UND_SET, P_COSTO, P_COSTO_IGV_K,P_VTA,P_VTA_MOD,ST_DSCTO,P_TOTAL,COMENTARIO,UND_FALT,NEW_CAJA," &
                            "NEW_DOC,NEW_UND,NEW_UND_TOT,NEW_UND_TOT_2, CANT_IMPR, PK_GRP_VENTA)" &
                            "VALUES('" & oEPreVenta._ITEM & "','" & oEPreVenta._FK_PRE_VTA & "'," & oEPreVenta._EST_POS & ",'" &
                            oEPreVenta._FK_MAESTRO & "','" & oEPreVenta._FK_EMP_ART & "'," + _
                            "'" & oEPreVenta._FK_EMPR & "','" & oEPreVenta._FK_ALM & "','" & oEPreVenta._FK_UBIC & "','" & oEPreVenta._UND_TOT &
                            "','" & oEPreVenta._CAJAS & "','" & oEPreVenta._DOC & "'," + _
                            "'" & oEPreVenta._UND_SET & "'," & pCosto &
                            "," & pCostoIgvK & "," & pVta & "," & pVtaMod & ",'" & oEPreVenta._ST_DSCTO & "'," & PTotal & ",'" & oEPreVenta._COMENTARIO & "',0,0," &
                            "0,0,0," & oEPreVenta._UND_TOT & ",0," & oEPreVenta._PK_GRP_VENTA & "); SELECT @@IDENTITY"
            cantidadUnidadesSolicitadas = cantidadUnidadesSolicitadas + (oEPreVenta._UND_TOT)
            oComandos.Connection = oConexion
            oComandos.CommandType = CommandType.Text
            oComandos.CommandText = strDetallePreVenta
            oComandos.Transaction = SqlTransac
            Dim pkDet As Integer = oComandos.ExecuteScalar

            ''GSILVA'
            ''18/12/2014'
            ''-------------ELIMINAMOS EL STOCK COMPROMETIDO TEMPORAL DE LA TABLA TB_TX_ST_COMP'
            'strSqlXDeleteTxStComp = "DELETE FROM TB_TX_ST_COMP WHERE NUM_PED='" & oEPreVenta._ID_TX & "' AND FK_EMPR=" & oEPreVenta._FK_EMPR & " AND FK_MA_ART=" & oEPreVenta._FK_MAESTRO & " AND FK_ALM=" & oEPreVenta._FK_ALM & " AND FK_UBIC=" & oEPreVenta._FK_UBIC & ""
            'oComandos.CommandText = strSqlXDeleteTxStComp
            'oComandos.Transaction = SqlTransac
            'oComandos.ExecuteNonQuery()

            '-------------INSERT PARA LA TABLA TB_TX_ST_COMP'
            strSqlTxStComp = "INSERT INTO TB_TX_ST_COMP(NUM_PED,FK_MA_ART,FK_EMPR,FK_ALM,FK_UBIC,UNID,EST,FK_DET_PEDIDO) VALUES('" &
                oEPreVenta._NUM_PRE_VTA & "','" & oEPreVenta._FK_MAESTRO & "','" & oEPreVenta._FK_EMPR & "','" & oEPreVenta._FK_ALM & "','" &
                oEPreVenta._FK_UBIC & "','" & oEPreVenta._UND_TOT & "',1,'" & pkDet & "')"
            oComandos.CommandText = strSqlTxStComp
            oComandos.Transaction = SqlTransac
            oComandos.ExecuteNonQuery()

            '--------------ACTUALIZA EL STOCK COMPROMETIDO
            strUpdateStockComprometido = "UPDATE TB_EMPR_ARTC SET STOCK_COMPROMETIDO = STOCK_COMPROMETIDO + " & cantidadUnidadesSolicitadas &
                ", FECHA_ACTUALIZACION = getDate() WHERE PK_EMPR_ART = " & oEPreVenta._FK_EMP_ART
            oComandos.CommandText = strUpdateStockComprometido
            oComandos.Transaction = SqlTransac
            oComandos.ExecuteNonQuery()
            '-------------ACTUALIZA EL STOCK DISPONIBLE'
            strUpdateStockDisponible = "UPDATE TB_EMPR_ARTC SET STOCK_DISPONIBLE = STOCK_FISICO - STOCK_COMPROMETIDO, FECHA_ACTUALIZACION = getDate()  WHERE PK_EMPR_ART = " & oEPreVenta._FK_EMP_ART
            oComandos.CommandText = strUpdateStockDisponible
            oComandos.Transaction = SqlTransac
            oComandos.ExecuteNonQuery()

            'Prueba de calculo de stock 17/07/2014'
            cantidadUnidadesSolicitadas = 0
            SqlTransac.Commit()
            resultado = 1
            Return resultado
        Catch ex As Exception
            SqlTransac.Rollback()
            Throw ex
        Finally
            oConexion.Close()
        End Try
    End Function

    Function EliminarDetallePreVenta(codigoItemDetalle As Integer) As Integer
        Dim cantidadUnidadesSolicitadas As Integer = 0
        Dim oConexion As SqlConnection = New SqlConnection(CadenaConexion.CadenaConexionServer)
        Dim oComandos As SqlCommand
        Dim resultado As String

        oConexion.Open()
        Try
            oComandos = New SqlCommand("SP_TB_DET_PRE_VTA_DEL", oConexion)
            oComandos.Parameters.Add("@PK_DET_PRE_VTA", SqlDbType.Int).Value = codigoItemDetalle
            oComandos.CommandType = CommandType.StoredProcedure
            resultado = oComandos.ExecuteScalar()
        Catch ex As Exception
            Throw ex
        Finally
            oConexion.Close()
        End Try
        Return resultado
    End Function

    Function Convertir(oHelperPreVenta As EHelperPreVenta) As EDetallePreVenta
        Dim oDetalle As New EDetallePreVenta
        Try
            oDetalle._ITEM = oHelperPreVenta._ORDEN_ITEM
            oDetalle._ID_TX = oHelperPreVenta._ID_TX
            oDetalle._FK_PRE_VTA = oHelperPreVenta._FK_PRE_VTA
            oDetalle._EST_POS = "1"
            oDetalle._FK_MAESTRO = oHelperPreVenta._FK_MAESTRO
            oDetalle._FK_EMP_ART = oHelperPreVenta._FK_EMP_ART
            oDetalle._FK_EMPR = oHelperPreVenta._FK_EMPRESA
            oDetalle._FK_ALM = oHelperPreVenta._FK_ALMACEN
            oDetalle._FK_UBIC = oHelperPreVenta._FK_UBICACION
            oDetalle._UND_TOT = oHelperPreVenta._UNIDADES_TOTAL_PEDIDAS
            oDetalle._CAJAS = oHelperPreVenta._CAJAS_PEDIDAS
            oDetalle._UND_SET = oHelperPreVenta._UNIDADES_PEDIDAS
            oDetalle._DOC = oHelperPreVenta._DOCENAS_PEDIDAS
            oDetalle._ST_DSCTO = oHelperPreVenta._ST_DSCTO
            oDetalle._COMENTARIO = oHelperPreVenta._COMENTARIO

            oDetalle._P_COSTO = oHelperPreVenta._PRECIO_COSTO
            oDetalle._P_VTA = oHelperPreVenta._PRECIO
            oDetalle._P_VTA_MOD = oHelperPreVenta._PRECIO
            oDetalle._P_TOTAL = oHelperPreVenta._PRECIO_TOTAL
            oDetalle._NUM_PRE_VTA = oHelperPreVenta._NUM_PRE_VTA
            oDetalle._PK_GRP_VENTA = oHelperPreVenta._PK_GRP_VENTA
        Catch ex As Exception
            Throw ex
        End Try
        Return oDetalle
    End Function

#Region "**************** IMPRIMIR PREVENTA ***************"
    Function ObtenerPreVentaImprimir(_num_preVenta As String) As EImprimirPreVenta
        Dim oImprimir As EImprimirPreVenta = Nothing
        Dim oConexion As SqlConnection = New SqlConnection(CadenaConexion.CadenaConexionServer)
        Dim oComandos As SqlCommand

        oConexion.Open()
        Try
            oComandos = New SqlCommand("SP_OBTENER_PREVENTA_WEB", oConexion)
            oComandos.Parameters.Add("@NUM_PRE_VTA", SqlDbType.NVarChar).Value = _num_preVenta
            oComandos.CommandType = CommandType.StoredProcedure
            Dim sqlReader As SqlDataReader
            sqlReader = oComandos.ExecuteReader

            If (sqlReader.HasRows) Then
                oImprimir = New EImprimirPreVenta
                While (sqlReader.Read())
                    oImprimir.PK_PREVENTA = sqlReader("PK_PREVENTA").ToString()
                    oImprimir.NUM_PREVENTA = sqlReader("NUMERO_PREVENTA").ToString()
                    oImprimir.DIR_ALMACEN = sqlReader("DIRECCION").ToString()
                    oImprimir.FECHA = sqlReader("FECHA").ToString().Substring(0, 10)
                    oImprimir.NOM_CLIENTE = sqlReader("NOMBRE_CLIENTE").ToString()
                    oImprimir.DIR_ENTREGA = sqlReader("DIRECCION_ENTREGA").ToString()
                    oImprimir.RUC_DNI = sqlReader("RUC_DNI").ToString()
                    oImprimir.VENDEDOR = sqlReader("VENDEDOR").ToString()
                    oImprimir.FORMA_PAGO = sqlReader("FORMA_PAGO").ToString()
                End While
            End If
        Catch ex As Exception
            Throw ex
        End Try
        Return oImprimir
    End Function
#End Region
End Class

