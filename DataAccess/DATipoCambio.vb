﻿Imports System.Data.SqlClient

Public Class DATipoCambio

    Public Function obtenerTipoCambioPorEstado() As DataTable

        Dim oConexion As SqlConnection
        Dim oComandos As SqlCommand
        Dim strSql As String

        oConexion = New SqlConnection(CadenaConexion.CadenaConexionServer)
        oComandos = New SqlCommand()

        Try
            oConexion.Open()
            strSql = "SELECT PK_TIPO_CAMBIO AS 'CODIGO',VALOR_USD AS 'VALOR' FROM TB_TIPO_CAMBIO WHERE FLAG_ACTIVO = 1"
            oComandos.Connection = oConexion
            oComandos.CommandType = CommandType.Text
            oComandos.CommandText = strSql

            Dim oDataAdapter As New SqlDataAdapter(oComandos)
            Dim dt As New DataTable
            oDataAdapter.Fill(dt)
            Return dt

        Catch ex As Exception
            Throw ex
        Finally
            oConexion.Close()
        End Try
    End Function

    Public Function obtenerTipoCambioPorPrimaria(ByVal _llave_primaria As Integer) As DataTable

        Dim oConexion As SqlConnection
        Dim oComandos As SqlCommand
        Dim strSql As String

        oConexion = New SqlConnection(CadenaConexion.CadenaConexionServer)
        oComandos = New SqlCommand()

        Try
            oConexion.Open()

            strSql = "SELECT PK_TIPO_CAMBIO AS 'CODIGO',VALOR_USD AS 'VALOR' FROM TB_TIPO_CAMBIO WHERE PK_TIPO_CAMBIO = " & _llave_primaria
            oComandos.Connection = oConexion
            oComandos.CommandType = CommandType.Text
            oComandos.CommandText = strSql

            Dim oDataAdapter As New SqlDataAdapter(oComandos)
            Dim dt As New DataTable
            oDataAdapter.Fill(dt)
            Return dt

        Catch ex As Exception
            Throw ex
        Finally
            oConexion.Close()
        End Try
    End Function



End Class
