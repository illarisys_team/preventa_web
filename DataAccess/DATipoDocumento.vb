﻿Imports System.Data.SqlClient

Public Class DATipoDocumento
    Public Function obtenerTipoDocumento_PFV() As DataTable

        Dim oConexion As SqlConnection
        Dim oComandos As SqlCommand
        Dim strSql As String

        oConexion = New SqlConnection(CadenaConexion.CadenaConexionServer)
        oComandos = New SqlCommand

        Try
            oConexion.Open()
            strSql = "SELECT PK_TIPO_DOCUMENTO as 'CODIGO',DOC,NOMBRE FROM TB_TIPOS_DOCUMENTO WHERE PK_TIPO_DOCUMENTO IN (190000000,190000001)"
            oComandos.Connection = oConexion
            oComandos.CommandType = CommandType.Text
            oComandos.CommandText = strSql

            Dim oDataAdapter As New SqlDataAdapter(oComandos)
            Dim dt As New DataTable
            oDataAdapter.Fill(dt)

            Return dt

        Catch ex As Exception
            Throw ex
        Finally
            oConexion.Close()
        End Try
    End Function

End Class
