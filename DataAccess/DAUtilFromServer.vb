﻿Imports System.Data.SqlClient

Public Class DAUtilFromServer

    Public Function getSysDate() As DateTime
        Dim oConexion As SqlConnection = New SqlConnection(CadenaConexion.CadenaConexionServer)
        Dim oComandos As SqlCommand
        Dim strSql As String
        Dim sysDate As DateTime
        Try
            strSql = "SELECT CONVERT(smalldatetime,GETDATE()) "
            oComandos = New SqlCommand(strSql, oConexion)
            oConexion.Open()
            oComandos.CommandType = CommandType.Text
            sysDate = oComandos.ExecuteScalar()
        Catch ex As Exception
            Throw ex
        Finally
            oConexion.Close()
        End Try
        Return sysDate
    End Function

End Class
