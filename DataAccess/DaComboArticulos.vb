﻿Imports System.Data
Imports System.Data.SqlClient
Imports Entidades

Public Class DaComboArticulos

    Public Function obtenerCombos() As DataTable

        Dim oConexion As SqlConnection
        Dim oComandos As SqlCommand
        Dim strSql As String

        oConexion = New SqlConnection(CadenaConexion.CadenaConexionServer)
        oComandos = New SqlCommand()

        Try
            oConexion.Open()

            strSql = "SELECT  PK_GRP_VENTA  AS 'CODIGO', FK_EMPLEADO, NOMBRE_USUARIO, FK_ESTADO, NOMBRE_ESTADO, FK_EMPRESA , NOMBRE_EMPRESA ,  NOMBRE_AGRUPACION, COD_CBO_VTA AS 'COD_COMBO'  ,FECHA_INICIO_CAMP, FECHA_FIN_CAMP, COMENTARIO FROM TB_GRP_ARTICULOS A"

            oComandos.Connection = oConexion
            oComandos.CommandType = CommandType.Text
            oComandos.CommandText = strSql

            Dim oDataAdapter As New SqlDataAdapter(oComandos)
            Dim dt As New DataTable

            oDataAdapter.Fill(dt)

            Return dt

        Catch ex As Exception
            Throw ex
        Finally
            oConexion.Close()
        End Try

    End Function

    Public Function obtenerCombosPorCodigo(ByVal id As Integer) As DataTable
        Dim oConexion As SqlConnection
        Dim oComandos As SqlCommand

        Dim strSql As String
        oConexion = New SqlConnection(CadenaConexion.CadenaConexionServer)
        oComandos = New SqlCommand()

        Try
            oConexion.Open()
            strSql = "SELECT DGA.FK_CODIGO_VENTA, DGA.FK_EMPR_ARTC,DGA.FK_EMPRESA,EMP.NOMBRE_EMPRESA,MA.NOMBRE_VENTA,MA.TXT_PACKING,DGA.FK_MAESTRO_ARTICULOS,DGA.CANT_VTA,DGA.P_COSTO, DGA.P_VTA_TDA,DGA.P_VTA_CBO,DGA.P_VTA_F FROM TB_DET_GRP_ARTICULOS DGA INNER JOIN TB_GRP_ARTICULOS GA ON DGA.FK_GRP_VTA = GA.PK_GRP_VENTA INNER JOIN TB_MAESTRO_ARTICULOS MA ON DGA.FK_CODIGO_VENTA = MA.COD_ART_IMPORT INNER JOIN TB_EMPRESAS EMP ON DGA.FK_EMPRESA=EMP.PK_EMPRESAS WHERE DGA.FK_GRP_VTA = " & id & ""
            oComandos.Connection = oConexion
            oComandos.CommandType = CommandType.Text
            oComandos.CommandText = strSql

            Dim oDataAdapter As New SqlDataAdapter(oComandos)
            Dim dt As New DataTable

            oDataAdapter.Fill(dt)
            Return dt

        Catch ex As Exception
            Throw ex
        Finally
            oConexion.Close()
        End Try
    End Function

    Public Function verificarExistencia(ByVal codCombo As String) As Boolean
        Dim oConexion As SqlConnection
        Dim oComandos As SqlCommand
        Dim strSql As String = ""
        Dim existe As Integer = 0

        oConexion = New SqlConnection(CadenaConexion.CadenaConexionServer)
        oComandos = New SqlCommand

        Try
            oConexion.Open()
            strSql = "SELECT GRP.COD_CBO_VTA FROM TB_GRP_ARTICULOS GRP WHERE GRP.COD_CBO_VTA='" & codCombo & "'"
            oComandos.Connection = oConexion
            oComandos.CommandType = CommandType.Text
            oComandos.CommandText = strSql

            Dim cod As String = oComandos.ExecuteScalar

            If codCombo = cod Then
                existe = True
            Else
                existe = False
            End If

            Return existe
        Catch ex As Exception
            Throw ex
        Finally
            oConexion.Close()
        End Try
    End Function

    Public Function getFullArticulosxCodigoCombo(ByVal codCombo As String) As DataTable
        Dim dt As New DataTable
        Dim oConexion As SqlConnection
        Dim oComandos As SqlCommand
        Dim strSqlCombo As String = ""

        oConexion = New SqlConnection(CadenaConexion.CadenaConexionServer)
        oComandos = New SqlCommand

        Try
            oConexion.Open()
            strSqlCombo = "SELECT GA.PK_GRP_VENTA, GA.COD_CBO_VTA, GA.NOMBRE_AGRUPACION, DGA.FK_MAESTRO_ARTICULOS, " + _
                          "DGA.FK_EMPR_ARTC, DGA.FK_EMPRESA, EM.NOMBRE_EMPRESA, EM.DIRECCION, " + _
                          "EM.NIF, EM.TELEF1, MA.COD_ART_IMPORT, MA.NOMBRE_VENTA, MA.TXT_PACKING, DGA.P_VTA_CBO, DGA.P_VTA_CBO," + _
                          "DGA.CANT_VTA, DGA.P_COSTO, GA.PK_GRP_VENTA, MA.CANT_PACKING, MA.CANT_MAYOR, UM.NOMBRE_UND " + _
                          "FROM TB_GRP_ARTICULOS GA " + _
                          "INNER JOIN TB_DET_GRP_ARTICULOS DGA ON GA.PK_GRP_VENTA=DGA.FK_GRP_VTA " + _
                          "INNER JOIN TB_MAESTRO_ARTICULOS MA ON DGA.FK_MAESTRO_ARTICULOS=MA.PK_MAESTRO_ARTICULOS " + _
                          "INNER JOIN TB_UND_MEDIDA UM ON MA.UMD_PACKING=UM.PK_UND " + _
                          "INNER JOIN TB_EMPRESAS EM ON DGA.FK_EMPRESA=EM.PK_EMPRESAS " + _
                          "WHERE GA.FK_ESTADO=211 AND GA.COD_CBO_VTA = '" & Trim(codCombo) & "'"
            oComandos.Connection = oConexion
            oComandos.CommandType = CommandType.Text
            oComandos.CommandText = strSqlCombo

            Dim oDataAdapter As New SqlDataAdapter(oComandos)
            oDataAdapter.Fill(dt)
        Catch ex As Exception
            Throw ex
        Finally
            oConexion.Dispose()
            oConexion.Close()
        End Try
        Return dt
    End Function


    Public Function getFullArticulosxNombreCombo(ByVal nombreCombo As String) As DataTable
        Dim dt As New DataTable
        Dim oConexion As SqlConnection
        Dim oComandos As SqlCommand
        Dim strSqlCombo As String = ""

        oConexion = New SqlConnection(CadenaConexion.CadenaConexionServer)
        oComandos = New SqlCommand

        Try
            oConexion.Open()
            strSqlCombo = "SELECT GA.PK_GRP_VENTA, GA.COD_CBO_VTA, GA.NOMBRE_AGRUPACION, DGA.FK_MAESTRO_ARTICULOS," + _
                          "DGA.FK_EMPR_ARTC, DGA.FK_EMPRESA, EM.NOMBRE_EMPRESA, EM.DIRECCION, " + _
                          "EM.NIF, EM.TELEF1, MA.COD_ART_IMPORT, MA.NOMBRE_VENTA, MA.TXT_PACKING, DGA.P_VTA_CBO, DGA.P_VTA_CBO," + _
                          "DGA.CANT_VTA, DGA.P_COSTO, GA.PK_GRP_VENTA, MA.CANT_PACKING, MA.CANT_MAYOR, UM.NOMBRE_UND " + _
                          "FROM TB_GRP_ARTICULOS GA " + _
                          "INNER JOIN TB_DET_GRP_ARTICULOS DGA ON GA.PK_GRP_VENTA=DGA.FK_GRP_VTA " + _
                          "INNER JOIN TB_MAESTRO_ARTICULOS MA ON DGA.FK_MAESTRO_ARTICULOS=MA.PK_MAESTRO_ARTICULOS " + _
                          "INNER JOIN TB_UND_MEDIDA UM ON MA.UMD_PACKING=UM.PK_UND " + _
                          "INNER JOIN TB_EMPRESAS EM ON DGA.FK_EMPRESA=EM.PK_EMPRESAS " + _
                          "WHERE GA.FK_ESTADO=211 AND GA.NOMBRE_AGRUPACION = '" & nombreCombo & "'"
            oComandos.Connection = oConexion
            oComandos.CommandType = CommandType.Text
            oComandos.CommandText = strSqlCombo

            Dim oDataAdapter As New SqlDataAdapter(oComandos)
            oDataAdapter.Fill(dt)
        Catch ex As Exception
            Throw ex
        Finally
            oConexion.Dispose()
            oConexion.Close()
        End Try
        Return dt
    End Function

End Class