﻿Imports System.Data.SqlClient

Public Class DaPropiedadesSistema

    Dim oConexion As SqlConnection
    Dim oComandos As SqlCommand

    Public Function obtenerPropiedadesSistema() As Dictionary(Of String, String)
        Dim strSql As String
        Dim reader As SqlDataReader
        Dim dictionary As New Dictionary(Of String, String)
        oConexion = New SqlConnection(CadenaConexion.CadenaConexionServer)
        oComandos = New SqlCommand
        Try
            oConexion.Open()
            strSql = "SELECT PK_PROPIEDAD as 'Key', VALOR as 'Value' FROM TB_PROPIEDADES WHERE VISIBLE = 1"
            oComandos.Connection = oConexion
            oComandos.CommandType = CommandType.Text
            oComandos.CommandText = strSql

            reader = oComandos.ExecuteReader()

            While reader.Read()
                dictionary.Add(reader("Key").ToString(), reader("Value").ToString())
            End While

            reader.Close()
        Catch ex As Exception
            Throw ex
        End Try

        Return dictionary
    End Function
End Class
