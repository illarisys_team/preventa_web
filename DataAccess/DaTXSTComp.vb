﻿Imports System.Data.SqlClient
Imports Entidades

Public Class DaTXSTComp

    Dim oConexion As SqlConnection
    Dim resultado As String = ""
    Dim oComandos As SqlCommand
    Dim strSql As String = ""

    Public Function insertarStockComprometidoTemporal(ByVal objEntTXSTComp As ETXSTComp) As String
        oConexion = New SqlConnection(CadenaConexion.CadenaConexionServer)
        oComandos = New SqlCommand
        Try
            oConexion.Open()

            strSql = "INSERT INTO TB_TX_ST_COMP (NUM_PED, FK_MA_ART, FK_EMPR, FK_ALM, FK_UBIC, UNID, EST, FK_DET_PEDIDO) " + _
                     "VALUES ('" & objEntTXSTComp._NUM_PED & "'," & objEntTXSTComp._FK_MA_ART & ", " & objEntTXSTComp._FK_EMPR & "," & objEntTXSTComp._FK_ALM & "," + _
                     "" & objEntTXSTComp._FK_UBIC & "," & objEntTXSTComp._UNID & ",1,1)"
            oComandos.Connection = oConexion
            oComandos.CommandType = CommandType.Text
            oComandos.CommandText = strSql

            oComandos.ExecuteNonQuery()

            resultado = "OK"
            Return resultado

        Catch ex As Exception
            Throw ex
        Finally
            oConexion.Close()
        End Try
    End Function

    Public Function eliminarStockComprometidoTemporal(ByVal objEntTXSTComp As ETXSTComp) As String
        Dim resultado As String = ""
        oConexion = New SqlConnection(CadenaConexion.CadenaConexionServer)
        oComandos = New SqlCommand
        Try
            oConexion.Open()
            '------CODIGO QUE ELIMINA LA POSICION DEL LA TABLA TB_TX_ST_COMP'
            strSql = "DELETE FROM TB_TX_ST_COMP WHERE NUM_PED='" & objEntTXSTComp._NUM_PED & "' AND FK_MA_ART=" & objEntTXSTComp._FK_MA_ART & " AND FK_ALM=" & objEntTXSTComp._FK_ALM & " AND FK_UBIC=" & objEntTXSTComp._FK_UBIC & ""
            oComandos.Connection = oConexion
            oComandos.CommandText = strSql
            oComandos.ExecuteNonQuery()

            resultado = "OK"
            Return resultado
        Catch ex As Exception
            Throw ex
        Finally
            oConexion.Close()
        End Try
    End Function

    'Mejora de eliminación por listado'
    'GSILVA 24/12/2014'
    Public Function eliminarListaStockComprometidoTemporal(ByVal objEntTXSTComp As ETXSTComp) As String
        Dim resultado As String = ""
        oConexion = New SqlConnection(CadenaConexion.CadenaConexionServer)
        oComandos = New SqlCommand

        Try
            oConexion.Open()
            For Each item As ETXSTComp In objEntTXSTComp._listadoETXSTComp
                strSql = "DELETE FROM TB_TX_ST_COMP WHERE NUM_PED='" & item._NUM_PED & "' AND FK_MA_ART=" & item._FK_MA_ART & " AND FK_EMPR=" & item._FK_EMPR & " AND FK_ALM=" & item._FK_ALM & " AND FK_UBIC=" & item._FK_UBIC & ""
                oComandos.Connection = oConexion
                oComandos.CommandText = strSql
                oComandos.ExecuteNonQuery()
            Next

            resultado = "OK"
            Return resultado
        Catch ex As Exception
            Throw ex
        Finally
            oConexion.Close()
        End Try
    End Function

End Class
