﻿Imports System.Data.SqlClient
Imports Entidades

Public Class DaUbicacionAlmacen

    Public Function obtenerUbicacionCodigo(ByVal pkUbicacion As Integer) As EUbicacionAlmacen
        Dim oConexion As SqlConnection
        Dim oComandos As SqlCommand
        Dim strSql As String
        Dim reader As SqlDataReader
        Dim objEntUbicacion As New EUbicacionAlmacen

        oConexion = New SqlConnection(CadenaConexion.CadenaConexionServer)
        oComandos = New SqlCommand

        Try
            oConexion.Open()
            strSql = "SELECT NOMBRE_UBICACION, NOMBRE_ALMACEN, DESCRIPCION_ESTADO, NOMBRE_EMPLEADO, FK_EMPLEADO, IMPRESORA, REPOSICION, " + _
                     "COMENTARIOS, PK_UBICACIONES, FK_ALMACEN, FK_ESTADO " + _
                     "FROM TB_UBICAC_ALM " + _
                     "WHERE PK_UBICACIONES=" & pkUbicacion & ""

            oComandos.Connection = oConexion
            oComandos.CommandType = CommandType.Text
            oComandos.CommandText = strSql

            'Dim FilasAfectadas As Integer = oComandos.ExecuteNonQuery()
            reader = oComandos.ExecuteReader()
            If reader.Read() Then
                objEntUbicacion._NOMBRE_UBICACION = reader("NOMBRE_UBICACION")
                objEntUbicacion._NOMBRE_ALMACEN = reader("NOMBRE_ALMACEN")
                objEntUbicacion._DESCRIPCION_ESTADO = reader("DESCRIPCION_ESTADO")
                objEntUbicacion._COMENTARIOS = reader("COMENTARIOS")
                objEntUbicacion._PK_UBICACIONES = reader("PK_UBICACIONES")
                objEntUbicacion._FK_ALMACEN = reader("FK_ALMACEN")
                objEntUbicacion._FK_ESTADO = reader("FK_ESTADO")
                objEntUbicacion._IMPRESORA = reader("IMPRESORA")
                objEntUbicacion._REPOSICION = reader("REPOSICION")
                objEntUbicacion._FK_EMPLEADO = reader("FK_EMPLEADO")
                objEntUbicacion._NOMBRE_EMPLEADO = reader("NOMBRE_EMPLEADO")
            End If
            reader.Close()
        Catch ex As Exception
            ex.Message.ToString()
        Finally
            oConexion.Close()
        End Try
        Return objEntUbicacion
    End Function

End Class
