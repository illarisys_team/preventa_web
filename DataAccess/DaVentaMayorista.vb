﻿Imports System.Data.SqlClient

Public Class DaVentaMayorista

    Dim oConexion As SqlConnection
    Dim oComandos As SqlCommand
    Dim oDataAdapter As SqlDataAdapter
    Dim reader As SqlDataReader
    Dim oTransaction As SqlTransaction

    Public Function obtenerStockDisponibleArticulo(ByVal intCodMaestroArt As Integer, ByVal intCodAlmacen As Integer,
                                                       ByVal intCodUbicacion As Integer, ByVal intCodEmpresa As Integer) As Integer
        Dim oConexion As SqlConnection
        Dim oComandos As SqlCommand
        Dim oDataAdapter As SqlDataAdapter
        Dim reader As SqlDataReader
        Dim stock As Integer
        Dim SvalNull As String = "-"
        oConexion = New SqlConnection(CadenaConexion.CadenaConexionServer)
        oComandos = New SqlCommand

        'validar parametros qué recibe el método no esten vacios, si están vacios..devovler =>0 si están vacíos
        If intCodAlmacen = 0 Or intCodUbicacion = 0 Then Return stock = 0
        '
        Try
            oDataAdapter = New SqlDataAdapter("[SP_OBTENER_STOCK_DISP_ART_CBO]", oConexion)
            oDataAdapter.SelectCommand.CommandType = CommandType.StoredProcedure
            With oDataAdapter.SelectCommand.Parameters
                .Add("@FK_MA_ART", SqlDbType.Int).Value = intCodMaestroArt
                .Add("@FK_ALM", SqlDbType.Int).Value = intCodAlmacen
                .Add("@FK_UBIC", SqlDbType.Int).Value = intCodUbicacion
                .Add("@FK_EMPRESA", SqlDbType.Int).Value = intCodEmpresa
            End With
            oConexion.Open()
            reader = oDataAdapter.SelectCommand.ExecuteReader

            If reader.Read() Then
                SvalNull = reader("STOCK").ToString

                If String.IsNullOrEmpty(SvalNull) Then
                    stock = 0
                Else
                    stock = reader("STOCK").ToString
                End If
            End If

            reader.Close()
        Catch ex As Exception
            stock = 0
            Throw ex
        Finally
            oConexion.Dispose()
            oConexion.Close()
        End Try
        Return stock
    End Function

End Class
