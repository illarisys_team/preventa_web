﻿Friend NotInheritable Class VariableArticulo

    'VARIABLES DEL CONSOLIDADO ARTICULO
    Public Shared codArticulo As String = "COD_ARTICULO" 'CODIGO_ARTICULO_IMP
    Public Shared codMaestro As String = "COD_MAESTRO"
    Public Shared txtPacking As String = "TXT_PACKING"
    Public Shared nombreEmpresa As String = "NOM_EMPRESA"
    Public Shared nombreArticulo As String = "NOM_ARTICULO"
    Public Shared codEmpresa As String = "COD_EMPRESA"
    Public Shared codEmpresaArticulo As String = "COD_EMPRESA_ART"
    Public Shared precioMinimo As String = "PRECIO_MINIMO"
    Public Shared precioMax As String = "PRECIO_MAX"
    Public Shared precioProm As String = "PRECIO_PROM"
    Public Shared precioCostoIgvUtil As String = "PRECIO_COSTO_IGV_UTIL"
    Public Shared precioCosto As String = "PRECIO_COSTO"
    Public Shared precioTienda As String = "PRECIO_TIENDA"
    Public Shared codUMedidaPack As String = "COD_UMD_PACK" 'codigo_umd_packing
    Public Shared nombreUMedida As String = "NOMBRE_UMEDIDA"
    Public Shared stockComprometido As String = "STOCK_COMPROMETIDO"
    Public Shared stockFisico As String = "STOCK_FISICO"
    Public Shared stockDisponible As String = "STOCK_DISPONIBLE"
    Public Shared cantPacking As String = "CAN_PACKING"
    Public Shared cantMayor As String = "CAN_MAYOR"

    'VARIABLES DEL CONSOLIDADO ARTICULO POR ALMACEN PROCEDURE
    Public Shared codAlmacen As String = "PK_ALMACEN"
    Public Shared codUbicacion As String = "PK_UBICACIONES"
    Public Shared nombreAlmacen As String = "NOMBRE_ALMACEN"
    Public Shared nombreUbicacion As String = "NOMBRE_UBICACION"
    Public Shared reposicion As String = "REPOSICION"
    Public Shared stockDispxAlmacen As String = "STOCK_DISPONIBLE"
    Public Shared ipImpresora As String = "IMPRESORA"

    Public Shared sqlGetConsolidado As String = _
        "SELECT ea.PK_EMPR_ART AS " & codEmpresaArticulo &
        ", ea.FK_EMPRESAS AS " & codEmpresa &
        ", e.NOMBRE_EMPRESA AS " & nombreEmpresa &
        ", ea.FK_MAESTRO_ARTICULOS AS " & codMaestro &
        ", ma.COD_ART_IMPORT AS " & codArticulo &
        ", ma.NOMBRE_VENTA AS " & nombreArticulo &
        ", ma.TXT_PACKING AS " & txtPacking &
        ", ma.UMD_PACKING AS " & codUMedidaPack &
        ", u.NOMBRE_UND AS " & nombreUMedida &
        ", ma.CANT_PACKING AS " & cantPacking &
        ", ea.STOCK_COMPROMETIDO AS " & stockComprometido &
        ", ea.STOCK_DISPONIBLE AS " & stockDisponible &
        ", ea.STOCK_FISICO AS " & stockFisico &
        ", ma.PRECIO_MINIMO AS " & precioMinimo &
        ", ma.PRECIO_PROM AS " & precioProm &
        ", PRECIO_MAX AS " & precioMax &
        ", ma.P_COSTO_IGV_UTIL " & precioCostoIgvUtil &
        ", ma.PRECIO_COSTO AS " & precioCosto &
        ", ma.PRECIO_TIENDA AS " & precioTienda &
        ", ma.CANT_MAYOR " & cantMayor &
        " FROM TB_EMPR_ARTC as ea INNER JOIN" &
        " TB_EMPRESAS as e on ea.FK_EMPRESAS = e.PK_EMPRESAS" &
        " INNER JOIN TB_MAESTRO_ARTICULOS as ma ON" &
        " ea.FK_MAESTRO_ARTICULOS = ma.PK_MAESTRO_ARTICULOS INNER JOIN" &
        " TB_UND_MEDIDA as u on u.PK_UND = ma.UMD_PACKING" &
        " WHERE ea.ESTADO = 1 " 'AND ea.FK_MAESTRO_ARTICULOS = 
End Class
