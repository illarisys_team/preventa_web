﻿Public Class EArticulo

    Public Property _pk_articulo As Integer
    Public Property _precio_maximo As Decimal
    Public Property _precio_minimo As Decimal
    Public Property _precio_promedio As Decimal
    Public Property _precio_tienda As Decimal
    Public Property _precio_costo As Decimal

    'NUEVOS CAMPOS POR DENOMINACION NOMINAL
    Public Property _fk_moneda_precio As Decimal
    Public Property _tipo_cambio As Decimal
    Public Property _precio_costo_cotizacion As Decimal

    'campos para el descuento
    Public Property _desc_categoria As Decimal
    Public Property _desc_cond_pago As Decimal
    Public Property _desc_volumen As Decimal
    Public Property _desc_cantidad As Integer


End Class
