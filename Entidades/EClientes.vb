﻿Public Class EClientes

    Public Property _pk_cliente As Integer
    Public Property _fk_corp As Integer
    Public Property _nombre_corp As String
    Public Property _nombre_comercial As String
    Public Property _nif As String
    Public Property _dni As String
    Public Property _razon_social As String
    Public Property _tratamiento As String
    Public Property _nombre_contacto As String
    Public Property _tipo_cliente As String
    Public Property _dpto As String
    Public Property _fk_departamento As Integer
    Public Property _distrito As String
    Public Property _fk_distrito As Integer
    Public Property _fk_prov As Integer
    Public Property _txt_prov As String
    Public Property _email As String
    Public Property _telef1 As String
    Public Property _telef2 As String
    Public Property _fax As String
    Public Property _website As String
    Public Property _linea_credito As Decimal
    Public Property _fk_linea As Integer
    Public Property _nombre_linea As String
    Public Property _fk_estado As Integer
    Public Property _descripcion_estado As String
    Public Property _direcciones As New List(Of Entidades.EDirecEmpresas)
    Public Property _flag_interno As String

    'Se crea entidades para ayuda de otros procesos(HELPER)'
    'Para la agrupacion'
    Public Property _CTA_AGRP As String
    Public Property _txt_direcc As String
    Public Property _tipo_direcc As String

End Class
