﻿Public Class EDetallePreVenta

    Public Property _PK_DET_PRE_VTA As Integer
    Public Property _ITEM As Integer
    Public Property _FK_PRE_VTA As Integer
    Public Property _EST_POS As Integer
    Public Property _FK_MAESTRO As Integer
    Public Property _FK_EMP_ART As Integer
    Public Property _FK_EMPR As Integer
    Public Property _FK_ALM As Integer
    Public Property _FK_UBIC As Integer
    Public Property _UND_TOT As Integer
    Public Property _CAJAS As Integer
    Public Property _DOC As Integer
    Public Property _UND_SET As Integer
    Public Property _P_COSTO As Decimal
    Public Property _P_COSTO_IGV_K As Decimal
    Public Property _P_VTA As Decimal
    Public Property _P_VTA_MOD As Decimal
    Public Property _ST_DSCTO As Boolean
    Public Property _P_TOTAL As Decimal
    Public Property _COMENTARIO As String
    Public Property _NUEVO_REGISTRO As String

    'Helpers para traer los datos enlazados a las llaves foraneas'
    Public Property _COD_ART_IMPORT As String
    Public Property _NOMBRE_VENTA As String
    Public Property _TXT_PACKING As String
    Public Property _CANT_PACKING As Integer
    Public Property _CANT_MAYOR As Integer
    Public Property _NOMBRE_EMPRESA As String
    Public Property _NOMBRE_ALMACEN As String
    Public Property _NOMBRE_UBICACION As String

    Public Property _ruta_impresora As String

    'PRE VENTA WEB
    Public Property _NOMBRE_UNIDAD As String
    Public Property _CODIGO_UNIDAD As Integer
    Public Property _PRECIO_MIN As Decimal
    Public Property _PRECIO_PROM As Decimal
    Public Property _PRECIO_MAX As Decimal
    Public Property _PRECIO_COSTO As Decimal
    Public Property _PRECIO_TIENDA As Decimal
    Public Property _IP_IMPRESORA As String
    Public Property _CAJAS_PEDIDAS As Integer
    Public Property _DOCENAS_PEDIDAS As Integer
    Public Property _UNIDADES_PEDIDAS As Integer
    Public Property _UNIDADES_TOTAL_PEDIDAS As Integer
    Public Property _FK_EMPLEADO As String
    Public Property _ID_TX As String

    Public Property _NUM_PRE_VTA As String
    Public Property _PK_GRP_VENTA As Int32

End Class
