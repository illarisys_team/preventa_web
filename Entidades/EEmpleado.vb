﻿Public Class EEmpleado
    Public Property _PK_EMPLEADO As Integer
    Public Property _DESCRIPCION As String
    Public Property _NOMBRES As String
    Public Property _APELLIDOS As String
    Public Property _DNI As String
    Public Property _FECHA_NACIMIENTO As Date
    Public Property _USUARIO As String
    Public Property _FECHA_INGRESO As Date
    Public Property _FECHA_CREACION As Date
    Public Property _TELEFONO1 As String
    Public Property _TELEFONO2 As String
    Public Property _CLAVE As String
    Public Property _FK_ESTADO As Integer
    Public Property _FK_ROL_COMERCIAL As Integer
    Public Property _FK_AREA_EMPRESA As Integer
    Public Property _FK_EMPRESA As Integer
    Public Property _CAMB_PASS As Integer
    Public Property _FECHA_CADUCIDAD As String

    Public Property Imagen() As Byte()
        Get
            Return m_Imagen
        End Get
        Set(ByVal value As Byte())
            m_Imagen = Value
        End Set
    End Property
    Private m_Imagen As Byte()

End Class
