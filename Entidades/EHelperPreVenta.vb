﻿Public Class EHelperPreVenta

    Public Property _FK_EMPLEADO As String
    Public Property _NUM_PRE_VTA As String
    Public Property _ORDEN_ITEM As Integer
    Public Property _FK_PRE_VTA As Integer
    Public Property _FK_MAESTRO As Integer
    Public Property _FK_EMP_ART As Integer
    Public Property _FK_EMPRESA As Integer
    Public Property _DESC_EMPRESA As String
    Public Property _DESC_ALMACEN As String
    Public Property _DESC_UBICACION As String
    Public Property _FK_ALMACEN As Integer
    Public Property _FK_UBICACION As Integer
    Public Property _PRECIO As Decimal
    Public Property _ST_DSCTO As Boolean
    Public Property _PRECIO_TOTAL As Decimal
    Public Property _COMENTARIO As String
    Public Property _NUEVO_REGISTRO As String
    Public Property _COD_ART_IMPORT As String
    Public Property _NOMBRE_VENTA As String
    Public Property _NOMBRE_UNIDAD As String 'CHECK
    Public Property _CODIGO_UNIDAD As Integer
    Public Property _PRECIO_COSTO_IGV_K As Decimal
    Public Property _PRECIO_COSTO As Decimal
    Public Property _IP_IMPRESORA As String
    Public Property _DESC_PACKING As String
    Public Property _CAN_MAYOR As Integer
    Public Property _CAN_PACKING As Integer
    Public Property _CAJAS_PEDIDAS As Integer
    Public Property _DOCENAS_PEDIDAS As Integer
    Public Property _UNIDADES_PEDIDAS As Integer
    Public Property _UNIDADES_TOTAL_PEDIDAS As Integer
    Public Property _ID_TX As String

    Public Property _DLL_CLASE As String
    Public Property _PK_GRP_VENTA As Int32

End Class

