﻿Public Class EPreVenta
    Public Property _pk_cab_pre_vta As Integer
    Public Property _num_pre_vta As String
    Public Property _fk_cliente As Integer
    Public Property _fk_cond_pago As Integer
    Public Property _fk_emp_vend As Integer
    Public Property _fk_alm_desp As Integer
    Public Property _fk_moned As Integer
    Public Property _fk_tipo_camb As Integer
    Public Property _fk_tip_doc_vta As Integer
    Public Property _tip_doc_cli As String
    Public Property _ruc_dni As String
    Public Property _direc_entreg As String
    Public Property _fech_crea As Date
    Public Property _fech_mod As Date
    Public Property _fech_desp As Date
    Public Property _usu_crea As String
    Public Property _usu_mod As String
    Public Property _obs As String
    Public Property _est_doc As Int16
    Public Property _mot_rech_doc As String
    Public Property _imp_tot_igv As Decimal
    Public Property _fk_linea_cred As Integer
    Public Property _st_dscto As String
    Public Property _int_print As Int16
    Public Property _detallePreVenta As List(Of EDetallePreVenta)
    Public Property _detallePreVentaEliminado As List(Of EDetallePreVenta)

    Public Property _FECHA_TX_ST_COMP As Date

    'Helpers para traer datos que se enlazan entre las llaves'
    Public Property _nom_cliente As String
    Public Property _cta_agrp As Integer
    Public Property _pk_cliente As Integer
    Public Property _idTx As String

    'Para el vendedor'
    Public Property _nombres As String
    Public Property _apellidos As String

End Class
