﻿Public Class ETipoCambio
    Public Property _pk_tipo_cambio As Integer
    Public Property _fk_empleado As Integer
    Public Property _texto_empleado As String
    Public Property _valor_pen As Decimal
    Public Property _valor_usd As Decimal
    Public Property _valor_cambio As Decimal
    Public Property _fecha_modificacion As Date
    Public Property _flag_activo As Integer
    Public Property _fk_corporacion As Integer
End Class
