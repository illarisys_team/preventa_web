﻿Public Class EUbicacionAlmacen

    Public Property _PK_UBICACIONES As Integer ''
    Public Property _FK_ALMACEN As Integer
    Public Property _FK_ESTADO As Integer
    Public Property _REPOSICION As Integer '(--- 1 SI ACEPTA | 0 NO ACEPTA)
    Public Property _FK_EMPLEADO As Integer

    Public Property _IMPRESORA As String
    Public Property _NOMBRE_ALMACEN As String
    Public Property _DESCRIPCION_ESTADO As String
    Public Property _NOMBRE_UBICACION As String
    Public Property _COMENTARIOS As String
    Public Property _NOMBRE_EMPLEADO As String

End Class