﻿Public Class HCConsolidado

    Private _idMaestroArticulo As String
    Public Property IDMaestroArticulo() As String
        Get
            Return _idMaestroArticulo
        End Get
        Set(ByVal value As String)
            _idMaestroArticulo = value
        End Set
    End Property

    Private _idEmpresa As String
    Public Property IDEmpresa() As String
        Get
            Return _idEmpresa
        End Get
        Set(ByVal value As String)
            _idEmpresa = value
        End Set
    End Property

    Private _codigoUMPack As String
    Public Property CodigoUMedidaPack() As String
        Get
            Return _codigoUMPack
        End Get
        Set(ByVal value As String)
            _codigoUMPack = value
        End Set
    End Property

    Private _nombreUMedida As String
    Public Property NombreUMedida() As String
        Get
            Return _nombreUMedida
        End Get
        Set(ByVal value As String)
            _nombreUMedida = value
        End Set
    End Property

    Private _cantUPacking As String
    Public Property CantUPacking() As String
        Get
            Return _cantUPacking
        End Get
        Set(ByVal value As String)
            _cantUPacking = value
        End Set
    End Property

    Private _nombreEmpresa As String
    Public Property NombreEmpresa() As String
        Get
            Return _nombreEmpresa
        End Get
        Set(ByVal value As String)
            _nombreEmpresa = value
        End Set
    End Property

    Private _idEmpresaArticulo As String
    Public Property IDEmpresaArticulo() As String
        Get
            Return _idEmpresaArticulo
        End Get
        Set(ByVal value As String)
            _idEmpresaArticulo = value
        End Set
    End Property

    Private _codigoArticulo As String
    Public Property CodigoArticulo() As String
        Get
            Return _codigoArticulo
        End Get
        Set(ByVal value As String)
            _codigoArticulo = value
        End Set
    End Property

    Private _nombreArticulo As String
    Public Property NombreArticulo() As String
        Get
            Return _nombreArticulo
        End Get
        Set(ByVal value As String)
            _nombreArticulo = value
        End Set
    End Property

    Private _precioMin As String
    Public Property PrecioMin() As String
        Get
            Return _precioMin
        End Get
        Set(ByVal value As String)
            _precioMin = value
        End Set
    End Property

    Private _precioMax As String
    Public Property PrecioMax() As String
        Get
            Return _precioMax
        End Get
        Set(ByVal value As String)
            _precioMax = value
        End Set
    End Property

    Private _precioProm As String
    Public Property PrecioProm() As String
        Get
            Return _precioProm
        End Get
        Set(ByVal value As String)
            _precioProm = value
        End Set
    End Property

    Private _precioTienda As String
    Public Property PrecioTienda() As String
        Get
            Return _precioTienda
        End Get
        Set(ByVal value As String)
            _precioTienda = value
        End Set
    End Property

    Private _precioCosto As String
    Public Property PrecioCosto() As String
        Get
            Return _precioCosto
        End Get
        Set(ByVal value As String)
            _precioCosto = value
        End Set
    End Property

    Private _precioCostoIgvUtil As String
    Public Property PrecioCostoIgvUtil() As String
        Get
            Return _precioCostoIgvUtil
        End Get
        Set(ByVal value As String)
            _precioCostoIgvUtil = value
        End Set
    End Property

    Private _cantMayor As String
    Public Property CantMayor() As String
        Get
            Return _cantMayor
        End Get
        Set(ByVal value As String)
            _cantMayor = value
        End Set
    End Property

    Private _txtPacking As String
    Public Property TxtPacking() As String
        Get
            Return _txtPacking
        End Get
        Set(ByVal value As String)
            _txtPacking = value
        End Set
    End Property
End Class
