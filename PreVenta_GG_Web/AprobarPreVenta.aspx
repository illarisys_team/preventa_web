﻿<%@ Page Title="Aprobar PreVenta" Language="vb" AutoEventWireup="false" MasterPageFile="~/PreVenta.Master" 
    CodeBehind="AprobarPreVenta.aspx.vb" Inherits="PreVenta_GG_Web.AprobarPreVenta" ClientIDMode="Static" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:Panel ID="pnlCreaPreVentas" runat="server" DefaultButton="btnDefault">
        <link href="../Resource/bootstrap/datetimepicker/bootstrap-datetimepicker.min.css" rel="stylesheet" />
        <script src="../Resource/bootstrap/datetimepicker/moment.min.js"></script>
        <link href="../Resource/bootstrap/datetimepicker/bootstrap-datetimepicker.min.css" rel="stylesheet" />
        <script src="../Resource/bootstrap/datetimepicker/locales/bootstrap-datetimepicker.es.js"></script>
        <script src="../Resource/jquery-ui-1.11.1/jquery-ui.min.js"></script>
        <script>
            $(function () {
                $('#datetimepicker8').datetimepicker({
                    icons: {
                        time: "fa fa-clock-o",
                        date: "fa fa-calendar",
                        up: "fa fa-arrow-up",
                        down: "fa fa-arrow-down"
                    },
                    language: 'es',
                    pickTime: false,
                    minDate: '01/06/2014',
                    maxDate: new Date(),
                    useCurrent: false
                });
                $('#datetimepicker9').datetimepicker({
                    icons: {
                        time: "fa fa-clock-o",
                        date: "fa fa-calendar",
                        up: "fa fa-arrow-up",
                        down: "fa fa-arrow-down"
                    },
                    language: 'es',
                    pickTime: false,
                    maxDate: new Date(),
                    useCurrent: false
                });
                $("#datetimepicker8").on("dp.change", function (e) {
                    $('#datetimepicker9').data("DateTimePicker").setMinDate(e.date.add('days', -1));
                });
                $("#datetimepicker9").on("dp.change", function (e) {
                    $('#datetimepicker8').data("DateTimePicker").setMaxDate(e.date);
                });
            });
        </script>
        <asp:Button ID="btnDefault" runat="server" UseSubmitBehavior="false" Style="display: none;" />
        <div class="panel panel-primary">
            <div class="panel-heading">
                <h3 class="panel-title text-center">APROBACI&Oacute;N DE PRE-VENTA</h3>
            </div>
            <div class="panel-body">
                <div class="form-group">
                    <div class="row">
                        <div class="col-xs-12 col-sm-2 col-md-1 col-md-offset-2">
                            <p class="control-label text-left">C&oacute;digo:</p>
                        </div>
                        <div class="col-xs-12 col-sm-4 col-md-2">
                            <div class="input-group">
                                <asp:TextBox ID="txtCodigoPre" runat="server" CssClass="form-control input-sm">
                                </asp:TextBox>
                                <span class="input-group-btn">
                                    <asp:ImageButton ID="btnBuscarPrexCodigo" runat="server" CssClass="btn btn-default btn-xs" ImageUrl="~/Resource/images/ico-search.png"
                                        AlternateText="Buscar" Style="border-radius: 30px 30px" ImageAlign="AbsMiddle" OnClick="btnBuscarPrexCodigo_Click"></asp:ImageButton>
                                </span>
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-2 col-md-1 col-md-offset-2">
                            <p class="control-label text-left">Ruc / Dni:</p>
                        </div>
                        <div class="col-xs-12 col-sm-4 col-md-2">
                            <div class="input-group">
                                <asp:TextBox ID="txtNombre" runat="server" CssClass="form-control input-sm">
                                </asp:TextBox>
                                <span class="input-group-btn">
                                    <asp:ImageButton ID="btnBuscarPrexNombre" runat="server" CssClass="btn btn-default btn-xs" ImageUrl="~/Resource/images/ico-search.png"
                                        AlternateText="Buscar" Style="border-radius: 30px 30px" ImageAlign="AbsMiddle" OnClick="btnBuscarPrexNombre_Click"></asp:ImageButton>
                                </span>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <div class="row">
                        <div class="col-xs-12 col-sm-2 col-md-1 col-md-offset-2">
                            <p class="control-label text-left">Desde:</p>
                        </div>
                        <div class="col-xs-12 col-sm-4 col-md-2">
                            <div class='input-group date' id='datetimepicker8'>
                                <asp:TextBox ID="txtFechaInicial" runat="server" CssClass="form-control" placeholder="dd/mm/yyyy"></asp:TextBox>
                                <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span>
                                </span>
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-2 col-md-1 col-md-offset-2">
                            <p class="control-label text-left">Hasta:</p>
                        </div>
                        <div class="col-xs-12 col-sm-4 col-md-2">
                            <div class='input-group date' id='datetimepicker9'>
                                <asp:TextBox ID="txtFechaFinal" runat="server" CssClass="form-control" placeholder="dd/mm/yyyy"></asp:TextBox>
                                <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span>
                                </span>
                            </div>
                        </div>
                    </div>
                </div>

                <br />
                <div id="divPreVentas">
                    <h3 class="text-center">Pre-Ventas</h3>
                    <div class="form-group">
                        <div class="row">
                            <div class="table-responsive">
                                <asp:GridView ID="grvPreVentas" runat="server" AutoGenerateColumns="false"
                                    CssClass="table table-bordered table-condensed">
                                </asp:GridView>
                            </div>
                        </div>
                    </div>
                </div>

                <div id="divDetallePreVenta">
                    <h3 class="text-center">Detalle de la Pre-Venta</h3>
                    <div class="form-group">
                        <div class="row">
                            <div class="table-responsive">
                                <asp:GridView ID="grvDetallePreVenta" runat="server" AutoGenerateColumns="false"
                                    CssClass="table table-bordered table-condensed">
                                </asp:GridView>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="form-group">
            <div class="text-center">
                <asp:Button ID="btnVerDatosCliente" Text="Ver Datos del Cliente" runat="server" CssClass="btn btn-primary"
                    OnClick="btnVerDatosCliente_Click"></asp:Button>&nbsp;
                <asp:Button ID="btnAcciones" Text="Acciones a Tomar" runat="server" CssClass="btn btn-success"></asp:Button>
            </div>
        </div>
    </asp:Panel>

    <!-- ************* INICIO DEL LOS MODAL POPUPS  ***************** -->

    <!--INI POP UP VER DATOS DEL CLIENTE-->
    <asp:Button ID="btnHandlerPop" runat="server" Style="display: none" />
    <asp:ModalPopupExtender ID="MPopVerCliente" runat="server" Enabled="true" PopupControlID="pnlVerCliente"
        PopupDragHandleControlID="pnlVerCliente" TargetControlID="btnHandlerPop" CancelControlID="btnCerrarVer">
    </asp:ModalPopupExtender>
    <asp:Panel ID="pnlVerCliente" runat="server" DefaultButton="btnCerrarVer">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="text-center modal-title">Datos del Cliente</h4>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <div class="row">
                            <div class="col-xs-12 col-sm-3 col-md-3">
                                <p class="control-label text-right">C&oacute;digo:</p>
                            </div>
                            <div class="col-xs-12 col-sm-8 col-md-8">
                                <asp:Label ID="lblCCodigo" runat="server" CssClass="form-control input-sm">
                                </asp:Label>
                            </div>
                            <div class="col-xs-12 col-sm-3 col-md-3">
                                <p class="control-label text-right">Documento:</p>
                            </div>
                            <div class="col-xs-12 col-sm-8 col-md-8">
                                <asp:Label ID="lblCDocumento" runat="server" CssClass="form-control input-sm">
                                </asp:Label>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <asp:Button ID="btnCerrarVer" runat="server" Text="Cerrar" CssClass="btn btn-danger"
                        OnClick="btnCerrarVer_Click"></asp:Button>
                </div>
            </div>
        </div>
    </asp:Panel>
    <!--END POP UP VER DATOS DEL CLIENTE-->
</asp:Content>
