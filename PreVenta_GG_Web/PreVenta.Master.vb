﻿Imports WebUtils

Public Class PreVenta
    Inherits System.Web.UI.MasterPage

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Not Page.IsPostBack Then
                Dim cookie As HttpCookie = HttpContext.Current.Request.Cookies("EmployerGG")
                If (cookie Is Nothing) Then
                    HttpContext.Current.Response.Redirect("~/index.aspx")
                Else
                    hfEmpleadoID.Value = cookie("EmployerID").ToString() 'EmployerName
                    lblEmpleadoName.Text = cookie("EmployerName").ToString()
                    hfEmpleadoUser.Value = cookie("EmployerUser").ToString()
                    hfNombrePc.Value = My.Computer.Name
                    hfIdTXSTComp.Value = hfEmpleadoUser.Value & hfNombrePc.Value
                    hdNomEmpleado.Value = lblEmpleadoName.Text
                End If
            End If
        Catch ex As Exception
            MyMessageBox.Alert(ex.Message)
        End Try
    End Sub

    Protected Sub btnCerrarSession_Click(sender As Object, e As EventArgs)
        HttpContext.Current.Session.Abandon()
        FormsAuthentication.SignOut()
        HttpContext.Current.Response.Cookies.Remove(FormsAuthentication.FormsCookieName)
        HttpContext.Current.Session.Clear()
        HttpContext.Current.Response.Cache.SetCacheability(HttpCacheability.NoCache)
        HttpContext.Current.Response.Cache.SetExpires(DateTime.Now.AddSeconds(-1))
        HttpContext.Current.Response.Cache.SetNoStore()
        HttpContext.Current.Response.AppendHeader("Pragma", "no-cache")
        Response.ExpiresAbsolute = DateTime.Now.AddDays(-1)
        Response.Expires = -1
        Response.CacheControl = "no-cache"
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")
        Response.AppendHeader("Cache-Control", "no-cache")
        HttpContext.Current.Response.Cookies.Remove("EmployerGG")
        Response.Buffer = True

        Dim c1 As HttpCookie = Request.Cookies("EmployerGG")
        Dim c2 As HttpCookie = Request.Cookies("EmployerGG")
        Request.Cookies.Remove("EmployerGG")
        Request.Cookies.Remove("EmployerGG")
        c1.Expires = DateTime.Now.AddDays(-1)
        c2.Expires = DateTime.Now.AddDays(-1)
        c1.Value = Nothing
        c2.Value = Nothing
        Response.SetCookie(c1)
        Response.SetCookie(c2)
        Response.RedirectPermanent("~/index.aspx", True)
    End Sub
End Class