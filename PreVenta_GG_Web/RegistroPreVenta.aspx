﻿<%@ Page Title="Nueva PreVenta" Language="vb" AutoEventWireup="false" MasterPageFile="~/PreVenta.Master" CodeBehind="RegistroPreVenta.aspx.vb" 
    Inherits="PreVenta_GG_Web.RegistroPreVenta" ClientIDMode="Static"
    EnableViewStateMac="false" ViewStateEncryptionMode="Never" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:UpdatePanel ID="upnlGeneralPanel" runat="server" ChildrenAsTriggers="true">

        <ContentTemplate>

        <div class="row center-block text-center">
            <h1 class="text-info" style="color:whitesmoke">REGISTRO DE PRE-VENTA</h1>
        </div>
        <asp:Panel ID="pnlCreaPreVentas" runat="server" DefaultButton="btnDefault">
            <asp:Button ID="btnDefault" runat="server" UseSubmitBehavior="false" Style="display: none;" />

            <ul class="nav nav-tabs">
              <li id="liPreVenta" runat="server" class="active"><a href="#collapsePreVenta" data-toggle="tab" aria-expanded="true" class="panel-title h3">Pre-Venta</a></li>
              <li id="liCliente" runat="server" class=""><a href="#collapseDatosCli" data-toggle="tab" aria-expanded="false" class="panel-title h3">Cliente</a></li>
              <li id="liArticulo" runat="server" class=""><a href="#collapseBusquedaArt" data-toggle="tab" aria-expanded="false" class="panel-title h3">Artículo</a></li>
              <li id="liDetalle" runat="server" class=""><a href="#collapseDetallePre" data-toggle="tab" aria-expanded="false" class="panel-title h3">Detalle</a></li>
            </ul>

            <div class="tab-content" id="accordion">
                <!--PRE VENTA -->
                <div id="collapsePreVenta" runat="server" class="tab-pane fade active in">
                    <div class="panel panel-default">
                        <div class="panel-body">
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-xs-12 col-sm-3 col-md-2 col-lg-2 col-sm-offset-1 col-md-offset-1 col-lg-offset-2">
                                        <label class="control-label text-left">C&oacute;digo:</label>
                                    </div>
                                    <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4">
                                        <asp:HiddenField ID="hfCorrelativoPre" runat="server" Value="null" />
                                        <asp:HiddenField ID="hfCodigoPreVenta" runat="server" Value="null" />
                                        <asp:HiddenField ID="hfEstadoPreVenta" runat="server" Value="0"/>
                                        <asp:TextBox ID="txtCodigoPre" runat="server" Enabled="false" CssClass="form-control"/>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="row">
                                    <div class="col-xs-12 col-sm-3 col-md-2 col-lg-2 col-sm-offset-1 col-md-offset-1 col-lg-offset-2">
                                        <label class="control-label text-left">Clase:</label>
                                    </div>
                                    <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4">
                                        <asp:DropDownList ID="ddlClase" runat="server" Enabled="false" CssClass="form-control">
                                            <asp:ListItem Text="[SELECCIONE]" Value="null"></asp:ListItem>
                                            <asp:ListItem Text="I" Value="I"></asp:ListItem>
                                            <asp:ListItem Text="M" Value="M"></asp:ListItem>
                                        </asp:DropDownList>
                                    </div>
                                </div>
                            </div>

                            <asp:UpdatePanel ID="upnlFormasPago" runat="server" UpdateMode="Conditional">
                                <ContentTemplate>
                                    <div class="form-group">
                                        <div class="row">
                                            <div class="col-xs-12 col-sm-3 col-md-2 col-lg-2 col-sm-offset-1 col-md-offset-1 col-lg-offset-2">
                                                <label class="control-label text-left">Formas de Pago:</label>
                                            </div>
                                            <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4">
                                                    &nbsp;&nbsp;
                                                <asp:RadioButton ID="chkContado" GroupName="rgbFormaPago" AutoPostBack="true"
                                                    OnCheckedChanged="chkContado_CheckedChanged" Text="&nbsp;&nbsp;&nbsp;Contado" Checked="true" runat="server" />&nbsp;&nbsp;&nbsp;
                                                <asp:RadioButton ID="chkCredito" GroupName="rgbFormaPago" AutoPostBack="true"
                                                    OnCheckedChanged="chkCredito_CheckedChanged" Text="&nbsp;&nbsp;&nbsp;Crédito" runat="server" />
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <div class="row">
                                            <div class="col-xs-12 col-sm-3 col-md-2 col-lg-2 col-sm-offset-1 col-md-offset-1 col-lg-offset-2">
                                                <label class="control-label text-left">Cr&eacute;dito:</label>
                                            </div>
                                            <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4">
                                                <asp:DropDownList ID="ddlCredito" Enabled="false" runat="server" CssClass="form-control">
                                                </asp:DropDownList>
                                            </div>
                                        </div>
                                    </div>
                                </ContentTemplate>
                                <Triggers>
                                    <asp:AsyncPostBackTrigger ControlID="chkContado" EventName="CheckedChanged" />
                                    <asp:AsyncPostBackTrigger ControlID="chkCredito" EventName="CheckedChanged" />
                                </Triggers>
                            </asp:UpdatePanel>

                            <div class="form-group">
                                <div class="row">
                                    <div class="col-xs-12 col-sm-3 col-md-2 col-lg-2 col-sm-offset-1 col-md-offset-1 col-lg-offset-2">
                                        <label class="control-label text-left">Tipo Documento:</label>
                                    </div>
                                    <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4">
                                        <asp:DropDownList ID="ddlTipoDocumento" runat="server" CssClass="form-control">
                                        </asp:DropDownList>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="row">
                                    <div class="col-xs-12 col-sm-3 col-md-2 col-lg-2 col-sm-offset-1 col-md-offset-1 col-lg-offset-2">
                                        <label class="control-label text-left">Moneda:</label>
                                    </div>
                                    <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4">
                                        <asp:DropDownList ID="ddlMoneda" runat="server" CssClass="form-control">
                                        </asp:DropDownList>
                                    </div>                                                                
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="row">
                                    <div class="col-xs-12 col-sm-3 col-md-2 col-lg-2 col-sm-offset-1 col-md-offset-1 col-lg-offset-2">
                                        <label class="control-label text-left">Tipo de Cambio:</label>
                                    </div>
                                        <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4">
                                        <asp:DropDownList ID="ddlTipoCambio" runat="server" Enabled="false" CssClass="form-control">
                                        </asp:DropDownList>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group hide">
                                <div class="row">
                                    <div class="col-xs-12 col-sm-3 col-md-2 col-lg-2 col-sm-offset-1 col-md-offset-1 col-lg-offset-2">
                                        <label class="control-label text-right">Almacén Despacho:</label>
                                    </div>
                                    <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4">
                                        <asp:DropDownList ID="ddlAlmacen" runat="server" CssClass="form-control">
                                        </asp:DropDownList>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!--FIN PRE VENTA -->

                <!--CLIENTE -->
                <div id="collapseDatosCli" runat="server" class="tab-pane fade">
                    <div class="panel panel-default">
                        <div class="panel-body">
                            <asp:UpdatePanel ID="udpCliente" runat="server" UpdateMode="Conditional">
                                <ContentTemplate>
                                    <div class="form-group">
                                        <div class="row">
                                            <div class="col-xs-12 col-sm-3 col-md-2 col-lg-2 col-sm-offset-1 col-md-offset-1 col-lg-offset-2">
                                                <label class="control-label text-left">RUC/DNI:</label>
                                            </div>
                                            <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4">
                                                <asp:AutoCompleteExtender ID="aceBuscarCliente" runat="server" ServiceMethod="BuscarClienteXDocumento"
                                                    MinimumPrefixLength="3" Enabled="true" UseContextKey="true" ServicePath="~/WSArticulos.asmx" 
                                                    CompletionInterval="100" EnableCaching="false" CompletionSetCount="10" TargetControlID="txtDocCliente"
                                                    FirstRowSelected="false" CompletionListCssClass="completionList" 
                                                    CompletionListItemCssClass="listItem" CompletionListHighlightedItemCssClass="itemHighlighted">
                                                </asp:AutoCompleteExtender>
                                                <div class="input-group">
                                                    <asp:HiddenField ID="hfCodigoCliente" runat="server" Value="null" />
                                                    <asp:TextBox ID="txtDocCliente" placeholder="Ingrese N° documento" runat="server" CssClass="form-control"/>
                                                    <span class="input-group-btn">
                                                        <asp:ImageButton ID="btnBuscarCliente" runat="server" CssClass="image-btn" ImageUrl="~/Resource/images/ico-find32.png"
                                                            AlternateText="Buscar" ImageAlign="AbsMiddle" OnClick="btnBuscarCliente_Click"></asp:ImageButton>
                                                    </span>
                                                </div>
                                            </div>
                                            <div class="col-xs-2 col-sm-2 col-md-2 col-lg-1">
                                                <asp:Button ID="btnNuevoCliente" runat="server" Text=" + " CssClass="btn btn-success bolder" OnClick="btnNuevoCliente_Click"/>
                                            </div>
                                            <style>
                                                .bolder {
                                                    font-weight:bolder;
                                                }
                                            </style>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <div class="row">
                                            <div class="col-xs-12 col-sm-3 col-md-2 col-lg-2 col-sm-offset-1 col-md-offset-1 col-lg-offset-2">
                                                <label class="control-label text-left">Raz&oacute;n Social:</label>
                                            </div>
                                            <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4">
                                                <asp:TextBox ID="txtRazNomCliente" ReadOnly="true" runat="server" CssClass="form-control">
                                                </asp:TextBox>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <div class="row">
                                            <div class="col-xs-12 col-sm-3 col-md-2 col-lg-2 col-sm-offset-1 col-md-offset-1 col-lg-offset-2">
                                                <label class="control-label text-left">Direcci&oacute;n:</label>
                                            </div>
                                            <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4">
                                                <asp:TextBox ID="txtDirCliente" ReadOnly="true" runat="server" CssClass="form-control"/>
                                            </div>
                                        </div>
                                    </div>
                                </ContentTemplate>
                                <Triggers>
                                    <asp:AsyncPostBackTrigger ControlID="btnBuscarCliente" EventName="Click" />
                                    <asp:AsyncPostBackTrigger ControlID="btnNuevoCliente" EventName="Click" />
                                </Triggers>
                            </asp:UpdatePanel>
                        </div>
                    </div>
                </div>
            <!-- FIN CLIENTE -->

            <!-- INI PANEL ARTICULO -->
                <div id="collapseBusquedaArt" runat="server" class="tab-pane fade">
                    <div class="panel panel-default">
                        <div class="panel-body">
                            <!-- INI PANEL BUSQUEDA ARTICULO -->
                            <div class="panel panel-danger">
                                <div class="panel-heading">
                                    <h2 class="panel-title">Búsqueda</h2>
                                </div>
                                <div class="panel-body autocomplete">
                                    <div class="form-group">
                                        <div class="row">
                                            <div class="col-xs-12 col-sm-3 col-md-2 col-lg-2 col-sm-offset-1 col-md-offset-1 col-lg-offset-2">
                                                <label class="control-label text-left">¿Es Combo?</label>
                                            </div>      
                                            <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4">
                                                &nbsp;&nbsp;<asp:CheckBox ID="chkIsCombo" runat="server" AutoPostBack="true" OnCheckedChanged="chkIsCombo_CheckedChanged"/>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <div class="row">
                                            <div class="col-xs-12 col-sm-3 col-md-2 col-lg-2 col-sm-offset-1 col-md-offset-1 col-lg-offset-2">
                                                <label class="control-label text-left">C&oacute;digo:</label>
                                            </div>                                        
                                            <script type="text/javascript">
                                                function setContextKey() {
                                                    $find("aceBuscarCodigo").set_contextKey(document.getElementById("chkIsCombo").checked);
                                                    $find("aceBuscarNombre").set_contextKey(document.getElementById("chkIsCombo").checked);
                                                    return;
                                                }
                                            </script>
                                            <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4">
                                                <div class="input-group">
                                                    <asp:TextBox ID="txtCodigoArt" placeholder="Buscar Código" runat="server" CssClass="form-control" 
                                                        onkeyup="return setContextKey();"/>
                                                    <span class="input-group-btn">                                                    
                                                         <asp:ImageButton ID="ibtnBuscarAxCod" runat="server" CssClass="image-btn" 
                                                            ImageUrl="~/Resource/images/ico-find32.png" AlternateText="Buscar" OnClick="ibtnBuscarAxCod_Click"/>
                                                    </span>
                                                </div>
                                                <asp:AutoCompleteExtender ID="aceBuscarCodigo" runat="server" ServiceMethod="BuscarArticuloXCodigo"
                                                    MinimumPrefixLength="3" Enabled="true" UseContextKey="true" ServicePath="~/WSArticulos.asmx" 
                                                    CompletionInterval="100" EnableCaching="false" CompletionSetCount="10" TargetControlID="txtCodigoArt"
                                                    FirstRowSelected="false" CompletionListCssClass="completionList" 
                                                    CompletionListItemCssClass="listItem" CompletionListHighlightedItemCssClass="itemHighlighted">
                                                </asp:AutoCompleteExtender>
                                                <asp:AutoCompleteExtender ID="aceBuscarNombre" runat="server" ServiceMethod="BuscarArticuloXNombre"
                                                    MinimumPrefixLength="4" Enabled="true" UseContextKey="true" ServicePath="~/WSArticulos.asmx"
                                                    CompletionInterval="100" EnableCaching="false" CompletionSetCount="10" TargetControlID="txtNombreArt"
                                                    FirstRowSelected="false" CompletionListCssClass="completionList"
                                                    CompletionListItemCssClass="listItem" CompletionListHighlightedItemCssClass="itemHighlighted">
                                                </asp:AutoCompleteExtender>
                                            </div>                
                                        </div>
                                    </div>

                                     <div class="form-group">
                                        <div class="row">
                                            <div class="col-xs-12 col-sm-3 col-md-2 col-lg-2 col-sm-offset-1 col-md-offset-1 col-lg-offset-2">
                                                <label class="control-label text-left">Descripci&oacute;n:</label>
                                            </div>
                                            <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4">
                                                <div class="input-group">
                                                    <asp:TextBox ID="txtNombreArt" placeholder="Buscar descripción" runat="server" CssClass="form-control"
                                                    onkeyup="return setContextKey();"/>
                                                    <span class="input-group-btn">
                                                        <asp:ImageButton ID="ibtnBuscarAxDesc" runat="server" CssClass="image-btn" 
                                                            ImageUrl="~/Resource/images/ico-find32.png" AlternateText="Buscar" OnClick="ibtnBuscarAxDesc_Click"/>
                                                    </span>
                                                </div>
                                            </div>  
                                        </div>
                                    </div>

                                    <div id="divArticuloDetail" runat="server">
                                        <div class="form-group">
                                            <div class="row">
                                                <div class="col-xs-12 col-sm-3 col-md-2 col-lg-2 col-sm-offset-1 col-md-offset-1 col-lg-offset-2">
                                                    <label class="control-label text-left">Packing:</label>
                                                </div>
                                                <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4">
                                                    <asp:Label ID="lblPacking" runat="server" Text="----------------" CssClass="form-control"/>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="row">
                                                <div class="col-xs-12 col-sm-3 col-md-2 col-lg-2 col-sm-offset-1 col-md-offset-1 col-lg-offset-2">
                                                    <label class="control-label text-left">Observaci&oacute;n:</label>
                                                </div>
                                                <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4">
                                                    <asp:TextBox ID="txtObservacion" placeholder="Ingrese alguna observación" runat="server" CssClass="form-control"/>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- FIN PANEL BUSQUEDA ARTICULO -->

                            <!-- INI DE LOS PANELES DE CONSOLIDADO DE ARTICULO -->
                            <asp:HiddenField runat="server" ID="hdnConsolidadoArticulo" Value="hidden"/>
                            <asp:HiddenField runat="server" ID="hdnConsolidados" Value="hidden"/>
                            <div id="divConsolidados" hidden="hidden">
                                <!-- INI PANEL CONSOLIDADO ARTICULO -->
                                <div class="panel panel-warning" id="divConsolidadoArticulo" hidden="hidden">
                                    <div class="panel-heading">
                                        <h2 class="panel-title">ART&Iacute;CULOS</h2>
                                    </div>
                                    <div class="panel-body">
                                        <div class="form-group">
                                            <div class="table">
                                                <div class="ancho">
                                                    <asp:GridView ID="grvConsolidado" runat="server" AutoGenerateColumns="false" EmptyDataText="No hay Registros"
                                                        CssClass="table table-bordered table-condensed" OnSelectedIndexChanging="grvConsolidado_SelectedIndexChanging">
                                                        <Columns>
                                                            <asp:BoundField DataField="NOM_EMPRESA" HeaderText="EMPRESA" HtmlEncode="false" />
                                                            <asp:BoundField DataField="COD_ARTICULO" HeaderText="CÓDIGO" HtmlEncode="false" />
                                                            <asp:BoundField DataField="NOM_ARTICULO" HeaderText="ARTICULO" HtmlEncode="false" />
                                                            <asp:BoundField DataField="CAJA" HeaderText="CAJ" HtmlEncode="false" />
                                                            <asp:BoundField DataField="DOCENA" HeaderText="DOC" HtmlEncode="false" />
                                                            <asp:BoundField DataField="UNIDAD" HeaderText="UND" HtmlEncode="false" />
                                                            <asp:BoundField DataField="COD_MAESTRO" ItemStyle-CssClass="Hide" HeaderStyle-CssClass="Hide" HtmlEncode="false" />
                                                            <asp:BoundField DataField="COD_EMPRESA" ItemStyle-CssClass="Hide" HeaderStyle-CssClass="Hide" HtmlEncode="false" />
                                                            <asp:BoundField DataField="COD_EMPRESA_ART" ItemStyle-CssClass="Hide" HeaderStyle-CssClass="Hide" HtmlEncode="false" />
                                                            <asp:BoundField DataField="COD_UMD_PACK" ItemStyle-CssClass="Hide" HeaderStyle-CssClass="Hide" HtmlEncode="false" />
                                                            <asp:BoundField DataField="CAN_PACKING" ItemStyle-CssClass="Hide" HeaderStyle-CssClass="Hide" HtmlEncode="false" />
                                                            <asp:BoundField DataField="CAN_MAYOR" ItemStyle-CssClass="Hide" HeaderStyle-CssClass="Hide" HtmlEncode="false" />
                                                            <asp:BoundField DataField="NOMBRE_UMEDIDA" ItemStyle-CssClass="Hide" HeaderStyle-CssClass="Hide" HtmlEncode="false" />
                                                            <asp:BoundField DataField="PRECIO_MAX" ItemStyle-CssClass="Hide" HeaderStyle-CssClass="Hide" HtmlEncode="false" />
                                                            <asp:BoundField DataField="PRECIO_MINIMO" ItemStyle-CssClass="Hide" HeaderStyle-CssClass="Hide" HtmlEncode="false" />
                                                            <asp:BoundField DataField="PRECIO_PROM" ItemStyle-CssClass="Hide" HeaderStyle-CssClass="Hide" HtmlEncode="false" />
                                                            <asp:BoundField DataField="PRECIO_TIENDA" ItemStyle-CssClass="Hide" HeaderStyle-CssClass="Hide" HtmlEncode="false" />
                                                            <asp:BoundField DataField="PRECIO_COSTO" ItemStyle-CssClass="Hide" HeaderStyle-CssClass="Hide" HtmlEncode="false" />
                                                            <asp:BoundField DataField="TXT_PACKING" ItemStyle-CssClass="Hide" HeaderStyle-CssClass="Hide" HtmlEncode="false" />
                                                            <asp:BoundField DataField="PRECIO_COSTO_IGV_UTIL" ItemStyle-CssClass="Hide" HeaderStyle-CssClass="Hide" HtmlEncode="false" />
                                                            <asp:TemplateField ItemStyle-HorizontalAlign="Center" ItemStyle-Font-Bold="true">
                                                                <ItemTemplate>
                                                                    <asp:Button ID="btnSolicitar" runat="server" CssClass="btn btn-success btn-sm" CommandName="Select" Text="Obtener"/>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                        </Columns>
                                                    </asp:GridView>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- FIN PANEL CONSOLIDADO ARTICULO -->

                                <!-- INI PANEL CONSOLIDADO ARTICULO POR UBICACION-->
                                <div class="panel panel-primary">
                                    <div class="panel-heading">
                                        <h2 class="panel-title">ART&Iacute;CULO POR UBICACI&Oacute;N</h2>
                                    </div>
                                    <div class="panel-body">
                                        <div class="form-group">
                                            <div class="table">
                                                <div class="ancho">
                                                    <asp:GridView ID="grvConsolidadoAUbicacion" runat="server" AutoGenerateColumns="false" EmptyDataText="No hay Registros"
                                                        CssClass="table table-bordered table-condensed" OnSelectedIndexChanging="grvConsolidadoAUbicacion_SelectedIndexChanging">
                                                        <Columns>
                                                            <asp:BoundField DataField="NOM_EMPRESA" HeaderText="Empresa" HtmlEncode="false" ItemStyle-CssClass="Hide" HeaderStyle-CssClass="Hide" />
                                                            <asp:BoundField DataField="COD_ARTICULO" HeaderText="Código" HtmlEncode="false" />
                                                            <asp:BoundField DataField="NOM_ARTICULO" HeaderText="Nombre Artículo" HtmlEncode="false" />
                                                            <asp:BoundField DataField="NOMBRE_ALMACEN" HeaderText="ALM" HtmlEncode="false" />
                                                            <asp:BoundField DataField="NOMBRE_UBICACION" HeaderText="UBIC" HtmlEncode="false" />
                                                            <asp:BoundField DataField="CAJA" HeaderText="CJA" HtmlEncode="false" />
                                                            <asp:BoundField DataField="DOCENA" HeaderText="DOC" HtmlEncode="false" />
                                                            <asp:BoundField DataField="UNIDAD" HeaderText="UND" HtmlEncode="false" />
                                                            <asp:BoundField DataField="PK_ALMACEN" ItemStyle-CssClass="Hide" HeaderStyle-CssClass="Hide" HtmlEncode="false" />
                                                            <asp:BoundField DataField="PK_UBICACIONES" ItemStyle-CssClass="Hide" HeaderStyle-CssClass="Hide" HtmlEncode="false" />
                                                            <asp:BoundField DataField="COD_MAESTRO" ItemStyle-CssClass="Hide" HeaderStyle-CssClass="Hide" HtmlEncode="false" />
                                                            <asp:BoundField DataField="COD_EMPRESA" ItemStyle-CssClass="Hide" HeaderStyle-CssClass="Hide" HtmlEncode="false" />
                                                            <asp:BoundField DataField="COD_EMPRESA_ART" ItemStyle-CssClass="Hide" HeaderStyle-CssClass="Hide" HtmlEncode="false" />
                                                            <asp:BoundField DataField="COD_UMD_PACK" ItemStyle-CssClass="Hide" HeaderStyle-CssClass="Hide" HtmlEncode="false" />
                                                            <asp:BoundField DataField="CAN_PACKING" ItemStyle-CssClass="Hide" HeaderStyle-CssClass="Hide" HtmlEncode="false" />
                                                            <asp:BoundField DataField="CAN_MAYOR" ItemStyle-CssClass="Hide" HeaderStyle-CssClass="Hide" HtmlEncode="false" />
                                                            <asp:BoundField DataField="IMPRESORA" ItemStyle-CssClass="Hide" HeaderStyle-CssClass="Hide" HtmlEncode="false" />
                                                            <asp:BoundField DataField="NOMBRE_UMEDIDA" ItemStyle-CssClass="Hide" HeaderStyle-CssClass="Hide" HtmlEncode="false" />
                                                            <asp:BoundField DataField="PRECIO_MAX" ItemStyle-CssClass="Hide" HeaderStyle-CssClass="Hide" HtmlEncode="false" />
                                                            <asp:BoundField DataField="PRECIO_MINIMO" ItemStyle-CssClass="Hide" HeaderStyle-CssClass="Hide" HtmlEncode="false" />
                                                            <asp:BoundField DataField="PRECIO_PROM" ItemStyle-CssClass="Hide" HeaderStyle-CssClass="Hide" HtmlEncode="false" />
                                                            <asp:BoundField DataField="PRECIO_COSTO" ItemStyle-CssClass="Hide" HeaderStyle-CssClass="Hide" HtmlEncode="false" />
                                                            <asp:BoundField DataField="PRECIO_TIENDA" ItemStyle-CssClass="Hide" HeaderStyle-CssClass="Hide" HtmlEncode="false" />
                                                            <asp:BoundField DataField="REPOSICION" ItemStyle-CssClass="Hide" HeaderStyle-CssClass="Hide" HtmlEncode="false" />
                                                            <asp:BoundField DataField="TXT_PACKING" ItemStyle-CssClass="Hide" HeaderStyle-CssClass="Hide" HtmlEncode="false" />
                                                            <asp:BoundField DataField="PRECIO_COSTO_IGV_UTIL" ItemStyle-CssClass="Hide" HeaderStyle-CssClass="Hide" HtmlEncode="false" />

                                                            <asp:TemplateField ItemStyle-HorizontalAlign="Center" ItemStyle-Font-Bold="true">
                                                                <ItemTemplate>
                                                                    <asp:Button ID="btnSolicitarCUA" runat="server" CssClass="btn btn-success btn-sm" CommandName="Select" Text="Obtener" />
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                        </Columns>
                                                    </asp:GridView>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <asp:HiddenField ID="hfCurrentRowConsolidado" runat="server" Value="null" />
                                <asp:HiddenField ID="hfCurrentRowConsArticulo" runat="server" Value="null" />
                                <!-- FIN PANEL CONSOLIDADO ARTICULO POR UBICACION-->

                                <!-- INI PANEL CONSOLIDADO ARTICULO POR ALMACEN-->
                                <div class="panel panel-success">
                                    <div class="panel-heading">
                                        <h2 class="panel-title">ART&Iacute;CULO POR ALMAC&Eacute;N</h2>
                                    </div>
                                    <div class="panel-body">
                                        <div class="form-group">
                                            <div class="row">
                                                <div class="col-xs-6 col-sm-4 col-md-2">
                                                    <asp:Label ID="lblAlmacen" runat="server"></asp:Label>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="row">
                                                <div class="col-xs-3 col-sm-2 col-md-2">
                                                    <label class="text-left">Cajas:</label>
                                                </div>
                                                <div class="col-xs-3 col-sm-2 col-md-2">
                                                    <asp:Label ID="lblCajas" runat="server" CssClass="form-control"></asp:Label>
                                                </div>
                                                <div class="col-xs-3 col-sm-2 col-md-2">
                                                    <label class="text-left">Docenas:</label>
                                                </div>
                                                <div class="col-xs-3 col-sm-2 col-md-2">
                                                    <asp:Label ID="lblDocenas" runat="server" CssClass="form-control"></asp:Label>
                                                </div>
                                                <div class="col-xs-3 col-sm-2 col-md-2">
                                                    <label class="text-left">Unidades:</label>
                                                </div>
                                                <div class="col-xs-3 col-sm-2 col-md-2">
                                                    <asp:Label ID="lblUnidad" runat="server" CssClass="form-control"></asp:Label>
                                                </div>
                                                <div class="col-xs-3 col-sm-2 col-md-2">
                                                    <label class="text-left">Total:</label>
                                                </div>
                                                <div class="col-xs-3 col-sm-2 col-md-2">
                                                    <asp:Label ID="lblTotal" runat="server" CssClass="form-control"></asp:Label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- FIN PANEL CONSOLIDADO ARTICULO POR ALMACEN-->
                            </div>
                            <!-- FIN DE LOS PANELES DE CONSOLIDADO DE ARTICULO -->
                        </div>
                    </div>
                </div>
            <!-- END PANEL ARTICULO -->

            <!-- INI DETALLE PRE -->
                <div id="collapseDetallePre" runat="server" class="tab-pane fade">
                    <div class="panel panel-default">
                        <div class="panel-body">
                            <div class="form-group">
                                <div class="table">
                                    <div class="ancho">
                                        <asp:GridView ID="grvDetPreVenta" EmptyDataText="No hay Registros" runat="server" AutoGenerateColumns="false"
                                            CssClass="table-striped table-hover table-condensed table-bordered ancho" ShowHeaderWhenEmpty="true"
                                            OnRowDeleting="grvDetPreVenta_RowDeleting">
                                            <Columns>
                                                <asp:BoundField DataField="ORDEN_ITEM" HeaderText="N°" />
                                                <asp:BoundField DataField="COD_ARTICULO" HeaderText="ARTICULO" />
                                                <asp:BoundField DataField="DESC_ARTICULO" HeaderText="NOMBRE" />
                                                <asp:BoundField DataField="DESC_PACKING" HeaderText="PACKING" />
                                                <asp:BoundField DataField="DESC_EMPRESA" HeaderText="EMPRESA" ItemStyle-CssClass="Hide" HeaderStyle-CssClass="Hide"/>
                                                <asp:BoundField DataField="DESC_ALMACEN" HeaderText="ALM" />
                                                <asp:BoundField DataField="DESC_UBICACION" HeaderText="UBIC" />
                                                <asp:BoundField DataField="CAJAS" HeaderText="CJA" />
                                                <asp:BoundField DataField="DOCENA" HeaderText="DOC" />
                                                <asp:BoundField DataField="UNIDAD_SET" HeaderText="UND" />
                                                <asp:BoundField DataField="CANT_PACKING" HeaderText="CANTIDAD" ItemStyle-CssClass="Hide" HeaderStyle-CssClass="Hide" />                                                
                                                <asp:BoundField DataField="UND_TOTAL_PEDIDA" HeaderText="UND TOT" />
                                                <asp:BoundField DataField="CANT_MAYOR" ItemStyle-CssClass="Hide" HeaderStyle-CssClass="Hide" HeaderText="CANTIDAD MAYOR" />
                                                <asp:BoundField DataField="P_COSTO" ItemStyle-CssClass="Hide" HeaderStyle-CssClass="Hide" HeaderText="COSTO" />
                                                <asp:BoundField DataField="P_PRECIO" HeaderText="PRECIO" />
                                                <asp:BoundField DataField="P_TOTAL" HeaderText="TOTAL" />
                                                <asp:TemplateField ItemStyle-HorizontalAlign="Center">
                                                    <ItemTemplate>
                                                        <asp:CheckBox ID="chkDPVDescuento" runat="server" Checked='<%# Eval("DESCUENTO") %>' />
                                                    </ItemTemplate>
                                                    <HeaderTemplate>
                                                        <label>DSCTO</label>&nbsp;
                                                        <asp:CheckBox ID="chkHeader" ToolTip="Descuento a todos" runat="server"
                                                            onclick="changeAllCheckBoxes(this)"/>
                                                    </HeaderTemplate>
                                                </asp:TemplateField>                                                
                                                <asp:BoundField DataField="COMENTARIO" HeaderText="COMENTARIO"/>
                                                <asp:BoundField DataField="PK_EMPLEADO" ItemStyle-CssClass="Hide" HeaderStyle-CssClass="Hide" HtmlEncode="false" />
                                                <asp:BoundField DataField="FK_MAESTRO" ItemStyle-CssClass="Hide" HeaderStyle-CssClass="Hide" HtmlEncode="false" />
                                                <asp:BoundField DataField="FK_EMP_ART" ItemStyle-CssClass="Hide" HeaderStyle-CssClass="Hide" HtmlEncode="false" />
                                                <asp:BoundField DataField="FK_EMPRESA" ItemStyle-CssClass="Hide" HeaderStyle-CssClass="Hide" HtmlEncode="false" />
                                                <asp:BoundField DataField="FK_ALMACEN" ItemStyle-CssClass="Hide" HeaderStyle-CssClass="Hide" HtmlEncode="false" />
                                                <asp:BoundField DataField="FK_UBICACION" ItemStyle-CssClass="Hide" HeaderStyle-CssClass="Hide" HtmlEncode="false" />
                                                <asp:BoundField DataField="IP_IMPR" ItemStyle-CssClass="Hide" HeaderStyle-CssClass="Hide" HeaderText="Impresora" />
                                                <asp:BoundField DataField="PK_DET_PV" ItemStyle-CssClass="Hide" HeaderStyle-CssClass="Hide" HtmlEncode="false" />
                                                <asp:BoundField DataField="FK_PV" ItemStyle-CssClass="Hide" HeaderStyle-CssClass="Hide" HtmlEncode="false" />
                                                <asp:BoundField DataField="P_COSTO_IGV_K" ItemStyle-CssClass="Hide" HeaderStyle-CssClass="Hide" HtmlEncode="false" />
                                                <asp:BoundField DataField="PK_GRP_VENTA" HeaderText="COMBO" HtmlEncode="false" />
                                                <asp:TemplateField>
                                                    <ItemTemplate>
                                                        <asp:Button ID="btnDetalleDel" CommandName="Delete" runat="server" CssClass="btn btn-danger btn-sm" Text="Quitar"
                                                           OnClientClick="if(!confirm('¿Desea eliminar el producto?')){return false;}"/>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                            </Columns>
                                        </asp:GridView>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div style="padding:3%">
                                <div class="row">
                                    <div class="col-xs-12 col-sm-2 col-md-1 col-sm-offset-0 col-md-offset-6">
                                        <label class="control-label text-left">Total Caja:</label>
                                    </div>
                                    <div class="col-xs-4 col-sm-2 col-md-1">
                                        <asp:Label ID="lblTotCaja" runat="server" CssClass="form-control">
                                        </asp:Label>
                                    </div>
                                    <div class="col-xs-12 col-sm-2 col-md-1">
                                        <label class="control-label text-left">Total Doc.</label>
                                    </div>
                                    <div class="col-xs-4 col-sm-2 col-md-1">
                                        <asp:Label ID="lblTotDoc" runat="server" CssClass="form-control">
                                        </asp:Label>
                                    </div>
                                    <div class="col-xs-12 col-sm-2 col-md-1">
                                        <label class="control-label text-left">Total Und.:</label>
                                    </div>
                                    <div class="col-xs-4 col-sm-2 col-md-1">
                                        <asp:Label ID="lblTotUnd" runat="server" CssClass="form-control">
                                        </asp:Label>
                                    </div>
                                </div>
                                <br />
                                <div class="row">
                                    <div class="col-xs-12 col-sm-2 col-md-1 col-sm-offset-0 col-md-offset-6">
                                        <label class="control-label text-left">Importe Total:</label>
                                    </div>
                                    <div class="col-xs-4 col-sm-2 col-md-1">
                                        <asp:Label ID="lblImporteTotal" runat="server" CssClass="form-control">
                                        </asp:Label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="row">
                                <div class="col-xs-10 col-sm-2 col-md-1 col-xs-offset-1 col-sm-offset-1 col-md-offset-2">
                                    <label class="control-label text-left">Comentario:</label>
                                </div>
                                <div class="col-xs-10 col-sm-10 col-md-7 col-xs-offset-1 col-sm-offset-1 col-md-offset-1">
                                    <asp:TextBox ID="txtComentario" TextMode="MultiLine" Rows="3" runat="server" CssClass="form-control">
                                    </asp:TextBox>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- FIN DETALLE PRE -->
            </div>

            <div class="form-group">
                <div class="text-center">
                    <asp:Button ID="btnGrabar" Text="Guardar Pre-Venta" runat="server" CssClass="btn btn-success btn-lg" 
                        OnClick="btnGrabar_Click" OnClientClick="if(!confirm('¿Desea Guardar la Pre-Venta?')){return false;}"></asp:Button>&nbsp;
                
                    <asp:Button ID="btnImprimir" Text="Imprimir Pre-Venta" runat="server" Visible="false" 
                        CssClass="btn btn-warning btn-lg" OnClick="btnImprimir_Click"></asp:Button>&nbsp;

                    <asp:Button ID="btnCancelar" Text="Cancelar" runat="server" Visible="True" 
                        CssClass="btn btn-danger btn-lg" OnClick="btnCancelar_Click"  OnClientClick="if(!confirm('¿Desea Cencelar la Pre-Venta?')){return false;}"></asp:Button>

                </div>
            </div>
            <asp:Label ID="MostrarAccordion" runat="server" Text="P" Style="display: none;"></asp:Label>
        </asp:Panel>

        <!-- ************* INICIO DEL LOS MODAL POPUPS  ***************** -->

        <!--INI POP UP ON LOAD PAGE-->
        <asp:Button ID="btnDefaultLoad" runat="server" Style="display: none" />
        <asp:ModalPopupExtender ID="MPopLoad" runat="server" Enabled="true" PopupControlID="pnlLoadPreVenta"
            PopupDragHandleControlID="pnlLoadPreVenta" TargetControlID="btnDefaultLoad">
        </asp:ModalPopupExtender>
        <asp:Panel ID="pnlLoadPreVenta" runat="server" DefaultButton="btnLoadNueva">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="text-center modal-title">Búsqueda de Pre-Venta / Registro</h4>
                    </div>
                    <div class="modal-body">
                        <div class="form-group">
                            <div class="row">
                                <div class="col-xs-12 col-sm-4 col-md-2 col-lg-2 col-lg-offset-2">
                                    <label class="control-label text-right">N° Pre-Venta:</label>
                                </div>
                                <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4">
                                    <asp:TextBox ID="txtLoadCodigo" runat="server" CssClass="form-control"/>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="row">
                                <div class="col-xs-12 col-sm-4 col-md-2 col-lg-2 col-lg-offset-2">
                                    <label class="control-label text-right">Clase:</label>
                                </div>
                                <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4">
                                    <asp:DropDownList ID="ddlLoadClase" runat="server" CssClass="form-control">
                                        <asp:ListItem Text="[SELECCIONE]" Value="null"></asp:ListItem>
                                            <asp:ListItem Text="I" Value="I"></asp:ListItem>
                                            <asp:ListItem Text="M" Value="M"></asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <asp:Button ID="btnLoadBuscar" Text="Buscar"  runat="server" CssClass="btn btn-warning btn-lg"
                            OnClick="btnLoadBuscar_Click"></asp:Button>
                        <asp:Button ID="btnLoadNueva" runat="server" Text="Nueva Pre-Venta" CssClass="btn btn-success btn-lg"
                            OnClick="btnLoadNueva_Click"></asp:Button>
                    </div>
                </div>
            </div>
        </asp:Panel>
        <!--END POP UP ON LOAD PAGE-->

        <!--INI POP UP REGISTRAR CLIENTE-->
        <asp:Button ID="btnHandlerPop3" runat="server" Style="display: none" />
        <asp:ModalPopupExtender ID="MPopRegistrarCliente" runat="server" Enabled="true" PopupControlID="pnlRegistrarCliente"
            PopupDragHandleControlID="pnlRegistrarCliente" Drag="true" TargetControlID="btnHandlerPop3" CancelControlID="btnCerrarRCliente">
        </asp:ModalPopupExtender>
        <asp:Panel ID="pnlRegistrarCliente" runat="server" DefaultButton="btnGuardarRCliente">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="text-center modal-title">Registrar Cliente</h4>
                    </div>
                    <div class="modal-body">
                        <asp:UpdatePanel ID="upnlRCliente" runat="server" UpdateMode="Conditional">
                            <ContentTemplate>
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-xs-3 col-sm-3 col-md-3">
                                            <label class="control-label text-right">Tipo Cliente:</label>
                                        </div>
                                        <div class="col-xs-9 col-sm-8 col-md-6">
                                            <asp:RadioButton ID="rbPNatural" GroupName="rbgTPersona" AutoPostBack="true"
                                                OnCheckedChanged="rbPNatural_CheckedChanged" Text="&nbsp;&nbsp;&nbsp;Natural" Checked="true" runat="server" />&nbsp;&nbsp;&nbsp;
                                            <asp:RadioButton ID="rbPJuridica" GroupName="rbgTPersona" AutoPostBack="true"
                                                OnCheckedChanged="rbPJuridica_CheckedChanged" Text="&nbsp;&nbsp;&nbsp;Juridica" runat="server" />
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-xs-3 col-sm-3 col-md-3">
                                            <label id="pRPDocumento" runat="server" class="control-label text-left">N° Dni:</label>
                                        </div>
                                        <div class="col-xs-9 col-sm-6 col-md-6">
                                            <asp:TextBox ID="txtRPDocumento" runat="server" type="number" placeholder="Ingrese N° documento" CssClass="form-control" MaxLength="11"/>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="row">
                                         <div class="col-xs-3 col-sm-3 col-md-3">
                                            <label id="pRPContacto" runat="server" class="control-label text-left">Persona Contacto:</label>
                                        </div>
                                        <div class="col-xs-9 col-sm-9 col-md-9">
                                            <asp:TextBox ID="txtRPContacto" runat="server" placeholder="Ingrese su nombre" CssClass="form-control"/>
                                        </div>
                                    </div>
                                </div>
                                <div id="divRPJuridica" runat="server" hidden="hidden">
                                    <div class="form-group">
                                        <div class="row">
                                            <div class="col-xs-3 col-sm-3 col-md-3">
                                                <label class="control-label text-left">Teléfono:</label>
                                            </div>
                                            <div class="col-xs-9 col-sm-6 col-md-6">
                                                <asp:TextBox ID="txtRPFono" runat="server" placeholder="Ingrese telefono" CssClass="form-control">
                                                </asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="row">
                                            <div class="col-xs-3 col-sm-3 col-md-3">
                                                <label class="control-label text-left">Raz&oacute;n Social:</label>
                                            </div>
                                            <div class="col-xs-9 col-sm-9 col-md-9">
                                                <asp:TextBox ID="txtRPRazon" runat="server" placeholder="Ingrese Razón" CssClass="form-control">
                                                </asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-xs-3 col-sm-3 col-md-3">
                                            <label class="control-label text-left">Direcci&oacute;n Principal:</label>
                                        </div>
                                        <div class="col-xs-9 col-sm-9 col-md-9">
                                            <asp:TextBox ID="txtRPDireccion" placeholder="Ingrese dirección" runat="server" CssClass="form-control">
                                            </asp:TextBox>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-xs-3 col-sm-3 col-md-3">
                                            <label class="control-label text-left">Departamento:</label>
                                        </div>
                                        <div class="col-xs-9 col-sm-6 col-md-6">
                                            <asp:DropDownList ID="ddlDepartamento" AutoPostBack="true" runat="server"
                                                CssClass="form-control" OnSelectedIndexChanged="ddlDepartamento_SelectedIndexChanged">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-xs-3 col-sm-3 col-md-3">
                                            <label class="control-label text-left">Provincia:</label>
                                        </div>
                                        <div class="col-xs-9 col-sm-6 col-md-6">
                                            <asp:DropDownList ID="ddlProvincia" AutoPostBack="true" runat="server" Enabled="false"
                                                CssClass="form-control" OnSelectedIndexChanged="ddlProvincia_SelectedIndexChanged">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-xs-3 col-sm-3 col-md-3">
                                            <label class="control-label text-left">Distrito:</label>
                                        </div>
                                        <div class="col-xs-9 col-sm-6 col-md-6">
                                            <asp:DropDownList ID="ddlDistrito" runat="server" CssClass="form-control" Enabled="false">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-xs-3 col-sm-3 col-md-3">
                                            <label class="control-label text-left">Correo:</label>
                                        </div>
                                        <div class="col-xs-9 col-sm-9 col-md-9">
                                            <asp:TextBox ID="txtRPCorreo" placeholder="ejemplo@hotmail.com" runat="server"
                                                CssClass="form-control"></asp:TextBox>
                                        </div>
                                    </div>
                                </div>
                            </ContentTemplate>
                            <Triggers>
                                <asp:AsyncPostBackTrigger ControlID="ddlProvincia" EventName="SelectedIndexChanged" />
                                <asp:AsyncPostBackTrigger ControlID="ddlDepartamento" EventName="SelectedIndexChanged" />
                                <asp:AsyncPostBackTrigger ControlID="rbPJuridica" EventName="CheckedChanged" />
                                <asp:AsyncPostBackTrigger ControlID="rbPNatural" EventName="CheckedChanged" />
                                <asp:AsyncPostBackTrigger ControlID="chkIsCombo" EventName="CheckedChanged" />
                                <asp:AsyncPostBackTrigger ControlID="btnGuardarRCliente" EventName="Click" />
                                <asp:PostBackTrigger ControlID="btnCerrarRCliente" />
                            </Triggers>
                        </asp:UpdatePanel>
                    </div>
                    <div class="modal-footer">
                        <asp:Button ID="btnGuardarRCliente" runat="server" Text="Registrar" CssClass="btn btn-success btn-lg"
                            OnClick="btnGuardarRCliente_Click" OnClientClick="return validarCliente()"/>
                        <asp:Button ID="btnCerrarRCliente" runat="server" Text="Cerrar" CssClass="btn btn-danger btn-lg"
                            OnClick="btnCerrarRCliente_Click"/>
                    </div>
                </div>
            </div>
        </asp:Panel>
        <!--END POP UP REGISTRAR PREVENTA-->

        <!-- INI AGREGAR COMBOS 1234 -->
        <asp:Button ID="btnDefaultPop4" runat="server" Style="display: none" />
        <asp:ModalPopupExtender ID="MPopupAddCombo" runat="server" Enabled="true" PopupControlID="pnlPopAddCombo"
            PopupDragHandleControlID="pnlPopAddCombo" TargetControlID="btnDefaultPop4">
        </asp:ModalPopupExtender>
        <asp:Panel ID="pnlPopAddCombo" runat="server"  DefaultButton="btnPopCancelCombo">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="text-center modal-title"><asp:Label ID="lblTitlePopAddCombo" runat="server"/></h4>
                    </div>
                    <div class="modal-body" align="center">
                        <div class="form-group">
                            <div class="row center-block">
                                <div class="table">
                                    <asp:GridView ID="grvCombos" EmptyDataText="No hay Registros" runat="server" AutoGenerateColumns="false"
                                        CssClass="table-striped table-hover table-condensed table-bordered ancho" ShowHeaderWhenEmpty="true">
                                        <Columns>
                                            <asp:BoundField DataField="COD_ART_IMPORT" HeaderText="CÓDIGO" HtmlEncode="false" />
                                            <asp:BoundField DataField="NOMBRE_VENTA" HeaderText="ARTICULO" HtmlEncode="false" />
                                            <asp:BoundField DataField="FK_MAESTRO_ARTICULOS" HtmlEncode="false" HeaderStyle-CssClass="Hide" ItemStyle-CssClass="Hide"/>
                                            <asp:BoundField DataField="CANT_VTA" HtmlEncode="false" HeaderStyle-CssClass="Hide" ItemStyle-CssClass="Hide"/>
                                            <asp:BoundField DataField="NOMBRE_UND" HtmlEncode="false" HeaderStyle-CssClass="Hide" ItemStyle-CssClass="Hide"/>
                                            <asp:BoundField DataField="CANT_PACKING" HtmlEncode="false" HeaderStyle-CssClass="Hide" ItemStyle-CssClass="Hide"/>
                                            <asp:BoundField DataField="FK_EMPRESA" HtmlEncode="false" HeaderStyle-CssClass="Hide" ItemStyle-CssClass="Hide"/>
                                            <asp:BoundField DataField="NOMBRE_EMPRESA" HtmlEncode="false" HeaderStyle-CssClass="Hide" ItemStyle-CssClass="Hide"/>
                                            <asp:BoundField DataField="FK_EMPR_ARTC" HtmlEncode="false" HeaderStyle-CssClass="Hide" ItemStyle-CssClass="Hide"/>
                                            <asp:BoundField DataField="TXT_PACKING" HtmlEncode="false" HeaderStyle-CssClass="Hide" ItemStyle-CssClass="Hide"/>
                                            <asp:BoundField DataField="CANT_MAYOR" HtmlEncode="false" HeaderStyle-CssClass="Hide" ItemStyle-CssClass="Hide"/>
                                            <asp:BoundField DataField="PK_GRP_VENTA" HeaderText="COMBO" HtmlEncode="false"/>
                                        </Columns>
                                    </asp:GridView>                                
                                </div>
                            </div>
                            <br />
                            <div class="row">
                                <div class="col-xs-12 col-sm-10 col-md-10 col-sm-offset-1 col-md-offset-1">
                                    <asp:TextBox ID="txtComboCant" type="number" MaxLength="4" runat="server" CssClass="form-control" placeholder="Ingrese cantidad"/>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <asp:Button ID="btnPopAddCombo" runat="server" Text="Agregar" CssClass="btn btn-success" OnClientClick="return validarComboCantidad();" 
                            OnClick="btnPopAddCombo_Click"/>
                        <asp:Button ID="btnPopCancelCombo" runat="server" Text="Cancelar" CssClass="btn btn-danger" OnClick="btnPopCancelCombo_Click"/>
                    </div>
                    <script>
                        function validarComboCantidad() {

                            var cantidad = document.getElementById("txtComboCant");

                            if (cantidad.value.trim() == "" || cantidad.value.trim() == "0"  || isNaN(cantidad.value.trim())) {
                                cantidad.focus();
                                alert("Ingrese una valor correcto");
                                return false;
                            }
                        }
                    </script>
                </div>
            </div>
        </asp:Panel>
        <!--END POP UP AGREGAR COMBOS-->

        <!--INI POP UP INGRESAR CANTIDAD ARTICULOS-->
        <asp:Button ID="btnDefaultPopIn" runat="server" Style="display: none" />
        <asp:ModalPopupExtender ID="MPopupArticuloCant" runat="server" Enabled="true" PopupControlID="pnlPopArticuloCant"
            PopupDragHandleControlID="pnlPopArticuloCant" TargetControlID="btnDefaultPopIn">
        </asp:ModalPopupExtender>
        <asp:Panel ID="pnlPopArticuloCant" runat="server" DefaultButton="btnPopACancelar">
            <div class="modal-dialog modal-sm">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="text-center modal-title">Cantidades</h4>
                    </div>
                    <div class="modal-body">
                        <div class="form-group">
                            <div class="row">
                                <div class="col-xs-12 col-sm-4 col-md-4 col-sm-offset-1 col-md-offset-1">
                                    <label class="control-label text-left">Cajas:</label>
                                </div>
                                <div class="col-xs-12 col-sm-4 col-md-4">
                                    <asp:TextBox ID="txtPopACajas" type="number" runat="server" CssClass="form-control" >
                                    </asp:TextBox>
                                </div>
                            </div>
                            <br />
                            <div class="row">
                                <div class="col-xs-12 col-sm-4 col-md-4 col-sm-offset-1 col-md-offset-1">
                                    <label class="control-label text-left">Docenas:</label>
                                </div>
                                <div class="col-xs-12 col-sm-4 col-md-4">
                                    <asp:TextBox ID="txtPopADocenas" type="number" runat="server" CssClass="form-control">
                                    </asp:TextBox>
                                </div>
                            </div>
                            <br />
                            <div class="row">
                                <div class="col-xs-12 col-sm-4 col-md-4 col-sm-offset-1 col-md-offset-1">
                                    <label class="control-label text-left">Piezas / Set:</label>
                                </div>
                                <div class="col-xs-12 col-sm-4 col-md-4">
                                    <asp:TextBox ID="txtPopAPiezas" type="number" runat="server" CssClass="form-control">
                                    </asp:TextBox>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <asp:Button ID="btnPopAAgregar" runat="server" Text="Agregar" CssClass="btn btn-success"
                            OnClick="btnPopAAgregar_Click"></asp:Button>
                        <asp:Button ID="btnPopACancelar" runat="server" Text="Cancelar" CssClass="btn btn-danger"
                            OnClick="btnPopACancelar_Click"></asp:Button>
                    </div>
                </div>
            </div>
        </asp:Panel>
        <!--END POP UP INGRESAR CANTIDAD ARTICULOS-->
     <script>
         var prm = Sys.WebForms.PageRequestManager.getInstance();

         prm.add_endRequest(function () {
             $('#txtRPDocumento').keypress(function () {
                 if ($('#rbPNatural').is(':checked')) {
                     var documento = $('#txtRPDocumento').val();
                     if (documento.length > 7) {
                         $('#txtRPDocumento').val(documento.substring(0, 7));
                     }
                 }
                 else {
                     var documento = $('#txtRPDocumento').val();
                     if (documento.length > 10) {
                         $('#txtRPDocumento').val(documento.substring(0, 10));
                     }
                 }
             }); 
             if ($('#hdnConsolidados').val() == 'hidden') {
                 $('#divConsolidados').attr('hidden', 'hidden');
             } else {
                 $('#divConsolidados').removeAttr('hidden');
             }
             if ($('#hdnConsolidadoArticulo').val() == 'hidden') {
                 $('#divConsolidadoArticulo').attr('hidden', 'hidden');
             } else {
                 $('#divConsolidadoArticulo').removeAttr('hidden');
             }
         });

         $(document).ready(function () {
             $('#txtRPDocumento').keypress(function () {
                 if ($('#rbPNatural').is(':checked')) {
                     var documento = $('#txtRPDocumento').val();
                     if (documento.length > 7) {
                         $('#txtRPDocumento').val(documento.substring(0, 7));
                     }
                 }
                 else {
                     var documento = $('#txtRPDocumento').val();
                     if (documento.length > 10) {
                         $('#txtRPDocumento').val(documento.substring(0, 10));
                     }
                 }
             });
             if ($('#hdnShowConsolidados').val() == 'hidden') {
                 $('#divConsolidados').attr('hidden', 'hidden');
             } else {
                 $('#divConsolidados').removeAttr('hidden');
             }
             if ($('#hdnShowConsolidadosArticulos').val() == 'hidden') {
                 $('#divConsolidadoArticulo').attr('hidden', 'hidden');
             } else {
                 $('#divConsolidadoArticulo').removeAttr('hidden');
             }
         });

         function imprimirPreVenta() {
             codigoPreVenta = document.getElementById("txtCodigoPre").value;
             win = window.open("./ImprimirPreVenta.aspx?CodPreVentaGG=" + codigoPreVenta, "", "width=810,height=540,resizable=no,scrollbars=yes");
             win.focus();
         }

         function changeAllCheckBoxes(objparentcheckbox) {
             var HeaderCheckboxControl = objparentcheckbox
             var table = getParentByTagName(HeaderCheckboxControl, 'table');

             //get all the control of the type INPUT in the base control.
             var Inputs = table.getElementsByTagName("input");

             for (var n = 0; n < Inputs.length; ++n)
                 if (Inputs[n].type == 'checkbox') {
                     Inputs[n].checked = HeaderCheckboxControl.checked;
                 }
             return false;
         }
         function getParentByTagName(obj, tag) {
             var obj_parent = obj.parentNode;
             if (!obj_parent) return false;
             if (obj_parent.tagName.toLowerCase() == tag) return obj_parent;
             else return getParentByTagName(obj_parent, tag);
         }

         function validarCliente() {

             var contacto = document.getElementById("txtRPContacto");
             var document = document.getElementById("txtRPDocumento");
             var telefono = document.getElementById("txtRPFono");
             var razon = document.getElementById("txtRPRazon");
             var direccio = document.getElementById("txtRPDireccion");
             var departam = document.getElementById("ddlDepartamento");
             var provinci = document.getElementById("ddlProvincia");
             var distrito = document.getElementById("ddlDistrito");
             var correo = document.getElementById("txtRPCorreo");

             if (contacto.value.trim() = "") {
                 contacto.focus();
                 alert("Ingrese el nombre de contacto");
                 return false;
             }
             if (document.value.trim() = "") {
                 document.focus();
                 alert("Ingrese el documento");
                 return false;
             }
             if ($('#rbPJuridica').is(':checked')) {
                 if (telefono.value.trim() = "") {
                     telefono.focus();
                     alert("Ingrese el número de telefono");
                     return false;
                 }
                 if (razon.value.trim() = "") {
                     razon.focus();
                     alert("Ingrese la razón social");
                     return false;
                 }
             }
             if (direccio.value.trim() = "") {
                 direccio.focus();
                 alert("Ingrese la dirección");
                 return false;
             }
             if (departam.selectedIndex = "0") {
                 departam.focus();
                 alert("Seleccione el departamento");
                 return false;
             }
             if (provinci.selectedIndex = "0") {
                 provinci.focus();
                 alert("Seleccione la provincia");
                 return false;
             }
             if (distrito.selectedIndex = "0") {
                 distrito.focus();
                 alert("Seleccione el distrito");
                 return false;
             }
         }

         history.pushState(null, null, './RegistroPreVenta.aspx');
         window.addEventListener('popstate', function (event) {
             history.pushState(null, null, './RegistroPreVenta.aspx');
         });
            </script>

            <style type="text/css">
                .Hide {
                    display: none;
                }
                .ancho {
                    padding: 20px;
                    border: dotted 1px;
                    white-space: nowrap;
                    overflow-x: auto;
                }
                .completionList {
                    border: solid 2px;
                    padding: 3px;
                    margin: 0px;
                    /*
                    height: 120px;
                    overflow: auto;*/
                    list-style: none;
                    font-weight: bold;
                    background-color: #1eb3b7;
                }
                .listItem {
                    color: #000000;
                    list-style-position: initial;
                    list-style: none;
                    font-weight: bold;
                    font-size:x-large;
                }
                .itemHighlighted {
                    list-style: none;
                    font-weight: bold;
                    font-size:x-large;
                    background-color: #ff6a00;
                }
            </style>
        </ContentTemplate>
        <Triggers>
            <asp:PostBackTrigger ControlID="btnImprimir" />
        </Triggers>  
    </asp:UpdatePanel>

    <asp:UpdateProgress ID="uprgRegistroPre" runat="server">
        <ProgressTemplate>
            <div id="background">
                <div id="progress"></div>
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>

</asp:Content>
