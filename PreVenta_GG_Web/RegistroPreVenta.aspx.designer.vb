﻿'------------------------------------------------------------------------------
' <generado automáticamente>
'     Este código fue generado por una herramienta.
'
'     Los cambios en este archivo podrían causar un comportamiento incorrecto y se perderán si
'     se vuelve a generar el código. 
' </generado automáticamente>
'------------------------------------------------------------------------------

Option Strict On
Option Explicit On


Partial Public Class RegistroPreVenta

    '''<summary>
    '''Control upnlGeneralPanel.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents upnlGeneralPanel As Global.System.Web.UI.UpdatePanel

    '''<summary>
    '''Control pnlCreaPreVentas.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents pnlCreaPreVentas As Global.System.Web.UI.WebControls.Panel

    '''<summary>
    '''Control btnDefault.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents btnDefault As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''Control liPreVenta.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents liPreVenta As Global.System.Web.UI.HtmlControls.HtmlGenericControl

    '''<summary>
    '''Control liCliente.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents liCliente As Global.System.Web.UI.HtmlControls.HtmlGenericControl

    '''<summary>
    '''Control liArticulo.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents liArticulo As Global.System.Web.UI.HtmlControls.HtmlGenericControl

    '''<summary>
    '''Control liDetalle.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents liDetalle As Global.System.Web.UI.HtmlControls.HtmlGenericControl

    '''<summary>
    '''Control collapsePreVenta.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents collapsePreVenta As Global.System.Web.UI.HtmlControls.HtmlGenericControl

    '''<summary>
    '''Control hfCorrelativoPre.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents hfCorrelativoPre As Global.System.Web.UI.WebControls.HiddenField

    '''<summary>
    '''Control hfCodigoPreVenta.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents hfCodigoPreVenta As Global.System.Web.UI.WebControls.HiddenField

    '''<summary>
    '''Control hfEstadoPreVenta.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents hfEstadoPreVenta As Global.System.Web.UI.WebControls.HiddenField

    '''<summary>
    '''Control txtCodigoPre.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents txtCodigoPre As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''Control ddlClase.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents ddlClase As Global.System.Web.UI.WebControls.DropDownList

    '''<summary>
    '''Control upnlFormasPago.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents upnlFormasPago As Global.System.Web.UI.UpdatePanel

    '''<summary>
    '''Control chkContado.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents chkContado As Global.System.Web.UI.WebControls.RadioButton

    '''<summary>
    '''Control chkCredito.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents chkCredito As Global.System.Web.UI.WebControls.RadioButton

    '''<summary>
    '''Control ddlCredito.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents ddlCredito As Global.System.Web.UI.WebControls.DropDownList

    '''<summary>
    '''Control ddlTipoDocumento.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents ddlTipoDocumento As Global.System.Web.UI.WebControls.DropDownList

    '''<summary>
    '''Control ddlMoneda.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents ddlMoneda As Global.System.Web.UI.WebControls.DropDownList

    '''<summary>
    '''Control ddlTipoCambio.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents ddlTipoCambio As Global.System.Web.UI.WebControls.DropDownList

    '''<summary>
    '''Control ddlAlmacen.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents ddlAlmacen As Global.System.Web.UI.WebControls.DropDownList

    '''<summary>
    '''Control collapseDatosCli.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents collapseDatosCli As Global.System.Web.UI.HtmlControls.HtmlGenericControl

    '''<summary>
    '''Control udpCliente.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents udpCliente As Global.System.Web.UI.UpdatePanel

    '''<summary>
    '''Control aceBuscarCliente.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents aceBuscarCliente As Global.AjaxControlToolkit.AutoCompleteExtender

    '''<summary>
    '''Control hfCodigoCliente.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents hfCodigoCliente As Global.System.Web.UI.WebControls.HiddenField

    '''<summary>
    '''Control txtDocCliente.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents txtDocCliente As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''Control btnBuscarCliente.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents btnBuscarCliente As Global.System.Web.UI.WebControls.ImageButton

    '''<summary>
    '''Control btnNuevoCliente.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents btnNuevoCliente As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''Control txtRazNomCliente.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents txtRazNomCliente As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''Control txtDirCliente.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents txtDirCliente As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''Control collapseBusquedaArt.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents collapseBusquedaArt As Global.System.Web.UI.HtmlControls.HtmlGenericControl

    '''<summary>
    '''Control chkIsCombo.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents chkIsCombo As Global.System.Web.UI.WebControls.CheckBox

    '''<summary>
    '''Control txtCodigoArt.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents txtCodigoArt As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''Control ibtnBuscarAxCod.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents ibtnBuscarAxCod As Global.System.Web.UI.WebControls.ImageButton

    '''<summary>
    '''Control aceBuscarCodigo.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents aceBuscarCodigo As Global.AjaxControlToolkit.AutoCompleteExtender

    '''<summary>
    '''Control aceBuscarNombre.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents aceBuscarNombre As Global.AjaxControlToolkit.AutoCompleteExtender

    '''<summary>
    '''Control txtNombreArt.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents txtNombreArt As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''Control ibtnBuscarAxDesc.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents ibtnBuscarAxDesc As Global.System.Web.UI.WebControls.ImageButton

    '''<summary>
    '''Control divArticuloDetail.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents divArticuloDetail As Global.System.Web.UI.HtmlControls.HtmlGenericControl

    '''<summary>
    '''Control lblPacking.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents lblPacking As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Control txtObservacion.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents txtObservacion As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''Control hdnConsolidadoArticulo.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents hdnConsolidadoArticulo As Global.System.Web.UI.WebControls.HiddenField

    '''<summary>
    '''Control hdnConsolidados.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents hdnConsolidados As Global.System.Web.UI.WebControls.HiddenField

    '''<summary>
    '''Control grvConsolidado.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents grvConsolidado As Global.System.Web.UI.WebControls.GridView

    '''<summary>
    '''Control grvConsolidadoAUbicacion.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents grvConsolidadoAUbicacion As Global.System.Web.UI.WebControls.GridView

    '''<summary>
    '''Control hfCurrentRowConsolidado.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents hfCurrentRowConsolidado As Global.System.Web.UI.WebControls.HiddenField

    '''<summary>
    '''Control hfCurrentRowConsArticulo.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents hfCurrentRowConsArticulo As Global.System.Web.UI.WebControls.HiddenField

    '''<summary>
    '''Control lblAlmacen.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents lblAlmacen As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Control lblCajas.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents lblCajas As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Control lblDocenas.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents lblDocenas As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Control lblUnidad.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents lblUnidad As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Control lblTotal.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents lblTotal As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Control collapseDetallePre.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents collapseDetallePre As Global.System.Web.UI.HtmlControls.HtmlGenericControl

    '''<summary>
    '''Control grvDetPreVenta.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents grvDetPreVenta As Global.System.Web.UI.WebControls.GridView

    '''<summary>
    '''Control lblTotCaja.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents lblTotCaja As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Control lblTotDoc.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents lblTotDoc As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Control lblTotUnd.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents lblTotUnd As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Control lblImporteTotal.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents lblImporteTotal As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Control txtComentario.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents txtComentario As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''Control btnGrabar.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents btnGrabar As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''Control btnImprimir.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents btnImprimir As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''Control btnCancelar.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents btnCancelar As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''Control MostrarAccordion.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents MostrarAccordion As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Control btnDefaultLoad.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents btnDefaultLoad As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''Control MPopLoad.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents MPopLoad As Global.AjaxControlToolkit.ModalPopupExtender

    '''<summary>
    '''Control pnlLoadPreVenta.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents pnlLoadPreVenta As Global.System.Web.UI.WebControls.Panel

    '''<summary>
    '''Control txtLoadCodigo.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents txtLoadCodigo As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''Control ddlLoadClase.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents ddlLoadClase As Global.System.Web.UI.WebControls.DropDownList

    '''<summary>
    '''Control btnLoadBuscar.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents btnLoadBuscar As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''Control btnLoadNueva.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents btnLoadNueva As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''Control btnHandlerPop3.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents btnHandlerPop3 As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''Control MPopRegistrarCliente.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents MPopRegistrarCliente As Global.AjaxControlToolkit.ModalPopupExtender

    '''<summary>
    '''Control pnlRegistrarCliente.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents pnlRegistrarCliente As Global.System.Web.UI.WebControls.Panel

    '''<summary>
    '''Control upnlRCliente.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents upnlRCliente As Global.System.Web.UI.UpdatePanel

    '''<summary>
    '''Control rbPNatural.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents rbPNatural As Global.System.Web.UI.WebControls.RadioButton

    '''<summary>
    '''Control rbPJuridica.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents rbPJuridica As Global.System.Web.UI.WebControls.RadioButton

    '''<summary>
    '''Control pRPDocumento.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents pRPDocumento As Global.System.Web.UI.HtmlControls.HtmlGenericControl

    '''<summary>
    '''Control txtRPDocumento.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents txtRPDocumento As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''Control pRPContacto.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents pRPContacto As Global.System.Web.UI.HtmlControls.HtmlGenericControl

    '''<summary>
    '''Control txtRPContacto.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents txtRPContacto As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''Control divRPJuridica.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents divRPJuridica As Global.System.Web.UI.HtmlControls.HtmlGenericControl

    '''<summary>
    '''Control txtRPFono.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents txtRPFono As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''Control txtRPRazon.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents txtRPRazon As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''Control txtRPDireccion.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents txtRPDireccion As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''Control ddlDepartamento.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents ddlDepartamento As Global.System.Web.UI.WebControls.DropDownList

    '''<summary>
    '''Control ddlProvincia.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents ddlProvincia As Global.System.Web.UI.WebControls.DropDownList

    '''<summary>
    '''Control ddlDistrito.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents ddlDistrito As Global.System.Web.UI.WebControls.DropDownList

    '''<summary>
    '''Control txtRPCorreo.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents txtRPCorreo As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''Control btnGuardarRCliente.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents btnGuardarRCliente As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''Control btnCerrarRCliente.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents btnCerrarRCliente As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''Control btnDefaultPop4.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents btnDefaultPop4 As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''Control MPopupAddCombo.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents MPopupAddCombo As Global.AjaxControlToolkit.ModalPopupExtender

    '''<summary>
    '''Control pnlPopAddCombo.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents pnlPopAddCombo As Global.System.Web.UI.WebControls.Panel

    '''<summary>
    '''Control lblTitlePopAddCombo.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents lblTitlePopAddCombo As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Control grvCombos.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents grvCombos As Global.System.Web.UI.WebControls.GridView

    '''<summary>
    '''Control txtComboCant.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents txtComboCant As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''Control btnPopAddCombo.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents btnPopAddCombo As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''Control btnPopCancelCombo.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents btnPopCancelCombo As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''Control btnDefaultPopIn.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents btnDefaultPopIn As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''Control MPopupArticuloCant.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents MPopupArticuloCant As Global.AjaxControlToolkit.ModalPopupExtender

    '''<summary>
    '''Control pnlPopArticuloCant.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents pnlPopArticuloCant As Global.System.Web.UI.WebControls.Panel

    '''<summary>
    '''Control txtPopACajas.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents txtPopACajas As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''Control txtPopADocenas.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents txtPopADocenas As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''Control txtPopAPiezas.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents txtPopAPiezas As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''Control btnPopAAgregar.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents btnPopAAgregar As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''Control btnPopACancelar.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents btnPopACancelar As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''Control uprgRegistroPre.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents uprgRegistroPre As Global.System.Web.UI.UpdateProgress
End Class
