﻿Imports WebUtils
Imports BusinessLogic
Imports System.Web.Script.Services
Imports System.Web.Services
Imports Entidades
Imports System.Drawing
Imports System.Drawing.Printing
Imports System.Collections
Imports iTextSharp.text
Imports iTextSharp.text.pdf
Imports System.IO

Public Class RegistroPreVenta
    Inherits System.Web.UI.Page

    Dim objEntTXSTComp As New ETXSTComp
    Dim objBlTXSTComp As New BlTXSTComp
    Dim resultado As String = ""

    'Para el listado de la grilla'
    Dim listadoGrid As New List(Of Entidades.EDetallePreVenta)
    Dim listaImpresion As New List(Of Entidades.EDetallePreVenta)

    'Para el formato de Impresión'
    Dim NombreAlmacen As String = ""
    Dim NombreUbicacion As String = ""
    Dim rutaImpresora As String = ""

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Page.IsPostBack Then
            MPopLoad.Show()
            Try
                cargarTipoMoneda()
                CargarTipoDocumento()
                CargarLineaCredito()
                CargarDepartamento()
                cargarAlmacen()
                asignarValoresPorDefault()
            Catch ex As Exception
                MyMessageBox.Alert(ex.Message)
            End Try
        End If
    End Sub

    Protected Sub asignarValoresPorDefault()
        'Para valor Moneda'
        ddlMoneda.SelectedValue = 1
    End Sub

    Protected Sub btnLoadBuscar_Click(sender As Object, e As EventArgs)
        Try
            Dim CodigoPre As String = txtLoadCodigo.Text.Trim()

            If CodigoPre.Equals("") Then
                MyMessageBox.Alert("Ingrese el Nº de Pre-Venta")
                MPopLoad.Show()
            Else
                Dim objBlPreVenta As New BLPreVenta
                Dim oPreVenta As New EPreVenta
                Dim dtDetalleArticulos As New DataTable
                oPreVenta = objBlPreVenta.ObtenerPreVentaPorNumPV(CodigoPre)

                If oPreVenta._pk_cab_pre_vta = Nothing Then
                    MyMessageBox.Alert("El Nº de Pre-Venta está en otro estado o no Existe")
                    MPopLoad.Show()
                Else
                    cargarTipoCambio(False, oPreVenta._fk_tipo_camb)
                    hfCodigoPreVenta.Value = oPreVenta._pk_cab_pre_vta
                    hfCorrelativoPre.Value = oPreVenta._num_pre_vta
                    hfEstadoPreVenta.Value = oPreVenta._est_doc
                    txtCodigoPre.Text = oPreVenta._num_pre_vta
                    ddlClase.Text = oPreVenta._num_pre_vta.Substring(0, 1)
                    ddlMoneda.SelectedValue = oPreVenta._fk_moned

                    If oPreVenta._fk_cond_pago = 260000000 Then
                        chkContado.Checked = True
                    Else
                        chkCredito.Checked = True
                    End If

                    ddlCredito.SelectedValue = oPreVenta._fk_linea_cred
                    ddlTipoDocumento.SelectedValue = oPreVenta._fk_tip_doc_vta
                    txtComentario.Text = oPreVenta._obs

                    txtDocCliente.Text = oPreVenta._ruc_dni
                    hfCodigoCliente.Value = oPreVenta._pk_cliente
                    txtRazNomCliente.Text = oPreVenta._nom_cliente
                    txtDirCliente.Text = oPreVenta._direc_entreg
                    lblImporteTotal.Text = oPreVenta._imp_tot_igv.ToString("n2")
                    ddlAlmacen.SelectedValue = oPreVenta._fk_alm_desp

                    '----------------------Traemos el detalle-------------------'
                    dtDetalleArticulos = objBlPreVenta.ObtenerDetallePV(hfCodigoPreVenta.Value)

                    '----------------------Mostramos el check descuento del detalle -------------------'
                    If oPreVenta._st_dscto = True Then
                        'TryCast(grvDetPreVenta.HeaderRow.FindControl("chkHeader"), CheckBox).Checked = True
                    End If

                    grvDetPreVenta.DataSource = dtDetalleArticulos
                    grvDetPreVenta.DataBind()

                    '---------------------Totalizamos el detalle-----------------'
                    contTotalProductos()
                    btnImprimir.Visible = True
                    MPopLoad.Hide()
                End If
            End If
        Catch ex As Exception
            MyMessageBox.Alert(ex.Message)
        End Try
    End Sub


    Protected Sub btnLoadNueva_Click(sender As Object, e As EventArgs)
        If ddlLoadClase.SelectedValue.Equals("null") Then
            ddlLoadClase.Focus()
            MyMessageBox.Alert("Seleccione la clase por favor")
            MPopLoad.Show()
        Else
            ddlClase.SelectedValue = ddlLoadClase.SelectedValue
            ddlLoadClase.SelectedValue = "null"
            cargarTipoCambio(True, 0)
            Dim codEmpleado As Integer = (CType(Page.Master.FindControl("hfEmpleadoID"), HiddenField)).Value
            Dim result As Integer = New BLPreVenta().EliminarHelperDetPreVenta(codEmpleado, 0)
            MPopLoad.Hide()
        End If
    End Sub

    Protected Sub chkCredito_CheckedChanged(sender As Object, e As EventArgs)
        If chkCredito.Checked Then
            ddlCredito.Enabled = True
        End If
        MostrarAcordeon("P")
    End Sub

    Protected Sub chkContado_CheckedChanged(sender As Object, e As EventArgs)
        If chkContado.Checked Then
            ddlCredito.SelectedValue = "0"
            ddlCredito.Enabled = False
        End If
        MostrarAcordeon("P")
    End Sub

    Protected Sub btnBuscarCliente_Click(sender As Object, e As ImageClickEventArgs)
        Try
            Dim documento As String = txtDocCliente.Text.Trim().Split("-")(0)
            Dim oCliente As New EClientes()

            If documento.Length < 8 Or documento.Length > 12 Then
                MyMessageBox.Alert("Número de documento inválido")
            ElseIf documento.Length > 7 And documento.Length < 10 Then
                oCliente = New BLCliente().obtenerCliente_PFV_DNI(documento)
            ElseIf documento.Length > 10 And documento.Length < 13 Then
                oCliente = New BLCliente().obtenerCliente_PFV_RUC(documento)
            End If

            txtDocCliente.Text = ""
            txtDirCliente.Text = ""
            txtRazNomCliente.Text = ""
            hfCodigoCliente.Value = "null"

            If oCliente Is Nothing Then
                MyMessageBox.Alert("El cliente no existe")
            ElseIf oCliente._fk_estado <> 150 Then
                MyMessageBox.Alert("El cliente no está activo para la operación")
            Else
                txtDocCliente.Text = documento
                txtDirCliente.Text = oCliente._txt_direcc
                txtRazNomCliente.Text = oCliente._nombre_comercial
                hfCodigoCliente.Value = oCliente._pk_cliente
            End If
        Catch ex As Exception
            MyMessageBox.Alert(ex.Message)
        End Try
        MostrarAcordeon("C")
    End Sub

    Protected Sub btnNuevoCliente_Click(sender As Object, e As EventArgs) Handles btnNuevoCliente.Click
        MostrarAcordeon("C")
        MPopRegistrarCliente.Show()
        limpiarCliente()
    End Sub

    Protected Sub btnGuardarRCliente_Click(sender As Object, e As EventArgs) Handles btnGuardarRCliente.Click
        MostrarAcordeon("C")
        Try
            Dim objBlCliente As New BusinessLogic.BLCliente
            Dim objEntCliente As New EClientes
            Dim documento As String = txtRPDocumento.Text.Trim()
            Dim tipoCliente As String
            MPopRegistrarCliente.Show()

            If documento.Equals("") Then
                MyMessageBox.Alert("Ingrese número de documento")
            ElseIf txtRPContacto.Text.Trim().Equals("") Then
                MyMessageBox.Alert("Ingrese el contacto")
            ElseIf txtRPDireccion.Text.Trim().Equals("") Then
                MyMessageBox.Alert("Ingrese la dirección")
            Else
                If rbPJuridica.Checked = True Then
                    If Len(txtRPDocumento.Text) < 11 Then
                        MyMessageBox.Alert("Ingrese un N° de Ruc correcto")
                        Exit Sub
                    End If
                End If

                If rbPNatural.Checked = True Then
                    If Len(txtRPDocumento.Text) < 8 Then
                        MyMessageBox.Alert("Ingrese un N° de DNI correcto")
                        Exit Sub
                    End If
                End If

                If rbPNatural.Checked Then
                    tipoCliente = "N"
                    objEntCliente._dni = Trim(txtRPDocumento.Text)
                Else
                    tipoCliente = "J"
                    objEntCliente._nif = Trim(txtRPDocumento.Text)
                End If
                If objBlCliente.existeClientePorDoc(tipoCliente, documento) Then
                    MyMessageBox.Alert("Ya existe el cliente")
                    Exit Sub
                End If
                objEntCliente._fk_corp = 220000000
                objEntCliente._nombre_corp = "WEB"
                objEntCliente._razon_social = UCase(Trim(txtRPRazon.Text))
                objEntCliente._nombre_comercial = UCase(Trim(txtRPContacto.Text))
                objEntCliente._nombre_contacto = UCase(Trim(txtRPContacto.Text))
                objEntCliente._tratamiento = "SR(A)."
                objEntCliente._fk_departamento = CInt(ddlDepartamento.SelectedValue)
                objEntCliente._dpto = Trim(ddlDepartamento.SelectedItem.Text)
                objEntCliente._fk_distrito = CInt(ddlDistrito.SelectedValue)
                objEntCliente._distrito = Trim(ddlDistrito.SelectedItem.Text)
                objEntCliente._fk_prov = CInt(ddlProvincia.SelectedValue)
                objEntCliente._txt_prov = Trim(ddlProvincia.SelectedItem.Text)
                objEntCliente._email = UCase(Trim(txtRPCorreo.Text))
                objEntCliente._telef1 = Trim(txtRPFono.Text)
                objEntCliente._tipo_cliente = tipoCliente
                objEntCliente._linea_credito = 0
                objEntCliente._fk_linea = 0 'CInt(ddlCredito.SelectedValue)
                objEntCliente._nombre_linea = "" 'ddlCredito.Text
                objEntCliente._fk_estado = 150
                objEntCliente._descripcion_estado = "ACTIVO"
                objEntCliente._flag_interno = 0
                objEntCliente._CTA_AGRP = "0"

                objEntCliente._direcciones.Add(New EDirecEmpresas() With _
                {._TXT_DIRECC = txtRPDireccion.Text + " - " + objEntCliente._distrito + " - " + objEntCliente._txt_prov + " - " + objEntCliente._dpto, ._ESTADO = "1" _
                , ._TIPOD_DIRECC = "DIRECCION PRINCIPAL"})
                If objBlCliente.insertarCliente(objEntCliente) = "OK" Then
                    MyMessageBox.Alert("Se ha registrado un nuevo Cliente")
                    MPopRegistrarCliente.Hide()
                    limpiarCliente()
                Else
                    MyMessageBox.Alert("No se registró el Cliente")
                End If
            End If
        Catch ex As Exception
            MyMessageBox.Alert(ex.Message)
        End Try
    End Sub

    Protected Sub btnCerrarRCliente_Click(sender As Object, e As EventArgs)
        MostrarAcordeon("C")
        limpiarNuevoCliente()
    End Sub

    Protected Sub btnBuscarArticulo_Click(sender As Object, e As ImageClickEventArgs)
        MostrarAcordeon("A")
    End Sub

    Protected Sub ibtnBuscarAxCod_Click(sender As Object, e As ImageClickEventArgs)
        MostrarAcordeon("A")
        Dim codigo As String = txtCodigoArt.Text.Trim().ToUpper()
        Try
            If hfEstadoPreVenta.Value = 2 Then
                MyMessageBox.Alert("Ya no se puede editar la Pre-Venta porque ya se encuentra en el proceso de DESPACHO")
                Exit Sub
            End If

            If codigo.Length < 1 Then
                MyMessageBox.Alert("Ingrese el Código de artículo")
            Else
                If Not chkIsCombo.Checked Then
                    Dim dtEmpresas As New DataTable
                    dtEmpresas = New BLArticulo().getFullArticulosxCodigoImp(codigo)
                    createConsolidado(dtEmpresas)
                Else
                    Dim dtArticulos As New DataTable
                    dtArticulos = New BlComboArticulos().getFullArticulosxCodigoCombo(codigo)
                    grvCombos.DataSource = dtArticulos
                    grvCombos.DataBind()
                    txtNombreArt.Text = dtArticulos.Rows.Item(0).Item("NOMBRE_AGRUPACION")
                    lblTitlePopAddCombo.Text = dtArticulos.Rows.Item(0).Item("NOMBRE_AGRUPACION")
                    MPopupAddCombo.Show()
                End If
            End If
        Catch ex As Exception
            MyMessageBox.Alert(ex.Message)
        End Try
    End Sub

    Protected Sub ibtnBuscarAxDesc_Click(sender As Object, e As ImageClickEventArgs) Handles ibtnBuscarAxDesc.Click
        MostrarAcordeon("A")
        Dim nombre As String = txtNombreArt.Text.Trim().ToUpper()
        Try

            If hfEstadoPreVenta.Value = 2 Then
                MyMessageBox.Alert("Ya no se puede editar la Pre-Venta porque ya se encuentra en el proceso de DESPACHO")
                Exit Sub
            End If

            If nombre.Length < 1 Then
                MyMessageBox.Alert("Ingrese el Código de artículo")
            Else
                If Not chkIsCombo.Checked Then
                    Dim dtEmpresas As New DataTable
                    dtEmpresas = New BLArticulo().getFullArticulosxNombre(nombre)
                    createConsolidado(dtEmpresas)
                Else
                    Dim dtArticulos As New DataTable
                    dtArticulos = New BlComboArticulos().getFullArticulosxNombreCombo(nombre)
                    grvCombos.DataSource = dtArticulos
                    grvCombos.DataBind()
                    txtCodigoArt.Text = dtArticulos.Rows.Item(0).Item("COD_CBO_VTA")
                    lblTitlePopAddCombo.Text = dtArticulos.Rows.Item(0).Item("NOMBRE_AGRUPACION")
                    MPopupAddCombo.Show()
                End If
            End If
        Catch ex As Exception
            MyMessageBox.Alert(ex.Message)
        End Try
    End Sub

    Protected Sub createConsolidado(ByVal dtEmpresas As DataTable)
        If dtEmpresas.Rows.Count = 1 Then
            Dim hcConsolidado As New HCConsolidado
            hcConsolidado.NombreEmpresa = dtEmpresas.Rows.Item(0).Item("NOM_EMPRESA")
            hcConsolidado.CodigoArticulo = dtEmpresas.Rows.Item(0).Item("COD_ARTICULO")
            hcConsolidado.NombreArticulo = dtEmpresas.Rows.Item(0).Item("NOM_ARTICULO")
            hcConsolidado.IDMaestroArticulo = dtEmpresas.Rows.Item(0).Item("COD_MAESTRO")
            'caja docena unidad
            hcConsolidado.IDEmpresa = dtEmpresas.Rows.Item(0).Item("COD_EMPRESA")
            hcConsolidado.IDEmpresaArticulo = dtEmpresas.Rows.Item(0).Item("COD_EMPRESA_ART")
            hcConsolidado.CodigoUMedidaPack = dtEmpresas.Rows.Item(0).Item("COD_UMD_PACK")
            hcConsolidado.CantUPacking = dtEmpresas.Rows.Item(0).Item("CAN_PACKING")
            hcConsolidado.CantMayor = dtEmpresas.Rows.Item(0).Item("CAN_MAYOR")
            hcConsolidado.NombreUMedida = dtEmpresas.Rows.Item(0).Item("NOMBRE_UMEDIDA")
            hcConsolidado.PrecioMax = dtEmpresas.Rows.Item(0).Item("PRECIO_MAX")
            hcConsolidado.PrecioMin = dtEmpresas.Rows.Item(0).Item("PRECIO_MINIMO")
            hcConsolidado.PrecioProm = dtEmpresas.Rows.Item(0).Item("PRECIO_PROM")
            hcConsolidado.PrecioTienda = dtEmpresas.Rows.Item(0).Item("PRECIO_TIENDA")
            hcConsolidado.PrecioCosto = dtEmpresas.Rows.Item(0).Item("PRECIO_COSTO")
            hcConsolidado.TxtPacking = dtEmpresas.Rows.Item(0).Item("TXT_PACKING")
            hcConsolidado.PrecioCostoIgvUtil = dtEmpresas.Rows.Item(0).Item("PRECIO_COSTO_IGV_UTIL")
            txtCodigoArt.Text = hcConsolidado.CodigoArticulo
            txtNombreArt.Text = hcConsolidado.NombreArticulo
            lblPacking.Text = hcConsolidado.TxtPacking
            getConsolidado(hcConsolidado)
        ElseIf dtEmpresas.Rows.Count > 1 Then
            hdnConsolidados.Value = "show"
            hdnConsolidadoArticulo.Value = "show"
            grvConsolidado.DataSource = dtEmpresas
            grvConsolidado.DataBind()
            txtCodigoArt.Text = grvConsolidado.Rows(0).Cells(1).Text
            txtNombreArt.Text = grvConsolidado.Rows(0).Cells(2).Text
            lblPacking.Text = grvConsolidado.Rows(0).Cells(18).Text
            LimpiarConsolidadoAlmacen()
        End If
    End Sub

    Protected Sub rbPJuridica_CheckedChanged(sender As Object, e As EventArgs) Handles rbPJuridica.CheckedChanged
        If rbPJuridica.Checked Then
            pRPDocumento.InnerHtml = "N° Ruc:"
            divRPJuridica.Attributes.Remove("hidden")
            txtRPDocumento.Text = ""
            MPopRegistrarCliente.Show()
        End If
    End Sub

    Protected Sub rbPNatural_CheckedChanged(sender As Object, e As EventArgs) Handles rbPNatural.CheckedChanged
        If rbPNatural.Checked Then
            pRPDocumento.InnerHtml = "N° Dni:"
            divRPJuridica.Attributes.Add("hidden", "hidden")
            txtRPDocumento.Text = ""
            MPopRegistrarCliente.Show()
        End If
    End Sub

#Region "****************** AGREGAR PRODUCTOS *******************"
    Protected Sub btnPopAAgregar_Click(sender As Object, e As EventArgs)

        Dim UndTotalPedidas As Integer = 0
        Dim cajas As Integer = 0
        Dim docenas As Integer = 0
        Dim piezas As Integer = 0

        Integer.TryParse(txtPopACajas.Text.Trim(), cajas)
        Integer.TryParse(txtPopADocenas.Text.Trim(), docenas)
        Integer.TryParse(txtPopAPiezas.Text.Trim(), piezas)

        If (cajas = 0 And docenas = 0 And piezas = 0) Then
            txtPopACajas.Focus()
            MostrarConsolidados()
            MyMessageBox.Alert("Debe ingresar una cantidad.")
        Else
            Dim row As GridViewRow = grvConsolidadoAUbicacion.Rows(CInt(hfCurrentRowConsArticulo.Value))
            Dim cantPacking As Integer = row.Cells(14).Text 'cant_packing
            Dim nombreUnidad As String = row.Cells(17).Text.ToString 'caa_nombre_und_packing

            UndTotalPedidas = convertirCajasDocenaUnidadAUnidades(nombreUnidad, cantPacking, cajas, docenas, piezas)

            If UndTotalPedidas > CInt(lblTotal.Text) Then
                MostrarConsolidados()
                MyMessageBox.Alert("No hay stock disponible")
                Exit Sub
            Else
                AgregarVenta(UndTotalPedidas, cajas, docenas, piezas)
            End If
        End If
    End Sub

    Protected Sub btnPopACancelar_Click(sender As Object, e As EventArgs)
        MostrarConsolidados()
        MPopupArticuloCant.Hide()
    End Sub

    Public Function convertirCajasDocenaUnidadAUnidades(ByVal pTextoUnidadPacking As String, _
                                                        ByVal pCantidadUnidadPacking As Integer, _
                                                        ByVal cajas As Integer, ByVal docenas As Integer, _
                                                        ByVal unidades As Integer) As Integer
        Dim unidadPacking As Integer = FuncionesGlobales.textoUnidadesPacking.Item(pTextoUnidadPacking) * _
                                        pCantidadUnidadPacking
        Dim resultado As Integer
        resultado = (cajas * unidadPacking) + (docenas * 12) + unidades
        Return resultado
    End Function

    Protected Sub AgregarVenta(ByVal UndTotalPedidas As Integer, ByVal cajas As Integer, ByVal docenas As Integer, ByVal piezas As Integer)
        Dim stockDetalladoArticuloPorEmpresaAlamcenUbicacion As Integer = 0

        Dim row As GridViewRow = grvConsolidadoAUbicacion.Rows(CInt(hfCurrentRowConsArticulo.Value))
        Dim oPreVenta As EDetallePreVenta = Nothing
        If row.Cells(8).Text <> 170000001 And row.Cells(8).Text <> 170000002 Then
            oPreVenta = New EDetallePreVenta()

            oPreVenta._NOMBRE_EMPRESA = row.Cells(0).Text
            oPreVenta._COD_ART_IMPORT = row.Cells(1).Text
            oPreVenta._NOMBRE_VENTA = row.Cells(2).Text
            oPreVenta._NOMBRE_ALMACEN = row.Cells(3).Text
            oPreVenta._NOMBRE_UBICACION = row.Cells(4).Text
            oPreVenta._FK_ALM = row.Cells(8).Text
            oPreVenta._FK_UBIC = row.Cells(9).Text
            oPreVenta._FK_MAESTRO = row.Cells(10).Text
            oPreVenta._FK_EMPR = row.Cells(11).Text 'con
            oPreVenta._FK_EMP_ART = row.Cells(12).Text 'con
            oPreVenta._CODIGO_UNIDAD = row.Cells(13).Text 'con
            oPreVenta._CANT_PACKING = row.Cells(14).Text 'cant_packing
            oPreVenta._CANT_MAYOR = row.Cells(15).Text
            oPreVenta._IP_IMPRESORA = row.Cells(16).Text
            oPreVenta._NOMBRE_UNIDAD = row.Cells(17).Text 'nombre_und_packing
            oPreVenta._PRECIO_MAX = row.Cells(18).Text
            oPreVenta._PRECIO_MIN = row.Cells(19).Text
            oPreVenta._PRECIO_PROM = row.Cells(20).Text
            oPreVenta._PRECIO_COSTO = row.Cells(21).Text
            oPreVenta._PRECIO_TIENDA = row.Cells(22).Text
            oPreVenta._TXT_PACKING = row.Cells(24).Text
            oPreVenta._P_COSTO_IGV_K = row.Cells(25).Text
            oPreVenta._CAJAS_PEDIDAS = cajas
            oPreVenta._DOCENAS_PEDIDAS = docenas
            oPreVenta._UNIDADES_PEDIDAS = piezas
            oPreVenta._UNIDADES_TOTAL_PEDIDAS = UndTotalPedidas
            oPreVenta._COMENTARIO = IIf(txtObservacion.Text.Trim = "", "**", txtObservacion.Text.Trim.ToUpper)

            stockDetalladoArticuloPorEmpresaAlamcenUbicacion = FuncionesGlobales.obtenerStockArticulosPorEmpresaAlmacenUbicacionDelKardex _
                (oPreVenta._FK_EMPR, oPreVenta._FK_MAESTRO, oPreVenta._FK_ALM, oPreVenta._FK_UBIC)

            AgregarAPreVenta(oPreVenta)
        Else
            MostrarConsolidados()
            MyMessageBox.Alert("No se puede agregar el artículo porque la reposición de la ubicación está desactivada")
        End If
    End Sub
#End Region

    Protected Sub grvConsolidado_SelectedIndexChanging(sender As Object, e As GridViewSelectEventArgs)
        Dim hcConsolidado As New HCConsolidado
        Dim row As GridViewRow = grvConsolidado.Rows(e.NewSelectedIndex)

        hcConsolidado.NombreEmpresa = row.Cells(0).Text
        hcConsolidado.CodigoArticulo = row.Cells(1).Text
        hcConsolidado.NombreArticulo = row.Cells(2).Text
        hcConsolidado.IDMaestroArticulo = row.Cells(6).Text
        'caja docena unidad
        hcConsolidado.IDEmpresa = row.Cells(7).Text
        hcConsolidado.IDEmpresaArticulo = row.Cells(8).Text
        hcConsolidado.CodigoUMedidaPack = row.Cells(9).Text
        hcConsolidado.CantUPacking = row.Cells(10).Text
        hcConsolidado.CantMayor = row.Cells(11).Text
        hcConsolidado.NombreUMedida = row.Cells(12).Text
        hcConsolidado.PrecioMax = row.Cells(13).Text
        hcConsolidado.PrecioMin = row.Cells(14).Text
        hcConsolidado.PrecioProm = row.Cells(15).Text
        hcConsolidado.PrecioTienda = row.Cells(16).Text
        hcConsolidado.PrecioCosto = row.Cells(17).Text
        hcConsolidado.TxtPacking = row.Cells(18).Text
        hcConsolidado.PrecioCostoIgvUtil = row.Cells(19).Text
        getConsolidado(hcConsolidado)
    End Sub

    Protected Sub getConsolidado(ByVal hcConsolidado As HCConsolidado)
        MostrarAcordeon("A")
        MostrarConsolidados()
        Try
            grvConsolidadoAUbicacion.DataSource = New BLArticulo().getConsolidadoArticulosxEmpresaAlmacenUbicacion(hcConsolidado)
            grvConsolidadoAUbicacion.DataBind()
            lblAlmacen.Text = ""
            lblTotal.Text = ""
            lblCajas.Text = ""
            lblDocenas.Text = ""
            lblUnidad.Text = ""
        Catch ex As Exception
            MyMessageBox.Alert(ex.Message)
        End Try
    End Sub

    Protected Sub MostrarConsolidados()
        hdnConsolidados.Value = "show"
        If grvConsolidado.Rows.Count > 0 Then
            hdnConsolidadoArticulo.Value = "show"
        End If
    End Sub

#Region "****************** CONSOLIDADO POR ALMACEN *******************"
    Protected Sub grvConsolidadoAUbicacion_SelectedIndexChanging(sender As Object, e As GridViewSelectEventArgs) Handles grvConsolidadoAUbicacion.SelectedIndexChanging
        MostrarAcordeon("A")
        MostrarConsolidados()
        hfCurrentRowConsArticulo.Value = e.NewSelectedIndex.ToString()
        obtenerConsolidadoPorAlmacen(e.NewSelectedIndex)
        LimpiarPanelAgregarProducto()
        MPopupArticuloCant.Show()
    End Sub

    Private Sub obtenerConsolidadoPorAlmacen(ByVal index As Integer)
        If grvConsolidadoAUbicacion.Rows.Count > 0 Then

            Dim currentRow As GridViewRow = grvConsolidadoAUbicacion.Rows(index)
            Dim cajaAlmacenConsolidado, docenaAlamacenConsolidado, unidadAlmacenconsolidad As Integer
            Dim idAlmacenConsolidado As Integer = CInt(currentRow.Cells(8).Text)
            Dim idUbicacionConsolidado As Integer = CInt(currentRow.Cells(9).Text)
            Dim textoUnidadAlmacenConsolidado As String = currentRow.Cells(17).Text 'caa_nombre_und_packing
            Dim cantidadUnidadPackingAlmacenConsolidado As Integer = currentRow.Cells(14).Text 'cant_packing
            Dim unidadesTotalesAlmacenConsolidado As Integer
            Dim distribucionAlmacenConsolidado As Dictionary(Of String, Integer)

            For Each row As GridViewRow In grvConsolidadoAUbicacion.Rows
                If CStr(row.Cells(8).Text) = idAlmacenConsolidado And CStr(row.Cells(9).Text) = idUbicacionConsolidado Then
                    cajaAlmacenConsolidado = cajaAlmacenConsolidado + CInt(row.Cells(5).Text)
                    docenaAlamacenConsolidado = docenaAlamacenConsolidado + CInt(row.Cells(6).Text)
                    unidadAlmacenconsolidad = unidadAlmacenconsolidad + CInt(row.Cells(7).Text)
                End If
            Next

            unidadesTotalesAlmacenConsolidado = convertirCajasDocenaUnidadAUnidades(textoUnidadAlmacenConsolidado, _
                                                                                    cantidadUnidadPackingAlmacenConsolidado, _
                                                                                    cajaAlmacenConsolidado, docenaAlamacenConsolidado, _
                                                                                    unidadAlmacenconsolidad)
            distribucionAlmacenConsolidado = FuncionesGlobales.distribuirStockPresentaciones(textoUnidadAlmacenConsolidado, _
                                                                                             cantidadUnidadPackingAlmacenConsolidado, _
                                                                                             unidadesTotalesAlmacenConsolidado)
            lblAlmacen.Text = currentRow.Cells(3).Text 'caa_nombre_almacen
            lblTotal.Text = unidadesTotalesAlmacenConsolidado
            lblCajas.Text = distribucionAlmacenConsolidado.Item("CAJA")
            lblDocenas.Text = distribucionAlmacenConsolidado.Item("DOCENA")
            lblUnidad.Text = distribucionAlmacenConsolidado.Item("UNIDAD")

        Else
            lblAlmacen.Text = "No se ha seleccionado ningún almacen"
            lblTotal.Text = ""
            lblCajas.Text = ""
            lblDocenas.Text = ""
            lblUnidad.Text = ""
        End If
    End Sub
#End Region

    Protected Sub ddlDepartamento_SelectedIndexChanged(sender As Object, e As EventArgs)
        Try
            CargarProvincia()
            MPopRegistrarCliente.Show()
        Catch ex As Exception
            MyMessageBox.Alert(ex.Message)
        End Try
    End Sub

    Protected Sub ddlProvincia_SelectedIndexChanged(sender As Object, e As EventArgs)
        Try
            CargarDistrito()
            MPopRegistrarCliente.Show()
        Catch ex As Exception
            MyMessageBox.Alert(ex.Message)
        End Try
    End Sub

#Region "**************** CARGAR DROPDOWNLISTS *********************"
    Protected Sub cargarTipoMoneda()
        ddlMoneda.DataSource = New BusinessLogic.BLMoneda().obtenerMoneda()
        ddlMoneda.DataValueField = "CODIGO"
        ddlMoneda.DataTextField = "DESCRIPCION"
        ddlMoneda.DataBind()
    End Sub

    Protected Sub cargarAlmacen()
        ddlAlmacen.DataSource = New BusinessLogic.BlAlmacen().obtenerAlmacen()
        ddlAlmacen.DataValueField = "CODIGO"
        ddlAlmacen.DataTextField = "NOMBRE"
        ddlAlmacen.DataBind()
    End Sub

    Protected Sub cargarTipoDocumento()
        ddlTipoDocumento.DataSource = New BusinessLogic.BLTipoDocumento().obtenerTipoDocumento_PFV()
        ddlTipoDocumento.DataValueField = "CODIGO"
        ddlTipoDocumento.DataTextField = "NOMBRE"
        ddlTipoDocumento.DataBind()
    End Sub

    Protected Sub cargarLineaCredito()
        ddlCredito.DataSource = New BusinessLogic.BLLineaCredito().obtenerLineaCredito()
        ddlCredito.DataValueField = "CODIGO"
        ddlCredito.DataTextField = "DESCRIPCION"
        ddlCredito.DataBind()
        upnlFormasPago.Update()
    End Sub

    Protected Sub cargarTipoCambio(ByVal isNew As Boolean, ByVal _llave_primaria As Integer)
        If isNew Then
            ddlTipoCambio.DataSource = New BusinessLogic.BLTipoCambio().obtenerTipoCambioPorEstado()
        Else
            ddlTipoCambio.DataSource = New BusinessLogic.BLTipoCambio().obtenerTipoCambioPorPrimaria(_llave_primaria)
        End If
        ddlTipoCambio.DataValueField = "CODIGO"
        ddlTipoCambio.DataTextField = "VALOR"
        ddlTipoCambio.DataBind()
    End Sub

    Protected Sub cargarDepartamento()
        ddlDepartamento.DataSource = New BusinessLogic.BLDepartamento().cargarComboDepartamento()
        ddlDepartamento.DataValueField = "CODIGO"
        ddlDepartamento.DataTextField = "DESCRIPCION"
        ddlDepartamento.DataBind()
    End Sub

    Protected Sub CargarProvincia()
        Dim departamentoCod As Integer
        departamentoCod = CInt(ddlDepartamento.SelectedValue)
        ddlProvincia.DataSource = New BusinessLogic.BLProvincia().obtenerProvinciaPorDepartamento(departamentoCod)
        ddlProvincia.DataValueField = "CODIGO"
        ddlProvincia.DataTextField = "DESCRIPCION"
        ddlProvincia.DataBind()

        Dim table As New DataTable
        table.Columns.Add("CODIGO", GetType(Integer))
        table.Columns.Add("DESCRIPCION", GetType(String))
        Dim row As DataRow = table.NewRow()
        row("CODIGO") = 0
        row("DESCRIPCION") = "[SELECCIONE]"
        table.Rows.InsertAt(row, 0)
        ddlDistrito.DataSource = table
        ddlDistrito.DataValueField = "CODIGO"
        ddlDistrito.DataTextField = "DESCRIPCION"
        ddlDistrito.DataBind()
        ddlDistrito.Enabled = False

        If ddlProvincia.Items.Count > 0 Then
            ddlProvincia.Enabled = True
        Else
            ddlProvincia.Enabled = False
        End If

    End Sub

    Protected Sub cargarDistrito()
        Dim departamentoCod As Integer
        departamentoCod = CInt(ddlDepartamento.SelectedValue)
        Dim provinciaCod As Integer
        provinciaCod = CInt(ddlProvincia.SelectedValue)
        ddlDistrito.DataSource = New BusinessLogic.BLDistrito().obtenerDistritoxProvincia(provinciaCod, departamentoCod)
        ddlDistrito.DataValueField = "CODIGO"
        ddlDistrito.DataTextField = "DESCRIPCION"
        ddlDistrito.DataBind()

        If ddlDistrito.Items.Count > 0 Then
            ddlDistrito.Enabled = True
        Else
            ddlDistrito.Enabled = False
        End If
    End Sub
#End Region

#Region "***********************  AGREGAR A PREVENTA ************************"
    Protected Sub AgregarAPreVenta(ByVal oPreVenta As EDetallePreVenta)

        '----  VERIFICANDO DUPLICIDAD DE PRODUCTOS -----*/
        For Each row As GridViewRow In grvDetPreVenta.Rows
            If row.Cells(1).Text = oPreVenta._COD_ART_IMPORT And row.Cells(21).Text = oPreVenta._FK_EMPR And row.Cells(23).Text = oPreVenta._FK_UBIC Then
                MostrarConsolidados()
                MyMessageBox.Alert("Ya ha ingresado el artículo del almacén y ubicación seleccionado, si desea modificarlo, quítelo de la lista y agrégelo nuevamente")
                Exit Sub
            End If
        Next

        Dim ItemOrder As Integer = grvDetPreVenta.Rows.Count + 1
        Dim oHelperPreVenta As New EHelperPreVenta
        Dim unidadesDistribuidas As Dictionary(Of String, Integer)

        unidadesDistribuidas = FuncionesGlobales.distribuirStockPresentaciones(Trim(oPreVenta._NOMBRE_UNIDAD),
                                                                               CInt(Trim(oPreVenta._CANT_PACKING)),
                                                                               oPreVenta._UNIDADES_TOTAL_PEDIDAS)
        Try

            oHelperPreVenta._ORDEN_ITEM = ItemOrder
            oHelperPreVenta._FK_EMPRESA = oPreVenta._FK_EMPR
            oHelperPreVenta._DESC_EMPRESA = oPreVenta._NOMBRE_EMPRESA
            oHelperPreVenta._FK_EMP_ART = oPreVenta._FK_EMP_ART
            oHelperPreVenta._FK_MAESTRO = oPreVenta._FK_MAESTRO
            oHelperPreVenta._COD_ART_IMPORT = oPreVenta._COD_ART_IMPORT
            oHelperPreVenta._NOMBRE_VENTA = oPreVenta._NOMBRE_VENTA
            oHelperPreVenta._DESC_PACKING = oPreVenta._TXT_PACKING
            oHelperPreVenta._CAN_PACKING = oPreVenta._CANT_PACKING
            oHelperPreVenta._CAN_MAYOR = oPreVenta._CANT_MAYOR
            oHelperPreVenta._FK_ALMACEN = oPreVenta._FK_ALM
            oHelperPreVenta._DESC_ALMACEN = oPreVenta._NOMBRE_ALMACEN
            oHelperPreVenta._FK_UBICACION = oPreVenta._FK_UBIC
            oHelperPreVenta._DESC_UBICACION = oPreVenta._NOMBRE_UBICACION
            oHelperPreVenta._UNIDADES_TOTAL_PEDIDAS = oPreVenta._UNIDADES_TOTAL_PEDIDAS
            oHelperPreVenta._CAJAS_PEDIDAS = unidadesDistribuidas.Item("CAJA")
            oHelperPreVenta._DOCENAS_PEDIDAS = unidadesDistribuidas.Item("DOCENA")
            oHelperPreVenta._UNIDADES_PEDIDAS = unidadesDistribuidas.Item("UNIDAD")
            oHelperPreVenta._NUEVO_REGISTRO = "1"
            oHelperPreVenta._ST_DSCTO = oPreVenta._ST_DSCTO
            oHelperPreVenta._COMENTARIO = oPreVenta._COMENTARIO
            oHelperPreVenta._IP_IMPRESORA = oPreVenta._IP_IMPRESORA
            oHelperPreVenta._ID_TX = (CType(Page.Master.FindControl("hfIdTXSTComp"), HiddenField)).Value
            oHelperPreVenta._DLL_CLASE = ddlClase.SelectedValue

            Dim listPrecios As New Dictionary(Of String, Decimal)

            listPrecios = New AsignacionPreciosDocMay().asignarPrecioPorArticulo(oHelperPreVenta,
                                                                                 FuncionesGlobales.getPropertyInSession().Item("igvActual"))

            oHelperPreVenta._PRECIO = listPrecios.Item("PrecioAsignado") 'pMin CAMBIA
            oHelperPreVenta._PRECIO_COSTO = listPrecios.Item("CostoFactSol") 'oPreVenta._PRECIO_COSTO CAMBIA
            oHelperPreVenta._PRECIO_TOTAL = listPrecios.Item("PrecTotal") 'pTotal 'ToString("n2"), CAMBIA
            oHelperPreVenta._PRECIO_COSTO_IGV_K = listPrecios.Item("CostoCotSol")
            oHelperPreVenta._PK_GRP_VENTA = 0
            oHelperPreVenta._NUM_PRE_VTA = txtCodigoPre.Text

            If hfCodigoPreVenta.Value.Equals("null") Then
                '24/12/2014 Se hace el insert para un Comprometido temporal de los datos'
                Dim codEmpleado As Integer = (CType(Page.Master.FindControl("hfEmpleadoID"), HiddenField)).Value
                Dim result As Integer = 0
                oHelperPreVenta._FK_EMPLEADO = codEmpleado
                result = New BLPreVenta().GrabarHelperDetPreVenta(oHelperPreVenta)
                If result = 1 Then
                    limpiarBusquedaArticulo()
                    grvDetPreVenta.DataSource = New BLPreVenta().ObtenerHelperDetPreVenta(codEmpleado)
                    grvDetPreVenta.DataBind()
                    MostrarAcordeon("D")
                    hdnConsolidadoArticulo.Value = "hidden"
                    hdnConsolidados.Value = "hidden"
                Else
                    MyMessageBox.Alert("No se grabó el artículo")
                End If
            Else
                Dim result As Integer = 0
                oHelperPreVenta._FK_PRE_VTA = CInt(hfCodigoPreVenta.Value)
                result = New BLPreVenta().InsertarDetallePreventa(oHelperPreVenta)
                If result = 1 Then
                    limpiarBusquedaArticulo()
                    grvDetPreVenta.DataSource = New BLPreVenta().ObtenerDetallePV(CInt(hfCodigoPreVenta.Value))
                    grvDetPreVenta.DataBind()
                    MostrarAcordeon("D")
                Else
                    MyMessageBox.Alert("No se grabó el artículo")
                End If
            End If

            objEntTXSTComp._NUM_PED = (CType(Page.Master.FindControl("hfIdTXSTComp"), HiddenField)).Value
            objEntTXSTComp._FK_MA_ART = oPreVenta._FK_MAESTRO
            objEntTXSTComp._FK_EMPR = oPreVenta._FK_EMPR
            objEntTXSTComp._FK_ALM = oPreVenta._FK_ALM
            objEntTXSTComp._FK_UBIC = oPreVenta._FK_UBIC
            objEntTXSTComp._UNID = oPreVenta._UNIDADES_TOTAL_PEDIDAS
            resultado = objBlTXSTComp.insertarStockComprometidoTemporal(objEntTXSTComp)
            Call contTotalProductos()
        Catch ex As Exception
            MyMessageBox.Alert(ex.Message)
        End Try
    End Sub

    Private Sub contTotalProductos()
        Dim TotCaja As Integer = 0
        Dim TotDoc As Integer = 0
        Dim TotUnd As Integer = 0
        Dim TotImporte As Decimal = 0

        If grvDetPreVenta.Rows.Count > 0 Then
            For Each row As GridViewRow In grvDetPreVenta.Rows
                TotCaja += row.Cells(7).Text
                TotDoc += row.Cells(8).Text
                TotUnd += row.Cells(9).Text
                TotImporte += row.Cells(15).Text
            Next
        End If
        'Se agregan los totales'
        lblTotCaja.Text = TotCaja
        lblTotDoc.Text = TotDoc
        lblTotUnd.Text = TotUnd
        lblImporteTotal.Text = TotImporte.ToString("n2")
    End Sub
#End Region

#Region "************** GUARDAR PREVENTA  ***************"
    Protected Sub btnGrabar_Click(sender As Object, e As EventArgs) Handles btnGrabar.Click
        Dim fechaDespacho, fechaActual As Date

        fechaDespacho = CDate(FuncionesGlobales.ServerDate).Date
        fechaActual = CDate(FuncionesGlobales.ServerDate).Date

        If hfEstadoPreVenta.Value = 2 Then
            MyMessageBox.Alert("Ya no se puede editar la Pre-Venta porque ya se encuentra en el proceso de DESPACHO")
            Exit Sub
        End If

        If grvDetPreVenta.Rows.Count <= 0 Then
            MyMessageBox.Alert("No se ha listado ningún artículo en el detalle")
            Exit Sub
        ElseIf hfCodigoCliente.Value = "null" Then
            txtDocCliente.Focus()
            MyMessageBox.Alert("Debe ingresar un cliente")
            Exit Sub
        ElseIf ddlTipoDocumento.SelectedValue = "0" Then
            ddlTipoDocumento.Focus()
            MyMessageBox.Alert("Debe seleccionar tipo de documento")
            Exit Sub
        ElseIf ddlMoneda.SelectedValue = "0" Then
            ddlMoneda.Focus()
            MyMessageBox.Alert("Debe seleccionar una moneda")
            Exit Sub
        ElseIf ddlAlmacen.SelectedValue = "0" Then
            ddlAlmacen.Focus()
            MyMessageBox.Alert("Debe seleccionar un almacén")
            Exit Sub
        ElseIf chkCredito.Checked = True And ddlCredito.SelectedValue = 0 Then
            ddlCredito.Focus()
            MyMessageBox.Alert("Debe seleccionar un tipo de crédito")
            upnlFormasPago.Update()
            Exit Sub
        ElseIf fechaDespacho < fechaActual Then
            MyMessageBox.Alert("La Fecha de despacho no puede ser menor a la fecha de Hoy.")
            Exit Sub
        Else
            If hfCodigoPreVenta.Value = "null" Then
                RegistrarPreVenta()
                '    Imprimir()
                '    'Se ejecuta métodos de limpieza de GUI....'
            Else
                ActualizarPreVenta()
            End If
        End If
        MostrarAcordeon("D")
    End Sub

    Protected Sub RegistrarPreVenta()
        Dim resultado As String
        Dim oEPreVenta As New EPreVenta
        Try
            oEPreVenta._fk_cliente = hfCodigoCliente.Value
            oEPreVenta._idTx = (CType(Page.Master.FindControl("hfIdTXSTComp"), HiddenField)).Value
            If chkContado.Checked = True Then
                oEPreVenta._fk_cond_pago = 260000000
                oEPreVenta._fk_linea_cred = 0
            Else
                oEPreVenta._fk_cond_pago = 260000001
                oEPreVenta._fk_linea_cred = ddlCredito.SelectedValue
            End If

            oEPreVenta._fk_emp_vend = (CType(Page.Master.FindControl("hfEmpleadoID"), HiddenField)).Value
            oEPreVenta._fk_alm_desp = ddlAlmacen.SelectedValue  ' punto despacho
            oEPreVenta._fk_moned = ddlMoneda.SelectedValue
            oEPreVenta._fk_tipo_camb = ddlTipoCambio.SelectedValue
            oEPreVenta._fk_tip_doc_vta = ddlTipoDocumento.SelectedValue

            If Len(txtDocCliente.Text) > 7 And Len(txtDocCliente.Text) < 10 Then
                oEPreVenta._tip_doc_cli = "DNI"
            ElseIf Len(txtDocCliente.Text) > 10 And Len(txtDocCliente.Text) < 13 Then
                oEPreVenta._tip_doc_cli = "RUC"
            End If

            oEPreVenta._ruc_dni = txtDocCliente.Text
            oEPreVenta._direc_entreg = txtDirCliente.Text
            oEPreVenta._fech_crea = Format(Date.Now, "yyyy/MM/dd")
            'oEPreVenta._fech_mod = Nothing
            oEPreVenta._fech_desp = CDate(FuncionesGlobales.ServerDate).Date
            oEPreVenta._usu_crea = (CType(Page.Master.FindControl("hfEmpleadoUser"), HiddenField)).Value
            'oEPreVenta._usu_mod = Nothing
            oEPreVenta._obs = txtComentario.Text
            'oEPreVenta._mot_rech_doc
            oEPreVenta._imp_tot_igv = lblImporteTotal.Text
            oEPreVenta._FECHA_TX_ST_COMP = CDate(FuncionesGlobales.ServerDate).Date

            Dim objLstEntDetallePreVta As New List(Of Entidades.EDetallePreVenta)
            Dim est_dscto As Int16
            Dim countCheck As Integer = 0

            For i As Integer = 0 To grvDetPreVenta.Rows.Count - 1
                Dim cb As CheckBox = TryCast(grvDetPreVenta.Rows(i).FindControl("chkDPVDescuento"), CheckBox)
                If cb IsNot Nothing AndAlso cb.Checked Then
                    est_dscto = 1
                    countCheck += 1
                Else
                    est_dscto = 0
                End If
                'mluyo observar _P_COSTO_IGV_K
                objLstEntDetallePreVta.Add(New EDetallePreVenta() With
                                           {._ITEM = grvDetPreVenta.Rows(i).Cells(0).Text,
                                            ._EST_POS = 1,
                                            ._FK_MAESTRO = grvDetPreVenta.Rows(i).Cells(19).Text,
                                            ._FK_EMP_ART = grvDetPreVenta.Rows(i).Cells(20).Text,
                                            ._FK_EMPR = grvDetPreVenta.Rows(i).Cells(21).Text,
                                            ._FK_ALM = grvDetPreVenta.Rows(i).Cells(22).Text,
                                            ._FK_UBIC = grvDetPreVenta.Rows(i).Cells(23).Text,
                                            ._UND_TOT = grvDetPreVenta.Rows(i).Cells(11).Text,
                                            ._CAJAS = grvDetPreVenta.Rows(i).Cells(7).Text,
                                            ._DOC = grvDetPreVenta.Rows(i).Cells(8).Text,
                                            ._UND_SET = grvDetPreVenta.Rows(i).Cells(9).Text,
                                            ._P_COSTO = grvDetPreVenta.Rows(i).Cells(27).Text,
                                            ._P_VTA = grvDetPreVenta.Rows(i).Cells(14).Text,
                                            ._P_VTA_MOD = grvDetPreVenta.Rows(i).Cells(14).Text,
                                            ._ST_DSCTO = est_dscto,
                                            ._P_TOTAL = grvDetPreVenta.Rows(i).Cells(15).Text,
                                            ._COMENTARIO = grvDetPreVenta.Rows(i).Cells(17).Text,
                                            ._P_COSTO_IGV_K = grvDetPreVenta.Rows(i).Cells(13).Text,
                                            ._PK_GRP_VENTA = grvDetPreVenta.Rows(i).Cells(28).Text
                                           })
            Next

            oEPreVenta._detallePreVenta = objLstEntDetallePreVta

            'verificando los checks descuento
            'If countCheck = grvDetPreVenta.Rows.Count Then
            '    oEPreVenta._st_dscto = 1
            'Else
            '    oEPreVenta._st_dscto = 0
            'End If

            If countCheck > 0 Then
                oEPreVenta._st_dscto = 1
            Else
                oEPreVenta._st_dscto = 0
            End If

            If ddlClase.SelectedValue = "C" And oEPreVenta._fk_cond_pago = 260000000 And oEPreVenta._st_dscto = 0 Then
                oEPreVenta._est_doc = 2
            Else
                oEPreVenta._est_doc = 1
            End If

            'Registrando la Preventa
            oEPreVenta._num_pre_vta = New BLPreVenta().ObtenerCorrelativoNumPreVenta(ddlClase.SelectedValue)
            resultado = New BLPreVenta().InsertarPreVenta(oEPreVenta)
            If resultado = "OK" Then
                txtCodigoPre.Text = oEPreVenta._num_pre_vta
                btnImprimir.Visible = True
                Dim noUse As Integer = New BLPreVenta().EliminarHelperDetPreVenta((CType(Page.Master.FindControl("hfEmpleadoID"), HiddenField)).Value, 0)
                MyMessageBox.Alert("Se ha generado correctamente la Pre-Venta N°: " + oEPreVenta._num_pre_vta)
                If ddlClase.SelectedValue = "M" And oEPreVenta._fk_cond_pago = 260000000 And oEPreVenta._st_dscto = False Then
                    'Call DistribuirImpresion()  'mluyo implementar impresion'
                    ImpresionFormatoPreVenta()
                End If
                'Response.Redirect("~/RegistroPreVenta.aspx")
                limpiarBusquedaArticulo()
                limpiarCliente()
                limpiarCabeceraPre()
                LimpiarDetallePre()
                MPopLoad.Show()
            Else
                MyMessageBox.Alert("No se generó la PreVenta" + resultado)
                Exit Sub
            End If
        Catch ex As Exception
            MyMessageBox.Alert(ex.Message)
        End Try
    End Sub
#End Region

#Region "************************ACTUALIZAR PRE VENTA*********************"
    Protected Sub ActualizarPreVenta()
        Dim oPreVenta As New EPreVenta
        Dim oBlPreVenta As New BLPreVenta
        Dim resultado As String

        Try
            oPreVenta._pk_cab_pre_vta = hfCodigoPreVenta.Value
            oPreVenta._num_pre_vta = txtLoadCodigo.Text
            oPreVenta._fk_cliente = hfCodigoCliente.Value

            If chkContado.Checked = True Then
                oPreVenta._fk_cond_pago = 260000000
                oPreVenta._fk_linea_cred = 0
            Else
                oPreVenta._fk_cond_pago = 260000001
                oPreVenta._fk_linea_cred = ddlCredito.SelectedValue
            End If

            oPreVenta._fk_emp_vend = (CType(Page.Master.FindControl("hfEmpleadoID"), HiddenField)).Value
            oPreVenta._fk_alm_desp = ddlAlmacen.SelectedValue
            oPreVenta._fk_moned = ddlMoneda.SelectedValue
            oPreVenta._fk_tipo_camb = ddlTipoCambio.SelectedValue
            oPreVenta._fk_tip_doc_vta = ddlTipoDocumento.SelectedValue

            If Len(txtDocCliente.Text) > 7 And Len(txtDocCliente.Text) < 10 Then
                oPreVenta._tip_doc_cli = "DNI"
            ElseIf Len(txtDocCliente.Text) > 10 And Len(txtDocCliente.Text) < 13 Then
                oPreVenta._tip_doc_cli = "RUC"
            End If

            oPreVenta._ruc_dni = txtDocCliente.Text
            oPreVenta._direc_entreg = txtDirCliente.Text
            oPreVenta._fech_mod = CDate(FuncionesGlobales.ServerDate).Date
            oPreVenta._fech_desp = CDate(FuncionesGlobales.ServerDate).Date
            oPreVenta._usu_mod = (CType(Page.Master.FindControl("hfEmpleadoUser"), HiddenField)).Value
            oPreVenta._obs = txtComentario.Text
            oPreVenta._est_doc = 1
            oPreVenta._imp_tot_igv = lblImporteTotal.Text

            Dim objLstEntDetallePreVta As New List(Of Entidades.EDetallePreVenta)
            Dim est_dscto As Int16
            Dim countchk As Integer = 0

            For i As Integer = 0 To grvDetPreVenta.Rows.Count - 1
                Dim cb As CheckBox = TryCast(grvDetPreVenta.Rows(i).FindControl("chkDPVDescuento"), CheckBox)
                If cb IsNot Nothing AndAlso cb.Checked Then
                    countchk += 1
                    est_dscto = 1
                Else
                    est_dscto = 0
                End If

                objLstEntDetallePreVta.Add(New EDetallePreVenta() With
                                           {._ITEM = grvDetPreVenta.Rows(i).Cells(0).Text,
                                            ._EST_POS = 1,
                                            ._FK_MAESTRO = grvDetPreVenta.Rows(i).Cells(19).Text,
                                            ._FK_EMP_ART = grvDetPreVenta.Rows(i).Cells(20).Text,
                                            ._FK_EMPR = grvDetPreVenta.Rows(i).Cells(21).Text,
                                            ._FK_ALM = grvDetPreVenta.Rows(i).Cells(22).Text,
                                            ._FK_UBIC = grvDetPreVenta.Rows(i).Cells(23).Text,
                                            ._UND_TOT = grvDetPreVenta.Rows(i).Cells(11).Text,
                                            ._CAJAS = grvDetPreVenta.Rows(i).Cells(7).Text,
                                            ._DOC = grvDetPreVenta.Rows(i).Cells(8).Text,
                                            ._UND_SET = grvDetPreVenta.Rows(i).Cells(9).Text,
                                            ._P_COSTO = grvDetPreVenta.Rows(i).Cells(13).Text,
                                            ._P_VTA = grvDetPreVenta.Rows(i).Cells(14).Text,
                                            ._P_VTA_MOD = grvDetPreVenta.Rows(i).Cells(14).Text,
                                            ._ST_DSCTO = est_dscto,
                                            ._P_TOTAL = grvDetPreVenta.Rows(i).Cells(15).Text,
                                            ._COMENTARIO = grvDetPreVenta.Rows(i).Cells(17).Text,
                                            ._PK_DET_PRE_VTA = grvDetPreVenta.Rows(i).Cells(25).Text,
                                            ._PK_GRP_VENTA = grvDetPreVenta.Rows(i).Cells(28).Text
                                           })
            Next

            oPreVenta._detallePreVenta = objLstEntDetallePreVta

            'Verificando los check
            If countchk = grvDetPreVenta.Rows.Count Then
                oPreVenta._st_dscto = 1
            Else
                oPreVenta._st_dscto = 0
            End If

            'Registrando la actualizacion
            resultado = oBlPreVenta.ActualizarPreventa(oPreVenta)

            If resultado = "OK" Then
                MyMessageBox.Alert("Se ha actualizado correctamente la Pre-Venta N°: " + hfCorrelativoPre.Value)
                limpiarBusquedaArticulo()
                limpiarCliente()
                limpiarCabeceraPre()
                LimpiarDetallePre()
                MPopLoad.Show()
            Else
                MyMessageBox.Alert("Ha ocurrido un error en la operación")
                Exit Sub
            End If
        Catch ex As Exception
            MyMessageBox.Alert(ex.Message)
        End Try
    End Sub
#End Region

#Region "************************ DISTRIBUIR PRE VENTA PARA IMPRIMIR****************************"
    Protected Sub DistribuirImpresion()
        'Try
        '    listadoGrid.Clear()
        '    listaImpresion.Clear()

        '    For i As Integer = 0 To grvDetPreVenta.Rows.Count - 1
        '        listadoGrid.Add(New Entidades.EDetallePreVenta With
        '                       {._FK_MAESTRO = grvDetPreVenta.Rows(i).Cells(19).Text,
        '                        ._COD_ART_IMPORT = grvDetPreVenta.Rows(i).Cells(1).Text,
        '                        ._CAJAS = grvDetPreVenta.Rows(i).Cells(7).Text,
        '                        ._DOC = grvDetPreVenta.Rows(i).Cells(8).Text,
        '                        ._UND_SET = grvDetPreVenta.Rows(i).Cells(9).Text,
        '                        ._FK_ALM = grvDetPreVenta.Rows(i).Cells(22).Text,
        '                        ._NOMBRE_ALMACEN = grvDetPreVenta.Rows(i).Cells(5).Text,
        '                        ._FK_UBIC = grvDetPreVenta.Rows(i).Cells(23).Text,
        '                        ._NOMBRE_UBICACION = grvDetPreVenta.Rows(i).Cells(6).Text,
        '                        ._COMENTARIO = grvDetPreVenta.Rows(i).Cells(17).Text,
        '                        ._PK_GRP_VENTA = grvDetPreVenta.Rows(i).Cells(28).Text,
        '                        ._ruta_impresora = grvDetPreVenta.Rows(i).Cells(24).Text})
        '    Next

        '    Dim q = From d In listadoGrid _
        '                Select d._FK_MAESTRO, d._COD_ART_IMPORT, d._CAJAS, d._DOC, d._UND_SET, d._FK_ALM, d._NOMBRE_ALMACEN, d._FK_UBIC, d._NOMBRE_UBICACION, d._COMENTARIO, d._ruta_impresora
        '                Order By _FK_UBIC Ascending

        '    Dim ubi = (From u In listadoGrid Select u._FK_UBIC, u._NOMBRE_UBICACION, u._FK_ALM, u._NOMBRE_ALMACEN, u._ruta_impresora Order By _FK_UBIC Ascending).Distinct

        '    Dim fk_ubicInicial As Integer
        '    Dim fk_ubicFinal As Integer

        '    Dim CodArt As String
        '    Dim cajas As Integer
        '    Dim doc As Integer
        '    Dim und As Integer
        '    Dim comentario As String

        '    For x As Integer = 0 To ubi.Count - 1
        '        fk_ubicInicial = ubi.ElementAt(x)._FK_UBIC
        '        NombreAlmacen = ubi.ElementAt(x)._NOMBRE_ALMACEN.ToString
        '        NombreUbicacion = ubi.ElementAt(x)._NOMBRE_UBICACION.ToString
        '        rutaImpresora = ubi.ElementAt(x)._ruta_impresora
        '        For y As Integer = 0 To q.Count - 1
        '            fk_ubicFinal = q.ElementAt(y)._FK_UBIC.ToString
        '            CodArt = q.ElementAt(y)._COD_ART_IMPORT.ToString
        '            cajas = q.ElementAt(y)._CAJAS.ToString
        '            doc = q.ElementAt(y)._DOC.ToString
        '            und = q.ElementAt(y)._UND_SET.ToString
        '            comentario = q.ElementAt(y)._COMENTARIO.ToString
        '            If fk_ubicInicial = fk_ubicFinal Then
        '                listaImpresion.Add(New Entidades.EDetallePreVenta With {
        '                                    ._COD_ART_IMPORT = CodArt,
        '                                    ._CAJAS = cajas,
        '                                    ._DOC = doc,
        '                                    ._UND_SET = und,
        '                                    ._COMENTARIO = comentario})
        '            End If
        '        Next
        '        imprimirClaseC_SinDscto()
        '    Next
        'Catch ex As Exception
        '    Console.WriteLine(ex.Message)
        'End Try

    End Sub
#End Region

#Region "*******************IMPRESIÓN DE FORMATO DE PRE-VENTA**************************"
    Protected Sub ImpresionFormatoPreVenta()
        Dim MyDataSet As New dtTablas 'Declaramos el Conjunto de datos (DATASET)'
        Dim MyDataTable As New dtTablas.tb_detalle_PVDataTable 'Declaramos la tabla que vamos a utilizar para el reporte'
        Dim MyCReport As New CrFormatoPV  'Declaramos el reporte que vamos a utilizar
        Try
            For i As Int16 = 0 To grvDetPreVenta.Rows.Count - 1
                MyDataTable.Rows.Add(grvDetPreVenta.Rows(i).Cells(1).Text,
                                     grvDetPreVenta.Rows(i).Cells(2).Text,
                                     grvDetPreVenta.Rows(i).Cells(3).Text,
                                     grvDetPreVenta.Rows(i).Cells(5).Text,
                                     grvDetPreVenta.Rows(i).Cells(6).Text,
                                     grvDetPreVenta.Rows(i).Cells(7).Text,
                                     grvDetPreVenta.Rows(i).Cells(8).Text,
                                     grvDetPreVenta.Rows(i).Cells(9).Text)
            Next
            MyDataSet.Tables("tb_detalle_PV").Merge(MyDataTable)
            MyCReport.SetDataSource(MyDataSet)
            MyCReport.SetParameterValue("strNumSerie", txtCodigoPre.Text)
            MyCReport.SetParameterValue("nomCliente", txtRazNomCliente.Text)
            MyCReport.SetParameterValue("rucDniCli", txtDocCliente.Text)
            MyCReport.SetParameterValue("dirCliente", txtDirCliente.Text)
            MyCReport.SetParameterValue("strFecha", CDate(FuncionesGlobales.ServerDate).Date)

            MyCReport.PrintOptions.PrinterName = "\\bzarate\EPSONLX350"
            MyCReport.PrintToPrinter(1, True, 1, 200)
        Catch ex As Exception
            MyMessageBox.Alert(ex.Message)
        End Try
    End Sub
#End Region

#Region "************************PROCESO DE LANCE DE IMPRESION********************************"

    Private Sub imprimirClaseC_SinDscto()
        'Dim printDoc As New PrintDocument
        'AddHandler printDoc.PrintPage, AddressOf PrintPage
        'printDoc.PrinterSettings.PrinterName = rutaImpresora
        'printDoc.Print()
    End Sub

    Private Sub PrintPage(ByVal sender As Object, ByVal e As PrintPageEventArgs)
        Dim reportFont As Drawing.Font = New Drawing.Font("Arial", 8)
        Dim topMargin As Double = e.MarginBounds.Top
        Dim yPos As Double = 0
        Dim linesPerPage As Double = 0

        e.Graphics.DrawString("******************************************************", reportFont, Brushes.Black, 5, 20)
        e.Graphics.DrawString("SALIDA POR VENTAS", reportFont, Brushes.Black, 5, 40)
        e.Graphics.DrawString("******************************************************", reportFont, Brushes.Black, 5, 60)
        e.Graphics.DrawString("CORRELATIVO: ", reportFont, Brushes.Black, 5, 80)
        e.Graphics.DrawString(txtCodigoPre.Text, reportFont, Brushes.Black, 100, 80)

        e.Graphics.DrawString("FECHA: ", reportFont, Brushes.Black, 5, 100)
        e.Graphics.DrawString(Format(Date.Now, "dd/MM/yyyy"), reportFont, Brushes.Black, 50, 100)

        e.Graphics.DrawString("HORA: ", reportFont, Brushes.Black, 160, 100)
        e.Graphics.DrawString(Format(TimeOfDay, "HH:mm:ss"), reportFont, Brushes.Black, 200, 100)

        e.Graphics.DrawString("SALIDA DE: " & NombreAlmacen & "  " & NombreUbicacion, reportFont, Brushes.Black, 5, 120)

        e.Graphics.DrawString("CLIENTE: ", reportFont, Brushes.Black, 5, 140)
        e.Graphics.DrawString(txtRazNomCliente.Text, reportFont, Brushes.Black, 60, 140)

        e.Graphics.DrawString("VENDEDOR: ", reportFont, Brushes.Black, 5, 160)
        e.Graphics.DrawString((CType(Page.Master.FindControl("hdNomEmpleado"), HiddenField)).Value, reportFont, Brushes.Black, 75, 160)

        e.Graphics.DrawString("CODIGO", reportFont, Brushes.Black, 0, 190)

        e.Graphics.DrawString("CNT", reportFont, Brushes.Black, 65, 190)
        e.Graphics.DrawString("UND.", reportFont, Brushes.Black, 95, 190)

        e.Graphics.DrawString("CNT", reportFont, Brushes.Black, 135, 190)
        e.Graphics.DrawString("UND.", reportFont, Brushes.Black, 165, 190)

        e.Graphics.DrawString("CNT", reportFont, Brushes.Black, 205, 190)
        e.Graphics.DrawString("UND.", reportFont, Brushes.Black, 235, 190)
        e.Graphics.DrawString("----------------------------------------------------------------", reportFont, Brushes.Black, 15, 205)

        linesPerPage = e.MarginBounds.Height
        yPos += 200

        Dim codArt, caja, doc, und, coment As String
        Dim sizeNomCod As Int16 = 0
        For Each imp As EDetallePreVenta In listaImpresion.ToList
            codArt = imp._COD_ART_IMPORT
            caja = imp._CAJAS
            doc = imp._DOC
            und = imp._UND_SET
            coment = imp._COMENTARIO

            yPos += 20

            e.Graphics.DrawString(codArt, reportFont, Brushes.Black, 0, yPos)

            sizeNomCod = Len(codArt)

            If sizeNomCod > 7 Then
                yPos += 15
                e.Graphics.DrawString(caja, reportFont, Brushes.Black, 65, yPos)
                e.Graphics.DrawString("CJA.", reportFont, Brushes.Black, 95, yPos)

                e.Graphics.DrawString(doc, reportFont, Brushes.Black, 135, yPos)
                e.Graphics.DrawString("DOC.", reportFont, Brushes.Black, 165, yPos)

                e.Graphics.DrawString(und, reportFont, Brushes.Black, 205, yPos)
                e.Graphics.DrawString("UND.", reportFont, Brushes.Black, 235, yPos)
                yPos += 15
                e.Graphics.DrawString("OBS:", reportFont, Brushes.Black, 0, yPos)
                e.Graphics.DrawString(coment, reportFont, Brushes.Black, 35, yPos)
            Else
                e.Graphics.DrawString(caja, reportFont, Brushes.Black, 65, yPos)
                e.Graphics.DrawString("CJA.", reportFont, Brushes.Black, 95, yPos)

                e.Graphics.DrawString(doc, reportFont, Brushes.Black, 135, yPos)
                e.Graphics.DrawString("DOC.", reportFont, Brushes.Black, 165, yPos)

                e.Graphics.DrawString(und, reportFont, Brushes.Black, 205, yPos)
                e.Graphics.DrawString("UND.", reportFont, Brushes.Black, 235, yPos)
                yPos += 15
                e.Graphics.DrawString("OBS:", reportFont, Brushes.Black, 0, yPos)
                e.Graphics.DrawString(coment, reportFont, Brushes.Black, 35, yPos)
            End If
        Next
        listaImpresion.Clear()
    End Sub
#End Region

    Protected Sub grvDetPreVenta_RowDeleting(sender As Object, e As GridViewDeleteEventArgs) Handles grvDetPreVenta.RowDeleting
        Dim resultDelete As String = ""
        MostrarAcordeon("D")
        Try
            Dim result As Integer = 0
            Dim index As Integer = e.RowIndex
            Dim row As GridViewRow = grvDetPreVenta.Rows(index)
            Dim codEmpleado As Integer = (CType(Page.Master.FindControl("hfEmpleadoID"), HiddenField)).Value
            Dim codCombo As Integer

            objEntTXSTComp._NUM_PED = (CType(Page.Master.FindControl("hfIdTXSTComp"), HiddenField)).Value
            Integer.TryParse(row.Cells(28).Text.Trim, codCombo) 'P_COSTO_IGV_K 27

            If hfCodigoPreVenta.Value.Equals("null") Then
                If codCombo > 0 Then
                    For Each fila As GridViewRow In grvDetPreVenta.Rows
                        If Integer.Parse(fila.Cells(28).Text.Trim) = codCombo Then
                            'Se corre la rutina de eliminación de la TB_TX_ST_COMP'
                            objEntTXSTComp._FK_MA_ART = CInt(fila.Cells(19).Text)
                            objEntTXSTComp._FK_ALM = CInt(fila.Cells(22).Text)
                            objEntTXSTComp._FK_UBIC = CInt(fila.Cells(23).Text)

                            resultDelete = objBlTXSTComp.eliminarStockComprometidoTemporal(objEntTXSTComp)
                            result = New BLPreVenta().EliminarHelperDetPreVenta(codEmpleado, objEntTXSTComp._FK_MA_ART)
                        End If
                    Next
                Else
                    objEntTXSTComp._FK_MA_ART = CInt(row.Cells(19).Text)
                    objEntTXSTComp._FK_ALM = CInt(row.Cells(22).Text)
                    objEntTXSTComp._FK_UBIC = CInt(row.Cells(23).Text)

                    resultDelete = objBlTXSTComp.eliminarStockComprometidoTemporal(objEntTXSTComp)
                    result = New BLPreVenta().EliminarHelperDetPreVenta(codEmpleado, objEntTXSTComp._FK_MA_ART)
                End If
                grvDetPreVenta.DataSource = New BLPreVenta().ObtenerHelperDetPreVenta(codEmpleado)
                grvDetPreVenta.DataBind()

            Else
                If hfEstadoPreVenta.Value <> 2 Then
                    Dim intItemDetalle As Integer

                    If codCombo > 0 Then
                        For Each fila As GridViewRow In grvDetPreVenta.Rows
                            If Integer.Parse(fila.Cells(28).Text.Trim) = codCombo Then
                                'Se corre la rutina de eliminación de la TB_TX_ST_COMP'
                                objEntTXSTComp._FK_MA_ART = CInt(fila.Cells(19).Text)
                                objEntTXSTComp._FK_ALM = CInt(fila.Cells(22).Text)
                                objEntTXSTComp._FK_UBIC = CInt(fila.Cells(23).Text)
                                resultDelete = objBlTXSTComp.eliminarStockComprometidoTemporal(objEntTXSTComp)
                                intItemDetalle = CInt(fila.Cells(25).Text)
                                result = New BLPreVenta().EliminarDetallePreVenta(intItemDetalle)
                            End If
                        Next
                    Else
                        objEntTXSTComp._FK_MA_ART = CInt(row.Cells(19).Text)
                        objEntTXSTComp._FK_ALM = CInt(row.Cells(22).Text)
                        objEntTXSTComp._FK_UBIC = CInt(row.Cells(23).Text)
                        resultDelete = objBlTXSTComp.eliminarStockComprometidoTemporal(objEntTXSTComp)
                        intItemDetalle = CInt(row.Cells(25).Text)
                        result = New BLPreVenta().EliminarDetallePreVenta(intItemDetalle)
                    End If

                    grvDetPreVenta.DataSource = New BLPreVenta().ObtenerDetallePV(CInt(hfCodigoPreVenta.Value))
                    grvDetPreVenta.DataBind()

                Else
                    MyMessageBox.Alert("Ya no se puede editar la Pre-Venta porque ya se encuentra en el proceso de DESPACHO")
                    Exit Sub
                End If
            End If

            contTotalProductos()
        Catch ex As Exception
            MyMessageBox.Alert(ex.Message)
        End Try
    End Sub

#Region "**************** LIMPIAR ****************"
    Protected Sub limpiarPreVenta()
        txtLoadCodigo.Text = ""
        ddlLoadClase.SelectedValue = "null"
        LimpiarConsolidado()
        LimpiarConsolidadoAlmacen()
        limpiarPreVenta()
    End Sub
    Protected Sub limpiarCabeceraPre()
        txtCodigoPre.Text = ""
        ddlClase.SelectedValue = "null"
        chkContado.Checked = True
        chkCredito.Checked = False
        ddlCredito.SelectedValue = 0
        ddlMoneda.SelectedValue = 0
        ddlTipoDocumento.SelectedValue = 0
        upnlFormasPago.Update()
        cargarTipoCambio(True, 0)
    End Sub
    Protected Sub limpiarCliente()
        hfCodigoCliente.Value = "null"
        txtRazNomCliente.Text = ""
        txtDirCliente.Text = ""
        txtDocCliente.Text = ""
        txtRPContacto.Text = ""
        txtRPCorreo.Text = ""
        txtRPDireccion.Text = ""
        txtRPDocumento.Text = ""
        txtRPFono.Text = ""
        txtRPRazon.Text = ""
        ddlDepartamento.SelectedIndex = 0
        ddlDistrito.Enabled = False
        ddlProvincia.Enabled = False
        upnlRCliente.Update()
    End Sub
    Protected Sub limpiarNuevoCliente()
        rbPNatural.Checked = True
        rbPJuridica.Checked = False
        txtRPContacto.Text = ""
        txtRPCorreo.Text = ""
        txtRPDireccion.Text = ""
        txtRPDocumento.Text = ""
        txtRPFono.Text = ""
        txtRPRazon.Text = ""
        ddlDepartamento.SelectedIndex = 0
        ddlDistrito.SelectedIndex = 0
        ddlProvincia.SelectedIndex = 0
        upnlRCliente.Update()
    End Sub
    Protected Sub limpiarBusquedaArticulo()
        txtCodigoArt.Text = ""
        txtNombreArt.Text = ""
        txtObservacion.Text = ""
        lblPacking.Text = "xx-->xxx-->xxx"
        chkIsCombo.Checked = False
        LimpiarConsolidado()
    End Sub
    Protected Sub LimpiarConsolidado()
        grvConsolidado.DataSource = Nothing
        grvConsolidado.DataBind()
        hdnConsolidadoArticulo.Value = "hidden"
        hdnConsolidados.Value = "hidden"
        LimpiarConsolidadoAlmacen()
    End Sub
    Protected Sub LimpiarConsolidadoAlmacen()
        grvConsolidadoAUbicacion.DataSource = Nothing
        grvConsolidadoAUbicacion.DataBind()
        LimpiarConsolidadoAUbica()
    End Sub
    Protected Sub LimpiarConsolidadoAUbica()
        lblAlmacen.Text = ""
        lblCajas.Text = ""
        lblDocenas.Text = ""
        lblTotal.Text = ""
        lblUnidad.Text = ""
    End Sub
    Protected Sub LimpiarDetallePre()
        lblTotCaja.Text = ""
        lblTotUnd.Text = ""
        lblTotDoc.Text = ""
        lblImporteTotal.Text = ""
        txtComentario.Text = ""
        grvDetPreVenta.DataSource = Nothing
        grvDetPreVenta.DataBind()
    End Sub
    Protected Sub LimpiarPanelAgregarProducto()
        txtPopACajas.Text = ""
        txtPopADocenas.Text = ""
        txtPopAPiezas.Text = ""
    End Sub

    Protected Sub MostrarAcordeon(ByVal panel As String)
        collapsePreVenta.Attributes.Add("class", "tab-pane fade")
        collapseBusquedaArt.Attributes.Add("class", "tab-pane fade")
        collapseDatosCli.Attributes.Add("class", "tab-pane fade")
        collapseDetallePre.Attributes.Add("class", "tab-pane fade")
        liPreVenta.Attributes.Add("class", "")
        liCliente.Attributes.Add("class", "")
        liArticulo.Attributes.Add("class", "")
        liDetalle.Attributes.Add("class", "")

        If (panel.Equals("P")) Then
            collapsePreVenta.Attributes.Add("class", "tab-pane fade active in")
            liPreVenta.Attributes.Add("class", "active")
        ElseIf (panel.Equals("A")) Then
            collapseBusquedaArt.Attributes.Add("class", "tab-pane fade active in")
            liArticulo.Attributes.Add("class", "active")
        ElseIf (panel.Equals("C")) Then
            collapseDatosCli.Attributes.Add("class", "tab-pane fade active in")
            liCliente.Attributes.Add("class", "active")
        ElseIf (panel.Equals("D")) Then
            collapseDetallePre.Attributes.Add("class", "tab-pane fade active in")
            liDetalle.Attributes.Add("class", "active")
        End If
    End Sub

#End Region

#Region "******************  IMPRIMIR PREVENTA ********************"
    Dim totalVenta As Double = 0
    Dim totalCaja As Integer = 0
    Dim totalDocena As Integer = 0
    Dim totalUnidad As Integer = 0
    Dim formaPago As String = ""
    Dim vendedor As String = ""

    Protected Sub btnImprimir_Click(sender As Object, e As EventArgs)
        Dim filename As String = "C:\PreVenta_Files\" & txtCodigoPre.Text & ".txt"
        Dim oImprimirPreVenta As New EImprimirPreVenta
        Dim dtDetalleArticulos As New DataTable
        oImprimirPreVenta = New BLPreVenta().ObtenerPreVentaImprimir(txtCodigoPre.Text)
        dtDetalleArticulos = New BLPreVenta().obtenerDetallePVImprimir(oImprimirPreVenta.PK_PREVENTA)

        File.Create(filename).Dispose()

        Using writer As StreamWriter = New StreamWriter(filename)
            writer.WriteLine("Pre-Venta N°: " + oImprimirPreVenta.NUM_PREVENTA)
            writer.WriteLine(FuncionesGlobales.getPropertyInSession.Item("direccionPreVenta").ToString.Trim())
            writer.WriteLine("Cliente    : " + oImprimirPreVenta.NOM_CLIENTE)
            writer.WriteLine("RUC / DNI  : " + oImprimirPreVenta.RUC_DNI)
            writer.WriteLine("DIRECCIÓN  : " + oImprimirPreVenta.DIR_ENTREGA)
            writer.WriteLine("FECHA      : " + oImprimirPreVenta.FECHA)
            writer.WriteLine("")
            writer.WriteLine("ITEM " & vbTab & "CÓDIGO ART." & vbTab & "DESCRIPCIÓN" & vbTab & "PACKING" & vbTab & "ALMACEN" & vbTab & "UBIC" & vbTab & "CJA" & vbTab & "DOC" & vbTab & "UND" & vbTab & "PRECIO" & vbTab & "TOTAL")

            For Each oFila As DataRow In dtDetalleArticulos.Rows
                writer.WriteLine(oFila.Item("ITEM") & vbTab & oFila.Item("CÓDIGO ART.") & vbTab & oFila.Item("DESCRIPCIÓN") & vbTab & oFila.Item("PACKING") & vbTab & oFila.Item("ALM") & vbTab & oFila.Item("UBIC") & vbTab & oFila.Item("CJA") & vbTab & oFila.Item("DOC") & vbTab & oFila.Item("UND") & vbTab & oFila.Item("PRECIO") & vbTab & oFila.Item("TOTAL"))
            Next oFila

            writer.WriteLine("")
            writer.WriteLine("Total Caja   : " + totalCaja.ToString())
            writer.WriteLine("Total Doc    : " + totalDocena.ToString())
            writer.WriteLine("Total Und    : " + totalUnidad.ToString())
            writer.WriteLine("Importe Total: " + totalVenta.ToString("n2"))
            writer.WriteLine("____________________________________")
            writer.WriteLine("VENDEDOR           : " + oImprimirPreVenta.VENDEDOR)

        End Using
    End Sub

    Protected Sub makePdf()
        Dim oImprimirPreVenta As New EImprimirPreVenta
        Dim dtDetalleArticulos As New DataTable
        oImprimirPreVenta = New BLPreVenta().ObtenerPreVentaImprimir(txtCodigoPre.Text)
        dtDetalleArticulos = New BLPreVenta().obtenerDetallePVImprimir(oImprimirPreVenta.PK_PREVENTA)

        Dim pdfDoc As Document = New Document(PageSize.A4, 20, 20, 20, 20)


        Try
            Dim pdfw As iTextSharp.text.pdf.PdfWriter

            pdfw = PdfWriter.GetInstance(pdfDoc, System.Web.HttpContext.Current.Response.OutputStream)
            pdfDoc.Open()

            Dim rectangulo As PdfContentByte 'declaración del rectángulo
            rectangulo = pdfw.DirectContent 'código necesario antes de dar coordenadas del rectángulo

            rectangulo.SetLineWidth(2) 'configurando el ancho de linea
            rectangulo.SetColorStroke(BaseColor.BLACK)
            rectangulo.Rectangle(30.0F, 820.0F, 535.0F, -100.0F) 'ini x°, ini y°, x+, y+ ;sigue el plano cartesiano
            rectangulo.Stroke() 'traza el rectangulo actual       

            'crear cabecera
            Dim titulo As Chunk = New Chunk("Pre-Venta N°: " + oImprimirPreVenta.NUM_PREVENTA, FontFactory.GetFont("Times New Roman", 12))
            Dim pTitulo As New Paragraph
            pTitulo.IndentationLeft = 20
            pTitulo.SpacingBefore = 20
            pTitulo.Add(titulo)

            Dim direccion As Chunk = New Chunk(FuncionesGlobales.getPropertyInSession.Item("direccionPreVenta").ToString.Trim, FontFactory.GetFont("Times New Roman", 10))
            Dim pDireccion As New Paragraph
            pDireccion.IndentationLeft = 20
            pDireccion.Add(direccion)

            Dim cliente As Chunk = New Chunk("Cliente         : " + oImprimirPreVenta.NOM_CLIENTE, FontFactory.GetFont("Times New Roman", 10))
            Dim pCliente As New Paragraph
            pCliente.IndentationLeft = 20
            pCliente.Add(cliente)
            Dim ruc_dni As Chunk = New Chunk("RUC / DNI    : " + oImprimirPreVenta.RUC_DNI, FontFactory.GetFont("Times New Roman", 10))
            Dim pRuc_Dni As New Paragraph
            pRuc_Dni.IndentationLeft = 20
            pRuc_Dni.Add(ruc_dni)
            Dim lugarEntrega As Chunk = New Chunk("DIRECCIÓN : " + oImprimirPreVenta.DIR_ENTREGA, FontFactory.GetFont("Times New Roman", 10))
            Dim pLugarEntrega As New Paragraph
            pLugarEntrega.IndentationLeft = 20
            pLugarEntrega.Add(lugarEntrega)
            Dim fecha As Chunk = New Chunk("FECHA        : " + oImprimirPreVenta.FECHA, FontFactory.GetFont("Times New Roman", 10))
            Dim pFecha As New Paragraph
            pFecha.IndentationLeft = 20
            pFecha.SpacingAfter = 20
            pFecha.Add(fecha)

            'agregar cabecera
            pdfDoc.Add(pTitulo)
            pdfDoc.Add(pDireccion)
            pdfDoc.Add(pCliente)
            pdfDoc.Add(pRuc_Dni)
            pdfDoc.Add(pLugarEntrega)
            pdfDoc.Add(pFecha)

            'agregar detalle
            AgregarTabla(pdfDoc, dtDetalleArticulos, New Single() {0.8F, 1.8F, 4.0F, 2.0F, 1.5F, 1.0F, 0.8F, 0.8F, 0.8F, 1.2F, 1.2F}, True)


            Dim footer As Chunk = New Chunk("____________________________________", FontFactory.GetFont("Times New Roman", 14))
            Dim pFooter As New Paragraph
            pFooter.IndentationLeft = 250
            pFooter.Add(footer)
            pdfDoc.Add(pFooter)
            'crear pie
            Dim vendedor As Chunk = New Chunk("VENDEDOR           : " + oImprimirPreVenta.VENDEDOR, FontFactory.GetFont("Times New Roman", 10))
            Dim pVendedor As New Paragraph
            pVendedor.IndentationLeft = 250
            pVendedor.Add(vendedor)

            'agregar pie
            pdfDoc.Add(pVendedor)

            pdfDoc.Close()
            Response.ContentType = "application/pdf"
            Response.AddHeader("content-disposition", "attachment; filename= PreVenta_" + txtCodigoPre.Text + ".pdf")
            HttpContext.Current.Response.Write(pdfDoc)
            Response.Flush()
            Response.End()
        Catch de As DocumentException
            HttpContext.Current.Response.Write(de.Message)
        Catch ioEx As IOException
            HttpContext.Current.Response.Write(ioEx.Message)
        Catch ex As Exception
            HttpContext.Current.Response.Write(ex.Message)
        End Try
    End Sub

    Private Sub AgregarTabla(ByRef pdfDoc As Document, ByRef pTabla As DataTable, ByRef pDimensionColumnas() As Single, _
                          ByVal pIncluirEncabezado As Boolean)

        Dim filaActual As Int16 = 0
        Dim table As New PdfPTable(pTabla.Columns.Count)
        table.WidthPercentage = 95 'pdfDoc.PageSize.Width
        table.SetWidths(pDimensionColumnas)
        table.SpacingBefore = 2.0F
        table.SpacingAfter = 2.0F
        table.DefaultCell.Border = 0

        If pIncluirEncabezado Then
            'Dim fuenteEncabezado As iTextSharp.text.Font = FontFactory.GetFont(FontFactory.HELVETICA_BOLD, 8, iTextSharp.text.Font.NORMAL)
            Dim fuenteEncabezado As iTextSharp.text.Font = FontFactory.GetFont("Times New Roman", 8)

            For Each oColumna As DataColumn In pTabla.Columns
                Dim celda As New PdfPCell(New Phrase(oColumna.Caption, fuenteEncabezado))
                celda.Colspan = 1
                celda.Padding = 3
                'celda.BackgroundColor = ExtendedColor.LIGHT_GRAY
                celda.HorizontalAlignment = Element.ALIGN_CENTER
                celda.VerticalAlignment = Element.ALIGN_CENTER
                table.AddCell(celda)
            Next oColumna
        End If

        'Dim fuenteDatos As iTextSharp.text.Font = FontFactory.GetFont(FontFactory.COURIER, 8, iTextSharp.text.Font.NORMAL)
        Dim fuenteDatos As iTextSharp.text.Font = FontFactory.GetFont("Times New Roman", 8)

        For Each oFila As DataRow In pTabla.Rows
            For Each oColumna As DataColumn In pTabla.Columns
                Dim celda As New PdfPCell(New Phrase(oFila(oColumna), fuenteDatos))
                celda.Colspan = 1
                celda.Padding = 3
                If oColumna.DataType = System.Type.GetType("System.String") Then
                    celda.HorizontalAlignment = Element.ALIGN_LEFT
                ElseIf oColumna.DataType = System.Type.GetType("System.Decimal") Then
                    celda.HorizontalAlignment = Element.ALIGN_RIGHT
                End If
                celda.VerticalAlignment = Element.ALIGN_CENTER
                table.AddCell(celda)
            Next oColumna
        Next oFila
        pdfDoc.Add(table)

        CalcularTotal(pTabla)

        Dim chkTotalCaja As Chunk = New Chunk("Total Caja: " + totalCaja.ToString(), FontFactory.GetFont("Times New Roman", 10))
        Dim pTotalCaja As New Paragraph
        pTotalCaja.IndentationLeft = 250
        pTotalCaja.Add(chkTotalCaja)
        pdfDoc.Add(pTotalCaja)

        Dim chkTotalDocena As Chunk = New Chunk("Total Doc:  " + totalDocena.ToString(), FontFactory.GetFont("Times New Roman", 10))
        Dim pTotalDocena As New Paragraph
        pTotalDocena.IndentationLeft = 250
        pTotalDocena.Add(chkTotalDocena)
        pdfDoc.Add(pTotalDocena)

        Dim chkTotalUnidad As Chunk = New Chunk("Total Und:  " + totalUnidad.ToString(), FontFactory.GetFont("Times New Roman", 10))
        Dim pTotalUnidad As New Paragraph
        pTotalUnidad.IndentationLeft = 250
        pTotalUnidad.Add(chkTotalUnidad)
        pdfDoc.Add(pTotalUnidad)

        Dim chkImporte As Chunk = New Chunk("Importe Total: " + totalVenta.ToString("n2"), FontFactory.GetFont("Times New Roman", 10))
        Dim pImporte As New Paragraph
        pImporte.IndentationLeft = 250
        pImporte.Add(chkImporte)
        pdfDoc.Add(pImporte)

    End Sub

    Protected Sub CalcularTotal(ByVal pTable As DataTable)
        For Each fila As DataRow In pTable.Rows
            totalCaja += IIf(fila.Item(6) = Nothing, 0, fila.Item(6))
            totalDocena += IIf(fila.Item(7) = Nothing, 0, fila.Item(7))
            totalUnidad += IIf(fila.Item(8) = Nothing, 0, fila.Item(8))
            totalVenta += IIf(fila.Item(10) = Nothing, 0, fila.Item(10))
        Next
    End Sub
#End Region

#Region "Eliminar Stock Comprometido Temporal"

    Protected Sub eliminarListaStockTemporalTXSTComp()
        'Si hay detalle en la grilla, se elimina de la tabla temporal TX'
        Dim objListTXSTComp As New List(Of ETXSTComp)
        Dim resultado As String = ""
        If grvDetPreVenta.Rows.Count > 0 Then

            For i As Int16 = 0 To grvDetPreVenta.Rows.Count - 1
                objListTXSTComp.Add(New ETXSTComp With {
                                    ._NUM_PED = (CType(Page.Master.FindControl("hfIdTXSTComp"), HiddenField)).Value,
                                    ._FK_MA_ART = grvDetPreVenta.Rows(i).Cells(19).Text,
                                    ._FK_EMPR = grvDetPreVenta.Rows(i).Cells(21).Text,
                                    ._FK_ALM = grvDetPreVenta.Rows(i).Cells(22).Text,
                                    ._FK_UBIC = grvDetPreVenta.Rows(i).Cells(23).Text,
                                    ._UNID = grvDetPreVenta.Rows(i).Cells(11).Text})
            Next
            objEntTXSTComp._listadoETXSTComp = objListTXSTComp
            resultado = objBlTXSTComp.eliminarListaStockComprometidoTemporal(objEntTXSTComp)
        End If
    End Sub


    Protected Sub btnCancelar_Click(sender As Object, e As EventArgs) Handles btnCancelar.Click
        eliminarListaStockTemporalTXSTComp()
        'Corre limpieza de atributos'
        limpiarBusquedaArticulo()
        limpiarCliente()
        limpiarCabeceraPre()
        LimpiarDetallePre()
        MPopLoad.Show()
    End Sub
#End Region

#Region "SUPER MEGA COMBOS ;)"

    Protected Sub btnPopAddCombo_Click(sender As Object, e As EventArgs)
        Try
            Dim strCodCombo As String = txtCodigoArt.Text.Trim
            Dim intCantidadSol As Integer = 0
            Integer.TryParse(txtComboCant.Text.Trim, intCantidadSol)

            If intCantidadSol > 0 Then

                Dim intUndTotalDet, intStockArticulo As Integer
                Dim unidadesDistribuidas As Dictionary(Of String, Integer)
                Dim objBlComboArticulos As New BlComboArticulos
                Dim dtArticulosCombo As New DataTable
                Dim objEUbicacionAlm As New EUbicacionAlmacen
                Dim objBlUbicacionAlm As New BlUbicacionAlmacen
                Dim oBlVentaMayorista As New BlVentaMayorista
                Dim oHelperPreVenta As New EHelperPreVenta

                dtArticulosCombo = objBlComboArticulos.getFullArticulosxCodigoCombo(strCodCombo)

                If dtArticulosCombo.Rows.Count = 0 Then
                    MyMessageBox.Alert("Lo sentimos!! El Combo ingresado no se encuentra activo")
                    Exit Sub
                Else
                    'VALIDAR PRIMERO EL STOCK POR ARTICULO ANTES DE AGREGAR AL DETALLE'

                    Dim intCodUbicacionCBO As Integer = Integer.Parse(FuncionesGlobales.getPropertyInSession.Item("codigoUbicacionCombos").ToString.Trim)
                    Dim igvActual As Integer = Integer.Parse(FuncionesGlobales.getPropertyInSession.Item("igvActual").ToString.Trim)

                    objEUbicacionAlm = objBlUbicacionAlm.obtenerUbicacionCodigo(intCodUbicacionCBO)

                    For Each row As DataRow In dtArticulosCombo.Rows
                        intStockArticulo = oBlVentaMayorista.obtenerStockDisponibleArticulo(row("FK_MAESTRO_ARTICULOS"), objEUbicacionAlm._FK_ALMACEN, objEUbicacionAlm._PK_UBICACIONES, row("FK_EMPRESA"))
                        intUndTotalDet = FuncionesGlobales.calcularUndTotalComboArt(row("CANT_VTA"), intCantidadSol)
                        If intStockArticulo < intUndTotalDet Then
                            MyMessageBox.Alert("Solo hay " & intStockArticulo & " articulos del código " & row("COD_ART_IMPORT"))
                            Exit Sub
                        End If
                    Next

                    For Each row As DataRow In dtArticulosCombo.Rows
                        'Calculamos la cantidad total, contra cantidad de combos
                        intUndTotalDet = FuncionesGlobales.calcularUndTotalComboArt(row("CANT_VTA"), intCantidadSol)
                        'Distribuimos la cantidad
                        unidadesDistribuidas = FuncionesGlobales.distribuirStockPresentaciones(row("NOMBRE_UND"), row("CANT_PACKING"), intUndTotalDet)

                        oHelperPreVenta._ORDEN_ITEM = grvDetPreVenta.Rows.Count + 1
                        oHelperPreVenta._FK_EMPRESA = row("FK_EMPRESA")
                        oHelperPreVenta._DESC_EMPRESA = row("NOMBRE_EMPRESA")
                        oHelperPreVenta._FK_EMP_ART = row("FK_EMPR_ARTC")
                        oHelperPreVenta._FK_MAESTRO = row("FK_MAESTRO_ARTICULOS")
                        oHelperPreVenta._COD_ART_IMPORT = row("COD_ART_IMPORT")
                        oHelperPreVenta._NOMBRE_VENTA = row("NOMBRE_VENTA")
                        oHelperPreVenta._DESC_PACKING = row("TXT_PACKING")
                        oHelperPreVenta._CAN_PACKING = row("CANT_PACKING") 'intCantPackSolic
                        oHelperPreVenta._CAN_MAYOR = row("CANT_MAYOR") 'intCantMayor
                        oHelperPreVenta._FK_ALMACEN = objEUbicacionAlm._FK_ALMACEN
                        oHelperPreVenta._DESC_ALMACEN = objEUbicacionAlm._NOMBRE_ALMACEN
                        oHelperPreVenta._FK_UBICACION = objEUbicacionAlm._PK_UBICACIONES
                        oHelperPreVenta._DESC_UBICACION = objEUbicacionAlm._NOMBRE_UBICACION
                        oHelperPreVenta._UNIDADES_TOTAL_PEDIDAS = intUndTotalDet 'xxxxxxxxxxxx
                        oHelperPreVenta._CAJAS_PEDIDAS = unidadesDistribuidas.Item("CAJA")
                        oHelperPreVenta._DOCENAS_PEDIDAS = unidadesDistribuidas.Item("DOCENA")
                        oHelperPreVenta._UNIDADES_PEDIDAS = unidadesDistribuidas.Item("UNIDAD")
                        oHelperPreVenta._NUEVO_REGISTRO = "1"
                        oHelperPreVenta._ST_DSCTO = False
                        oHelperPreVenta._COMENTARIO = txtComentario.Text
                        oHelperPreVenta._IP_IMPRESORA = objEUbicacionAlm._IMPRESORA
                        oHelperPreVenta._ID_TX = (CType(Page.Master.FindControl("hfIdTXSTComp"), HiddenField)).Value
                        oHelperPreVenta._DLL_CLASE = ddlClase.SelectedValue
                        oHelperPreVenta._PK_GRP_VENTA = row("PK_GRP_VENTA")

                        Dim listPrecios As New Dictionary(Of String, Decimal)
                        listPrecios = New AsignacionPreciosDocMay().asignarPrecioPorArticulo(oHelperPreVenta, igvActual)

                        oHelperPreVenta._PRECIO = listPrecios.Item("PrecioAsignado") 'pMin CAMBIA
                        oHelperPreVenta._PRECIO_COSTO = listPrecios.Item("CostoFactSol") 'oPreVenta._PRECIO_COSTO CAMBIA
                        oHelperPreVenta._PRECIO_TOTAL = listPrecios.Item("PrecTotal") 'pTotal 'ToString("n2"), CAMBIA
                        oHelperPreVenta._PRECIO_COSTO_IGV_K = listPrecios.Item("CostoCotSol")
                        oHelperPreVenta._NUM_PRE_VTA = txtCodigoPre.Text

                        If hfCodigoPreVenta.Value.Equals("null") Then
                            Dim codEmpleado As Integer = (CType(Page.Master.FindControl("hfEmpleadoID"), HiddenField)).Value
                            Dim result As Integer = 0
                            oHelperPreVenta._FK_EMPLEADO = codEmpleado
                            result = New BLPreVenta().GrabarHelperDetPreVenta(oHelperPreVenta)
                            If result = 1 Then
                                limpiarBusquedaArticulo()
                                grvDetPreVenta.DataSource = New BLPreVenta().ObtenerHelperDetPreVenta(codEmpleado)
                                grvDetPreVenta.DataBind()
                                MostrarAcordeon("D")
                                MPopupAddCombo.Hide()
                            Else
                                MyMessageBox.Alert("No se grabó el artículo")
                            End If
                        Else
                            Dim result As Integer = 0
                            oHelperPreVenta._FK_PRE_VTA = CInt(hfCodigoPreVenta.Value)
                            result = New BLPreVenta().InsertarDetallePreventa(oHelperPreVenta)
                            If result = 1 Then
                                limpiarBusquedaArticulo()
                                grvDetPreVenta.DataSource = New BLPreVenta().ObtenerDetallePV(CInt(hfCodigoPreVenta.Value))
                                grvDetPreVenta.DataBind()
                                MostrarAcordeon("D")
                            Else
                                MyMessageBox.Alert("No se grabó el artículo")
                            End If
                        End If

                        '24/12/2014 Se hace el insert para un Comprometido temporal de los datos'
                        objEntTXSTComp._NUM_PED = (CType(Page.Master.FindControl("hfIdTXSTComp"), HiddenField)).Value 'idTxStComp
                        objEntTXSTComp._FK_MA_ART = row("FK_MAESTRO_ARTICULOS")
                        objEntTXSTComp._FK_EMPR = row("FK_EMPRESA")
                        objEntTXSTComp._FK_ALM = objEUbicacionAlm._FK_ALMACEN
                        objEntTXSTComp._FK_UBIC = objEUbicacionAlm._PK_UBICACIONES
                        objEntTXSTComp._UNID = intUndTotalDet
                        resultado = objBlTXSTComp.insertarStockComprometidoTemporal(objEntTXSTComp)

                    Next
                End If
                Call contTotalProductos()
                Call limpiarPopAddCombo()
            Else
                MyMessageBox.Alert("Ingrese una cantidad valida")
            End If
        Catch ex As Exception
            Console.WriteLine(ex.Message)
        End Try
    End Sub

    Protected Sub chkIsCombo_CheckedChanged(sender As Object, e As EventArgs)
        hdnConsolidados.Value = "hidden"
        If chkIsCombo.Checked Then
            divArticuloDetail.Attributes.Add("hidden", "hidden")
        Else
            divArticuloDetail.Attributes.Remove("hidden")
        End If
        MostrarAcordeon("A")
    End Sub

    Protected Sub btnPopCancelCombo_Click(sender As Object, e As EventArgs)
        limpiarPopAddCombo()
    End Sub

    Protected Sub limpiarPopAddCombo()
        txtComboCant.Text = ""
        grvCombos.DataSource = New DataTable
        grvCombos.DataBind()
        MPopupAddCombo.Hide()
    End Sub

#End Region

End Class
