﻿Imports System.Web.Services
Imports System.Web.Services.Protocols
Imports System.ComponentModel
Imports BusinessLogic
Imports System.Web.Script.Services

' Para permitir que se llame a este servicio web desde un script, usando ASP.NET AJAX, quite la marca de comentario de la línea siguiente.
<System.Web.Script.Services.ScriptService()>
Public Class WSArticulos
    Inherits System.Web.Services.WebService

    <ScriptMethod(), WebMethod()> _
    Public Function BuscarArticuloXCodigo(ByVal prefixText As String, ByVal count As Integer, ByVal contextKey As String) As List(Of String)
        Try
            If contextKey.Trim.ToUpper = "FALSE" Then
                Return New BLArticulo().getArticuloxCodigoImp(prefixText)
            Else
                Return New BLArticulo().getComboxCodigo(prefixText)
            End If
        Catch ex As Exception
            Console.WriteLine(ex.Message)
            Return New List(Of String)
        End Try

    End Function

    <ScriptMethod(), WebMethod()> _
    Public Function BuscarArticuloXNombre(ByVal prefixText As String, ByVal count As Integer, ByVal contextKey As String) As List(Of String)
        Try
            If contextKey.Trim.ToUpper = "FALSE" Then
                Return New BLArticulo().getArticuloxNombre(prefixText)
            Else
                Return New BLArticulo().getComboxNombre(prefixText)
            End If
        Catch ex As Exception
            Console.WriteLine(ex.Message)
            Return New List(Of String)
        End Try
    End Function

    <ScriptMethod(), WebMethod()> _
    Public Function BuscarClienteXDocumento(ByVal prefixText As String, ByVal count As Integer) As List(Of String)
        Try
            Return New BLCliente().getClienteXDocumento(prefixText)
        Catch ex As Exception
            Console.WriteLine(ex.Message)
            Return New List(Of String)
        End Try
    End Function

End Class

'<System.Web.Script.Services.ScriptService()>
'<System.Web.Services.WebService(Namespace:="http://tempuri.org/")> _
'<System.Web.Services.WebServiceBinding(ConformsTo:=WsiProfiles.BasicProfile1_1)> _
'<ToolboxItem(True)> _