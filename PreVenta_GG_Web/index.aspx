﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="index.aspx.vb" Inherits="PreVenta_GG_Web.index" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml" lang="es-pe">
<head runat="server">
    <meta name="viewport" content="width=device-width, initial-scale=1"/>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>Index</title>
    <link href="Resource/bootstrap/css/bootstrap.min.css" rel="stylesheet" />
    <script src="Resource/bootstrap/js/bootstrap.min.js"></script>
    <link href="Resource/font-awesome-4.2.0/css/font-awesome.min.css" rel="stylesheet" />
</head>
<body>
    <script>
        function ValidarIngreso() {
            var passw = document.getElementById('<%= txtPassword.ClientID%>');
            var codigo = document.getElementById('<%= txtUsuario.ClientID%>');

            if (codigo.value.trim() == "")
            {
                alert("Ingrese el usuario");
                codigo.focus();
                return false;
            }
            else if (passw.value.trim() == "")
            {
                alert("Ingrese la contraseña");
                passw.focus();
                return false;
            }
        }
    </script>
    <style>
        body {
          background-color: #063953;
          margin-top:80px;
          font-size:18px;
        }

        .form-signin {
          max-width: 500px;
          padding: 15px;
          margin: 0 auto;
        }
        .form-signin .form-signin-heading,
        .form-signin .checkbox {
          margin-bottom: 10px;
        }
        .form-signin .checkbox {
          font-weight: normal;
        }
        .form-signin .form-control {
          position: relative;
          height: 43px;
          -webkit-box-sizing: border-box;
             -moz-box-sizing: border-box;
                  box-sizing: border-box;
          padding: 10px;
          font-size: 16px;
        }
        .form-signin .form-control:focus {
          z-index: 2;
        }
        .form-signin input[type="email"] {
          margin-bottom: -1px;
          border-bottom-right-radius: 0;
          border-bottom-left-radius: 0;
        }
        .form-signin input[type="password"] {
          margin-bottom: 0px;
          border-top-left-radius: 0;
          border-top-right-radius: 0;
        }
    </style>
     <div class="container">
        <form class="form-signin" role="form" style="border: double; background-color: white" runat="server">
            <div class="control-group">
                <div class="row">
                    <div class="input-prepend">
                        <img src="Resource/images/logo.png" alt="Sistema Illari" class="img-responsive" 
                            style="height: 40%; width: 60%; margin-left:20%" />
                    </div>
                </div>
            </div>
            <h2 class="form-signin-heading" style="text-align: center;">Login</h2>
            <div class="control-group">
                <div class="input-group margin-bottom-sm">
                    <span class="input-group-addon"><i class="fa fa-user fa-fw"></i></span>
                    <asp:TextBox ID="txtUsuario" runat="server" placeholder="Ingrese usuario" CssClass="form-control" />
                </div>
            </div>
            <br />
            <div class="control-group">
                <div class="input-group margin-bottom-sm">
                    <span class="input-group-addon"><i class="fa fa-key fa-fw fa-spin"></i></span>
                    <asp:TextBox ID="txtPassword" runat="server" TextMode="Password" placeholder="Ingrese contraseña" 
                        CssClass="form-control" MaxLength="15"/>
                </div>
            </div>
            <br />
            <div class="control-group">
                <asp:Button ID="btnIngresar" runat="server" Text="Aceptar" CssClass="btn btn-lg btn-primary btn-block"
                     OnClientClick="return ValidarIngreso();" OnClick="btnIngresar_Click" ToolTip="Aceptar" />
            </div>
        </form>
    </div>
</body>
</html>
