﻿Imports WebUtils
Imports Entidades

Public Class index
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

    End Sub

    Protected Sub btnIngresar_Click(sender As Object, e As EventArgs)

        Dim usuario As String = txtUsuario.Text.Trim().ToLower()
        Dim password As String = txtPassword.Text.Trim()

        If usuario.Equals("") Then
            MyMessageBox.Alert("Ingrese su Usuario")
        ElseIf password.Equals("") Then
            MyMessageBox.Alert("Ingrese su Contraseña")
        Else
            Try
                Dim oBLEmpleado As New BusinessLogic.BLEmpleado()
                Dim oEmpleado As New EEmpleado
                oEmpleado = oBLEmpleado.obtenerEmpleadoPorUserName(usuario, password)
                'traer los datos del empleado si el usuario existe

                If oEmpleado._PK_EMPLEADO.Equals("") Or oEmpleado._PK_EMPLEADO <= 0 Then
                    MyMessageBox.Alert("El Usuario No Existe")
                Else
                    Dim intEstado, camp_pass As Integer
                    Dim clave As String

                    intEstado = oEmpleado._FK_ESTADO
                    camp_pass = oEmpleado._CAMB_PASS
                    clave = oEmpleado._CLAVE

                    If intEstado <> 194 And intEstado <> 156 Then
                        MsgBox("La Cuenta se encuentra Bloqueada", vbExclamation)
                        Exit Sub
                    ElseIf clave.Equals(password) Then
                        MsgBox("La Contraseña no es correcta", vbExclamation)
                    ElseIf Not clave.Equals(password) Then
                        Dim cookie As HttpCookie
                        cookie = HttpContext.Current.Request.Cookies("EmployerGG")
                        If (Not cookie Is Nothing) Then
                            cookie = Nothing
                        End If
                        cookie = New HttpCookie("EmployerGG")
                        cookie.Values.Clear()
                        cookie("EmployerID") = oEmpleado._PK_EMPLEADO
                        cookie("EmployerName") = (oEmpleado._NOMBRES & " " & oEmpleado._APELLIDOS).ToUpper()
                        cookie("EmployerUser") = oEmpleado._USUARIO
                        cookie.Expires = DateTime.Now.AddMinutes(480D)
                        HttpContext.Current.Response.Cookies.Add(cookie)
                        Response.Redirect("~/RegistroPreVenta.aspx")
                    End If
                End If
            Catch ex As Exception
                MyMessageBox.Alert(ex.Message)
            End Try
        End If
    End Sub
End Class