﻿Imports Entidades
Imports BusinessLogic
Imports WebUtils

Public Class AsignacionPreciosDocMay
    Dim objEntArticulo As New EArticulo
    Dim objBlArticulo As New BLArticulo
    Dim listPrecios As New Dictionary(Of String, Decimal)

    Public Function asignarPrecioPorArticulo(ByVal oHelperPreVenta As EHelperPreVenta, ByVal valorIgv As Decimal) As Dictionary(Of String, Decimal)
        Dim CostoFactSol = 0,
            CostoCotSol = 0,
            precMax = 0,
            precProm = 0,
            precMinimo = 0,
            precTienda = 0,
            dsctoCateg = 0,
            dsctoCondPago = 0,
            dsctoVol As Decimal = 0
        Dim dsctoCant As Int32 = 0
        Dim precAsignado = 0, precTotal As Decimal = 0
        Dim listAsignacionPrecio As New Dictionary(Of String, Decimal)

        Try
            objEntArticulo = objBlArticulo.obtenerPreciosArticulo(oHelperPreVenta._FK_MAESTRO)

            If objEntArticulo._pk_articulo <> Nothing Then
                precMax = objEntArticulo._precio_maximo
                precProm = objEntArticulo._precio_promedio
                precMinimo = objEntArticulo._precio_minimo
                precTienda = objEntArticulo._precio_tienda

                CostoFactSol = ((objEntArticulo._precio_costo / ((valorIgv / 100) + 1)) * objEntArticulo._tipo_cambio).ToString("n2")
                CostoCotSol = ((objEntArticulo._precio_costo_cotizacion / ((valorIgv / 100) + 1)) * objEntArticulo._tipo_cambio).ToString("n2")
                precMax = objEntArticulo._precio_maximo
                precProm = objEntArticulo._precio_promedio
                precMinimo = objEntArticulo._precio_minimo
                precTienda = objEntArticulo._precio_tienda
                dsctoCateg = objEntArticulo._desc_categoria
                dsctoCondPago = objEntArticulo._desc_cond_pago
                dsctoVol = objEntArticulo._desc_volumen
                dsctoCant = objEntArticulo._desc_cantidad
            End If

            'Condicionales por Clase de Pedido M'
            If oHelperPreVenta._DLL_CLASE = "M" Then
                If oHelperPreVenta._UNIDADES_TOTAL_PEDIDAS < 3 And oHelperPreVenta._CAJAS_PEDIDAS <= 1 Then 'Cuando las unidades son menores a 3 - P.TIENDA
                    precAsignado = precTienda
                    precTotal = calcularPrecioUnidades(oHelperPreVenta._CAN_MAYOR, oHelperPreVenta._CAN_PACKING,
                                                                         precAsignado, oHelperPreVenta._UNIDADES_TOTAL_PEDIDAS)
                Else
                    If oHelperPreVenta._UNIDADES_TOTAL_PEDIDAS < 3 And oHelperPreVenta._CAJAS_PEDIDAS >= 1 Then 'Para los articulos grandes paccking menor a 3
                        precAsignado = precMinimo
                    ElseIf oHelperPreVenta._UNIDADES_TOTAL_PEDIDAS < 12 And oHelperPreVenta._CAJAS_PEDIDAS >= 1 Then 'Para los articulos grandes packing menor a 12 
                        precAsignado = precMinimo
                    ElseIf oHelperPreVenta._UNIDADES_TOTAL_PEDIDAS >= 3 And oHelperPreVenta._UNIDADES_TOTAL_PEDIDAS < 12 Then 'Escala 1 --> de 3 a 11 
                        precAsignado = precMax
                    ElseIf oHelperPreVenta._UNIDADES_TOTAL_PEDIDAS >= 12 And oHelperPreVenta._CAJAS_PEDIDAS < 1 Then 'Escala 2 --> de 12 a packing (DOC)
                        precAsignado = precProm
                    ElseIf oHelperPreVenta._UNIDADES_TOTAL_PEDIDAS >= 12 And oHelperPreVenta._CAJAS_PEDIDAS >= 1 Then 'Escala 3 packing (Caja)'
                        precAsignado = precMinimo
                    End If
                    precTotal = calcularPrecioPacking(oHelperPreVenta._CAN_MAYOR, oHelperPreVenta._CAN_PACKING,
                                                                         precAsignado, oHelperPreVenta._UNIDADES_TOTAL_PEDIDAS)
                End If
            End If

            'Condicionales de Clase de Pedido I'
            If oHelperPreVenta._DLL_CLASE = "I" Then
                precAsignado = precMax
                precTotal = calcularPrecioPacking(oHelperPreVenta._CAN_MAYOR, oHelperPreVenta._CAN_PACKING,
                                                                        precAsignado, oHelperPreVenta._UNIDADES_TOTAL_PEDIDAS)
            End If

            'Condicionales de Clase de Pedido E'
            If oHelperPreVenta._DLL_CLASE = "E" Then
                precAsignado = precMinimo
                precTotal = calcularPrecioPacking(oHelperPreVenta._CAN_MAYOR, oHelperPreVenta._CAN_PACKING,
                                                                    precAsignado, oHelperPreVenta._UNIDADES_TOTAL_PEDIDAS)
            End If

            listAsignacionPrecio.Add("PrecioAsignado", precAsignado.ToString("n2"))
            listAsignacionPrecio.Add("PrecTotal", precTotal.ToString("n2"))
            listAsignacionPrecio.Add("CostoFactSol", CostoFactSol.ToString("n2"))
            listAsignacionPrecio.Add("CostoCotSol", CostoCotSol.ToString("n2"))
        Catch ex As Exception
            MyMessageBox.Alert("Error: " + ex.Message)
        End Try
        Return listAsignacionPrecio
    End Function

    Public Function calcularPrecioUnidades(ByVal cantMayor As Int16, ByVal cantPacking As Int16, ByVal precio As Decimal, ByVal UndTotal As Int16) As Decimal
        Dim precioUnidad As Decimal
        Try
            If cantMayor = 1 And cantPacking >= 12 Then
                precioUnidad = precio * UndTotal
            ElseIf cantMayor = 1 And cantPacking < 12 Then
                precioUnidad = precio * UndTotal
            ElseIf cantMayor = 12 And cantPacking = 1 Then
                precioUnidad = precio * UndTotal
            ElseIf cantMayor = 12 Then
                precioUnidad = precio * UndTotal
            End If
        Catch ex As Exception
        End Try
        Return precioUnidad
    End Function

    Public Function calcularPrecioPacking(ByVal cantMayor As Int16, ByVal cantPacking As Int16, ByVal precio As Decimal, ByVal UndTotal As Int16) As Decimal
        Dim precTotalAsignado As Decimal
        Try
            precTotalAsignado = precio * UndTotal
        Catch ex As Exception
        End Try
        Return precTotalAsignado
    End Function
End Class
