﻿Imports BusinessLogic
Imports System.Resources
Imports System.Reflection

Public NotInheritable Class FuncionesGlobales

    Public Shared textoUnidadesPacking As New Dictionary(Of String, Integer) From {{"SET", 1},
                                {"DOC", 12},
                                {"UND", 1},
                                {"DEC", 10}}
    Public Shared Function obtenerStockArticulosPorEmpresaAlmacenUbicacionDelKardex(ByVal idEmpresa As Integer, _
                                                                                    ByVal idMaestroArticulo As Integer, _
                                                                                    ByVal idAlmacen As Integer, _
                                                                                    ByVal idUbicacion As Integer) As Integer
        Return New BLArticulo().obtenerStockArticulosPorEmpresaAlmacenUbicacionDelKardex _
            (idEmpresa, idMaestroArticulo, idAlmacen, idUbicacion)
    End Function

    Public Shared Function getPropertyInSession() As Dictionary(Of String, String)
        If System.Web.HttpContext.Current.Session("properties") Is Nothing Then
            System.Web.HttpContext.Current.Session.Add("properties", New BlPropiedadesSistema().obtenerPropiedadesSistema)
        End If
        Dim dictionary As Dictionary(Of String, String) = System.Web.HttpContext.Current.Session("properties")
        Return dictionary
    End Function

    Public Shared Function distribuirStockPresentaciones(ByVal pTextoUnidadPacking As String, ByVal pCantidadUnidadPacking As Integer, ByVal pCantidadUnidades As Integer) As Dictionary(Of String, Integer)
        'Para evitar los negativos en el informe
        If pCantidadUnidades < 0 Then
            pCantidadUnidades = pCantidadUnidades * -1
        End If
        Try
            Dim unidadPacking As Double = textoUnidadesPacking.Item(pTextoUnidadPacking) * pCantidadUnidadPacking
            Dim residuo As Double = pCantidadUnidades Mod unidadPacking

            Dim cajas As Integer = Int(pCantidadUnidades / unidadPacking)
            Dim docenas As Integer = IIf(residuo >= 12, Int(residuo / 12), 0)
            Dim unidad As Integer = IIf(docenas > 0, residuo - (docenas * 12), residuo)

            Dim resultado As New Dictionary(Of String, Integer)

            resultado.Add("CAJA", cajas)
            resultado.Add("DOCENA", docenas)
            resultado.Add("UNIDAD", unidad)

            Return resultado
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Shared Function ServerDate() As DateTime
        Try
            Return New BLUtilFromServer().getSysDate()
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Shared Function calcularUndTotalComboArt(ByVal intCantDet As Int16, ByVal intCantCbo As Int16) As Integer
        Dim resultado As Integer
        Try
            resultado = (intCantDet * intCantCbo)
        Catch ex As Exception
        End Try
        Return resultado
    End Function
End Class
