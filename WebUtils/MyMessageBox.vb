﻿Imports System
Imports System.Text
Imports System.Web
Imports System.Web.UI
Imports System.Web.UI.WebControls

Public NotInheritable Class MyMessageBox

    Public Shared Sub Alert(sbMsg As String)
        Dim currentPage As Page = HttpContext.Current.Handler
        Dim sb As New StringBuilder

        sb.Append("alert('")
        sb.Append(sbMsg.Replace("'", ""))
        sb.Append("');")
        ScriptManager.RegisterClientScriptBlock(currentPage, GetType(MyMessageBox), "success", sb.ToString(), True)
    End Sub

End Class
